﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SearchPage : MonoBehaviour {

    public GameObject page;
    public InputField inputField;
    public GameObject brandContent;
    public GameObject categoryContent;
    bool searching;
    bool getActivate = true;
    GameObject go = null;

    // Movement speed in units/sec.
    float speed = 7560f;

    // Time when the movement started.
    private float startTime;

    public bool GetmenuActivate()
    {
        return getActivate;
    }

    public void Search(bool activate)
    {
        searching = activate;
    }

    public void GetPage(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
    }

    private void Start()
    {
        page.transform.localPosition = Vector2.zero;
    }

    private void Update()
    {
        if (brandContent.activeInHierarchy)
        {
            go = brandContent;
        }
        else if (categoryContent.activeInHierarchy)
        {
            go = categoryContent;
        }

        if (searching)
        {
            string search = inputField.text.ToLower();

            foreach (Transform child in go.transform)
            {
                if (child.name.ToLower().StartsWith(search, StringComparison.CurrentCultureIgnoreCase))
                {
                    child.gameObject.SetActive(true);
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            foreach (Transform child in go.transform)
            {
                child.gameObject.SetActive(true);
            }
        }

        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / 1080f;

        // Set our position as a fraction of the distance between the markers.
        if (!getActivate)
        {
            if (!page.transform.localPosition.Equals(new Vector2(-1080f, 0)))
            {
                page.transform.localPosition = Vector2.Lerp(Vector2.zero, new Vector2(-1080f, 0), fracJourney);
            }
        }
        else
        {
            if (!page.transform.localPosition.Equals(Vector2.zero))
            {
                page.transform.localPosition = Vector2.Lerp(new Vector2(-1080f, 0), Vector2.zero, fracJourney);
            }
        }
    }
}
