﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InstantiateObject : MonoBehaviour
{
    [SerializeField] GameObject anchorPlacements;
    [SerializeField] GameObject modelHolder;
    [SerializeField] GameObject loadingPanel;
    [SerializeField] Image loadingBar;
    [SerializeField] Text loadingProgress;

    GameObject TargetGoAR = null;
    GameObject TargetGoModel = null;
    Material[] TargetMats = null;

    PlaneManager planeManager;
    TouchHandler touchHandler;

    static Dictionary<string, AssetBundle> ObjectReference = new Dictionary<string, AssetBundle>();
    List<string> LoadingReferenceTable = new List<string>();

    string ConfigurationRequestURL = string.Empty;
    readonly string AssetsVersion = "AssetsVersion";

    //this for initialization
    void Start()
    {
        planeManager = FindObjectOfType<PlaneManager>();
        touchHandler = FindObjectOfType<TouchHandler>();

#if UNITY_IOS
        ConfigurationRequestURL = "http://galaxybattles.deekie.com/walkint-i/WalkintConfiguration.txt";
#elif UNITY_ANDROID
        ConfigurationRequestURL = "http://express.odanow.com/assetbundle/AssetBundleAndroid.txt";
#endif
        StartCoroutine(LoadVersion());
    }

    public void DestroyModel()
    {
        GameObject modelObject = modelHolder.GetComponentInChildren<ObjectComponents>().gameObject;
        Debug.Log("MODEL OBJECT IS NULL: " + modelObject == null);

        if (modelHolder.GetComponentInChildren<ObjectComponents>() != null)
        {
            Destroy(modelHolder.GetComponentInChildren<ObjectComponents>().gameObject);
            Debug.Log("MODEL DESTROYED");
        }
        else
        {
            Debug.Log("MODEL NOT DESTROYED");
        }
    }

    public void DestroyAR()
    {
        if (TargetGoAR != null)
        {
            TargetGoAR.transform.parent = anchorPlacements.transform;
        }

        if (anchorPlacements.GetComponentInChildren<ObjectComponents>() != null)
        {
            Destroy(anchorPlacements.GetComponentInChildren<ObjectComponents>().gameObject);
        }
    }

    int currentMatNum = 1;

    public int GetMatNum()
    {
        return currentMatNum;
    }

    public void SetMatNum(int num)
    {
        currentMatNum = num;
    }

    public void ChangeMaterials(int num)
    {
        currentMatNum = num;

        if (TargetGoModel != null)
        {
            if (TargetGoModel.GetComponent<ObjectComponents>().objectVisual.name.Equals("sofa"))
            {
                SwitchMaterials(num, TargetGoModel.GetComponent<ObjectComponents>().objectRenderer);
            }
        }

        if (TargetGoAR != null)
        {
            if (TargetGoAR.GetComponent<ObjectComponents>().objectVisual.name.Equals("sofa"))
            {
                SwitchMaterials(num, TargetGoAR.GetComponent<ObjectComponents>().objectRenderer);
            }
        }
    }

    public void SwitchMaterials(int num, MeshRenderer meshRenderer)
    {
        switch (num)
        {
            case 1:
                meshRenderer.materials = new Material[]
                {
                    Resources.Load("sofa1") as Material,
                    Resources.Load("pillow1") as Material
                };
                break;
            case 2:
                meshRenderer.materials = new Material[]
                {
                    Resources.Load("sofa2") as Material,
                    Resources.Load("pillow2") as Material
                };
                break;
            case 3:
                meshRenderer.materials = new Material[]
                {
                    Resources.Load("sofa1") as Material,
                    Resources.Load("pillow2") as Material
                };
                break;
            case 4:
                meshRenderer.materials = new Material[]
                {
                    Resources.Load("sofa2") as Material,
                    Resources.Load("pillow1") as Material
                };
                break;
        }
    }

    public void InstantiateAR(string ObjectID)
    {
        ProcessObject(ObjectID, true);
        planeManager.TurnOnOffBehaviours(true);
    }

    public void InstantiateModel(string ObjectID)
    {
        ProcessObject(ObjectID, false);
        planeManager.TurnOnOffBehaviours(false);
    }

    void ProcessObject(string ObjectLink, bool ARmode)
    {
        GC.Collect();

        if (ObjectReference.ContainsKey(ObjectLink))
        {
            string PrefabAssetName = string.Empty;
            PrefabAssetName = ObjectLink.Replace("http://galaxybattles.deekie.com/walkint-i/", "");
            PrefabAssetName = PrefabAssetName.Replace("android/", "");
            PrefabAssetName = PrefabAssetName.Replace("ios/", "");
            PrefabAssetName = PrefabAssetName.Replace("objects/", "");
            PrefabAssetName = PrefabAssetName.Replace(".assetbundle", "");
            PrefabAssetName += ".prefab";

            Debug.Log("PrefabAssetName : " + PrefabAssetName);

            foreach (string AssetName in ObjectReference[ObjectLink].GetAllAssetNames())
            {
                print("AssetName : " + AssetName);
            }

            GameObject TargetPrefab = ObjectReference[ObjectLink].LoadAsset(PrefabAssetName) as GameObject;

            if (TargetPrefab == null)
            {
                Debug.Log("TargetPrefab == null");
            }
            else
            {
                if (ARmode)
                {
                    if (TargetGoAR != null)
                    {
                        TargetGoAR.transform.parent = anchorPlacements.transform;
                    }

                    if (anchorPlacements.GetComponentInChildren<ObjectComponents>() != null)
                    {
                        Destroy(anchorPlacements.GetComponentInChildren<ObjectComponents>().gameObject);
                    }
                }
                else
                {
                    if (modelHolder.GetComponentInChildren<ObjectComponents>() != null)
                    {
                        Destroy(modelHolder.GetComponentInChildren<ObjectComponents>().gameObject);
                    }
                }

                if (ARmode)
                {
                    TargetGoAR = Instantiate(TargetPrefab, anchorPlacements.transform);
                    TargetGoAR.transform.localScale = Vector3.one;

                    ObjectComponents objectComponents = TargetGoAR.GetComponent<ObjectComponents>();
                    planeManager.placementAugmentation = objectComponents.objectSelf;
                    planeManager.productPlacement.augmentationObject = objectComponents.objectSelf;
                    planeManager.productPlacement.pivot = objectComponents.objectPivot;
                    planeManager.productPlacement.holder = objectComponents.objectHolder;
                    planeManager.productPlacement.visual = objectComponents.objectVisual;
                    planeManager.productPlacement.boxCollider = objectComponents.objectBoxCollider;
                    planeManager.productPlacement.objectRenderer = objectComponents.objectRenderer;
                    planeManager.productPlacement.objectShadow = objectComponents.objectShadow;
                    planeManager.productPlacement.animator = objectComponents.objectAnimator;
                    touchHandler.augmentationObject = objectComponents.objectSelf.transform;

                    planeManager.productPlacement.enabled = true;
                    planeManager.productPlacement.boxCollider.enabled = true;
                    planeManager.productPlacement.animator.enabled = true;
                    touchHandler.enabled = true;

                    //foreach (Material material in objectComponents.objectRenderer.materials)
                    //{
                    //    material.shader = Shader.Find(material.shader.name);
                    //}

                    //foreach (Material material in objectComponents.objectShadow.materials)
                    //{
                    //    material.shader = Shader.Find(material.shader.name);
                    //}

                    if (objectComponents.objectVisual.name.Equals("sofa"))
                    {
                        ChangeMaterials(currentMatNum);
                    }
                }
                else
                {
                    TargetGoModel = Instantiate(TargetPrefab, modelHolder.transform);
                    TargetGoModel.transform.localPosition = Vector3.zero;
                    TargetGoModel.transform.rotation = new Quaternion(0, 180, 0, 0);

                    ObjectComponents objectComponents = TargetGoModel.GetComponent<ObjectComponents>();
                    objectComponents.objectBoxCollider.enabled = false;
                    objectComponents.objectAnimator.enabled = false;

                    if (objectComponents.objectVisual.name.Equals("chair2"))
                    {
                        TargetGoModel.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                    }
                    else
                    {
                        TargetGoModel.transform.localScale = Vector3.one;
                    }

                    planeManager.productPlacement.objectRenderer.enabled = true;
                    planeManager.productPlacement.objectShadow.enabled = true;

                    UtilityHelper.EnableRendererColliderCanvas(TargetGoModel, true);

                    //foreach (Material material in objectComponents.objectRenderer.materials)
                    //{
                    //    material.shader = Shader.Find(material.shader.name);
                    //}

                    //foreach (Material material in objectComponents.objectShadow.materials)
                    //{
                    //    material.shader = Shader.Find(material.shader.name);
                    //}

                    if (objectComponents.objectVisual.name.Equals("sofa"))
                    {
                        ChangeMaterials(currentMatNum);
                    }
                }

                loadingPanel.SetActive(false);

                Debug.Log("Object loaded");
            }
        }
        else
        {
            if (LoadingReferenceTable.Contains(ObjectLink))
            {

            }
            else
            {
                LoadingReferenceTable.Add(ObjectLink);

                StartCoroutine(LoadObject(ObjectLink, ARmode));
            }
        }
        //FractionController.UpdateFraction (PreviewButton.GetSiblingIndex () + 1, PreviewButton.parent.childCount);
    }

    IEnumerator LoadObject(string ObjectLink, bool ARmode)
    {
        while (!Caching.ready)
        {
            yield return null;
        }

        loadingPanel.SetActive(true);

        WWW ObjectRequest = WWW.LoadFromCacheOrDownload(ObjectLink, GetAssetVersion);

        while (!ObjectRequest.isDone)
        {
            loadingBar.fillAmount = ObjectRequest.progress;
            loadingProgress.text = Mathf.RoundToInt((ObjectRequest.progress / 1.0f * 100.0f)).ToString() + "%";
            yield return null;
        }

        loadingBar.fillAmount = 1;
        loadingProgress.text = "100%";

        loadingPanel.SetActive(false);

        if (!string.IsNullOrEmpty(ObjectRequest.error))
        {
            Debug.LogError(ObjectRequest.error);
        }
        else
        {
            AssetBundle RequestedAssetBundle = ObjectRequest.assetBundle;

            ObjectReference[ObjectLink] = RequestedAssetBundle;

            ProcessObject(ObjectLink, ARmode);

            LoadingReferenceTable.Remove(ObjectLink);
        }
    }

    //public void InstantiateMat(string MatID, int MatNum)
    //{
        
    //}

    //void ProcessMaterial(string MatLink, int MatNum)
    //{
    //    GC.Collect();

    //    if (ObjectReference.ContainsKey(MatLink))
    //    {
    //        string MatAssetName = string.Empty;
    //        MatAssetName = MatLink.Replace("http://galaxybattles.deekie.com/walkint-i/", "");
    //        MatAssetName = MatAssetName.Replace("android/", "");
    //        MatAssetName = MatAssetName.Replace("ios/", "");
    //        MatAssetName = MatAssetName.Replace("objects/", "");
    //        MatAssetName = MatAssetName.Replace(".assetbundle", "");
    //        //MatAssetName += ".mat";

    //        Debug.Log("MatAssetName : " + MatAssetName);

    //        foreach (string AssetName in ObjectReference[MatLink].GetAllAssetNames())
    //        {
    //            print("AssetName : " + AssetName);
    //        }

    //        Material[] GetMats = new Material[MatNum];
    //        Array.Clear(TargetMats, 0, TargetMats.Length);
    //        TargetMats = null;

    //        for (int i = 0; i <= MatNum; i++)
    //        {
    //            GetMats[i] = ObjectReference[MatLink].LoadAsset(MatAssetName + (i+1) + ".mat") as Material;
    //        }
    //        //Material TargetMat = ObjectReference[MatLink].LoadAsset(MatAssetName) as Material;

    //        Array.Copy(GetMats, TargetMats, GetMats.Length);

    //        if (TargetMats == null)
    //        {
    //            Debug.Log("TargetMats == null");
    //        }
    //        else
    //        {
                
    //            loadingPanel.SetActive(false);

    //            Debug.Log("Materials loaded");
    //        }
    //    }
    //    else
    //    {
    //        if (LoadingReferenceTable.Contains(MatLink))
    //        {

    //        }
    //        else
    //        {
    //            LoadingReferenceTable.Add(MatLink);

    //            StartCoroutine(LoadMat(MatLink, MatNum));
    //        }
    //    }
    //    //FractionController.UpdateFraction (PreviewButton.GetSiblingIndex () + 1, PreviewButton.parent.childCount);
    //}

    //IEnumerator LoadMat(string MatLink, int MatNum)
    //{
    //    while (!Caching.ready)
    //    {
    //        yield return null;
    //    }

    //    loadingPanel.SetActive(true);

    //    WWW MatRequest = WWW.LoadFromCacheOrDownload(MatLink, GetAssetVersion);

    //    while (!MatRequest.isDone)
    //    {
    //        loadingBar.fillAmount = MatRequest.progress;
    //        loadingProgress.text = Mathf.RoundToInt((MatRequest.progress / 1.0f * 100.0f)).ToString() + "%";
    //        yield return null;
    //    }

    //    loadingBar.fillAmount = 1;
    //    loadingProgress.text = "100%";

    //    loadingPanel.SetActive(false);

    //    if (!string.IsNullOrEmpty(MatRequest.error))
    //    {
    //        Debug.LogError(MatRequest.error);
    //    }
    //    else
    //    {
    //        AssetBundle RequestedAssetBundle = MatRequest.assetBundle;

    //        ObjectReference[MatLink] = RequestedAssetBundle;

    //        ProcessMaterial(MatLink, MatNum);

    //        LoadingReferenceTable.Remove(MatLink);
    //    }
    //}

    [System.Serializable]
    public class SerializablePreviewProperties
    {
        public int Version;
    }

    SerializablePreviewProperties SerializablePreviewPropertiesObject = new SerializablePreviewProperties();

    IEnumerator LoadVersion()
    {
        UnityWebRequest GetAssetConfigurationRequest = UnityWebRequest.Get(ConfigurationRequestURL);

        yield return GetAssetConfigurationRequest.SendWebRequest();

        if (GetAssetConfigurationRequest.isNetworkError || GetAssetConfigurationRequest.isHttpError)
        {
            Debug.Log(GetAssetConfigurationRequest.error);
        }
        else
        {
            Debug.Log("WWW.Response : " + GetAssetConfigurationRequest.downloadHandler.text);
            int UpdatedVersion = JsonUtility.FromJson<SerializablePreviewProperties>(GetAssetConfigurationRequest.downloadHandler.text).Version;

            if (UpdatedVersion > PlayerPrefs.GetInt(AssetsVersion, 0))
            {
                if (Caching.ClearCache())
                {
                    Debug.Log("ClearCache (), updating to latest assets");
                    PlayerPrefs.SetInt(AssetsVersion, UpdatedVersion);
                    PlayerPrefs.Save();
                }
                else
                {
                    Debug.LogError("ClearCache () failed");
                }
            }
            else
            {
                Debug.Log("Assets up to date. [" + UpdatedVersion + "]");
            }
        }
    }

    public int GetAssetVersion
    {
        get
        {
            return PlayerPrefs.GetInt(AssetsVersion, 0);
        }
    }
}