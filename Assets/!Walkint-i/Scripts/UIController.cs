﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    [SerializeField] GameObject SplashScreen;
    [SerializeField] GameObject StartVideo;
    [SerializeField] GameObject StartPanel;

    [Header("Search")]
    [SerializeField] GameObject Back1;
    [SerializeField] GameObject Back2;
    [SerializeField] GameObject Panel1;

	void Start ()
    {
        StartCoroutine(StartSplash());
	}

    public IEnumerator StartSplash()
    {
        StartPanel.SetActive(false);
        SplashScreen.SetActive(true);
        yield return new WaitForSeconds(1);
        SplashScreen.SetActive(false);
        StartVideo.SetActive(true);
        StartPanel.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        StartPanel.GetComponentInChildren<BottomSidePage>().GetPage(true);
    }

    public void CloseVideo()
    {
        if (StartPanel.GetComponentInChildren<BottomSidePage>().gameObject.GetComponent<Image>().isActiveAndEnabled == false)
        {
            StartPanel.GetComponentInChildren<BottomSidePage>().gameObject.GetComponent<Image>().enabled = true;
        }

        if (StartVideo.activeInHierarchy)
        {
            StartVideo.SetActive(false);
        }
    }

    void Update ()
    {
		if (Panel1.activeInHierarchy)
        {
            Back1.SetActive(true);
            Back2.SetActive(false);
        }
        else
        {
            Back1.SetActive(false);
            Back2.SetActive(true);
        }
	}
}
