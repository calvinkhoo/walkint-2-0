﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductPageTest : MonoBehaviour
{
    public GetPointer getPointer;
    public GameObject page;
    public GameObject partA;
    public GameObject partB;

    bool getActivate = true;
    public GameObject material1;

    float updateHeight = 0;

    // Movement speed in units/sec.
    float speed = 3600f;

    // Time when the movement started.
    private float startTime;

    private void Start()
    {
        page.transform.localPosition = Vector2.zero;

        partA.GetComponent<LayoutElement>().preferredHeight = partA.GetComponentInParent<Canvas>().gameObject.GetComponent<RectTransform>().sizeDelta.y - 288.0f;
        updateHeight = partA.GetComponent<LayoutElement>().preferredHeight;

        partB.GetComponent<LayoutElement>().preferredHeight = partA.GetComponentInParent<Canvas>().gameObject.GetComponent<RectTransform>().sizeDelta.y;
    }

    private void Update()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / 1080f;

        // Set our position as a fraction of the distance between the markers.
        if (!getActivate)
        {
            if (!page.transform.localPosition.Equals(new Vector2(0, updateHeight)))
            {
                page.transform.localPosition = Vector2.Lerp(Vector2.zero, new Vector2(0, updateHeight), fracJourney);
            }
        }
        else
        {
            if (!page.transform.localPosition.Equals(Vector2.zero))
            {
                page.transform.localPosition = Vector2.Lerp(new Vector2(0, updateHeight), Vector2.zero, fracJourney);
            }
        }

#if UNITY_EDITOR
        if (getPointer.GetBoolean())
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (getActivate)
                {
                    GetPage(false);
                }
                else
                {
                    GetPage(true);
                }
            }
        }
#endif
    }

    public bool GetmenuActivate()
    {
        return getActivate;
    }

    public void GetPage(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
    }

    public void ChangeMatSize(int num)
    {
        foreach (Image images in material1.GetComponentsInChildren<Image>())
        {
            if (images.gameObject.name.Equals("Image" + num.ToString()))
            {
                images.rectTransform.sizeDelta = new Vector2(80, 80);
            }
            else
            {
                images.rectTransform.sizeDelta = new Vector2(60, 60);
            }
        }
    }

    public void ResetMatSize()
    {
        foreach (Image images in material1.GetComponentsInChildren<Image>())
        {
            if (images.gameObject.name.Equals("Image1"))
            {
                images.rectTransform.sizeDelta = new Vector2(80, 80);
            }
            else
            {
                images.rectTransform.sizeDelta = new Vector2(60, 60);
            }
        }
    }
}
