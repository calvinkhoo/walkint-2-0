﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SearchQuery : MonoBehaviour
{
    SearchList searchList = new SearchList();

    [SerializeField] Transform searchSuggestion;
    [SerializeField] InputField inputField;
    bool searching;

    public void Start ()
    {
        string SearchResult = File.ReadAllText(Application.dataPath + "/Resources/SearchList.txt");
        searchList = JsonUtility.FromJson<SearchList>(SearchResult);
    }

    public void Update()
    {
        if (searching)
        {
            if (inputField.text.Equals(""))
            {
                foreach (Transform child in searchSuggestion)
                {
                    if (!child.name.Equals("Panel"))
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
        }
        else
        {
            if (searchSuggestion.childCount > 1)
            {
                foreach (Transform child in searchSuggestion)
                {
                    if (!child.name.Equals("Panel"))
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
        }
    }

    public void Searching()
    {
        foreach (Transform child in searchSuggestion)
        {
            if (!child.name.Equals("Panel"))
            {
                Destroy(child.gameObject);
            }
        }

        if (!inputField.text.Equals(""))
        {
            string search = inputField.text.ToLower();

            for (int i = 0; i < searchList.SearchResult.Count; i++)
            {
                if (searchList.SearchResult[i].StartsWith(search, StringComparison.CurrentCultureIgnoreCase))
                {
                    GameObject go = Instantiate((GameObject)Resources.Load("SearchResults"), searchSuggestion);
                    go.GetComponentInChildren<Text>().text = "<color=#808080>" + FirstCharToUpper(inputField.text) + "</color>" + searchList.SearchResult[i].Substring(inputField.text.Length);
                }
            }
        }
    }

    public void Search(bool activate)
    {
        searching = activate;
    }

    public string FirstCharToUpper(string input)
    {
        char[] newChars = input.ToCharArray();

        return newChars[0].ToString().ToUpper() + input.Substring(1);
    }

    public struct SearchList
    {
        public List<string> SearchResult;
    }
}
