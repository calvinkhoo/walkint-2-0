﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SortPage : MonoBehaviour
{
    [SerializeField] GameObject FilterPanel;

    Toggle[] toggles1;

    private void Start()
    {
        toggles1 = FilterPanel.GetComponentsInChildren<Toggle>();
    }

    public void SetAllToggleOff()
    {
        foreach (Toggle toggle1 in toggles1)
        {
            toggle1.isOn = false;
        }
    }
}
