﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class ScrollRectEx : ScrollRect
{
    private bool routeToParent = false;

    /// <summary>
    /// Do action for all parents
    /// </summary>
    private void DoForParents<T>(Action<T> action) where T : IEventSystemHandler
    {
        Transform parent = transform.parent;
        while (parent != null)
        {
            foreach (var component in parent.GetComponents<Component>())
            {
                if (component is T)
                    action((T)(IEventSystemHandler)component);
            }
            parent = parent.parent;
        }
    }

    /// <summary>
    /// Always route initialize potential drag event to parents
    /// </summary>
    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
        DoForParents<IInitializePotentialDragHandler>((parent) => { parent.OnInitializePotentialDrag(eventData); });
        base.OnInitializePotentialDrag(eventData);
    }

    /// <summary>
    /// Drag event
    /// </summary>
    public override void OnDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (routeToParent)
            DoForParents<IDragHandler>((parent) => { parent.OnDrag(eventData); });
        else
            base.OnDrag(eventData);
    }

    /// <summary>
    /// Begin drag event
    /// </summary>
    public override void OnBeginDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (!horizontal && Math.Abs(eventData.delta.x) > Math.Abs(eventData.delta.y))
            routeToParent = true;
        else if (!vertical && Math.Abs(eventData.delta.x) < Math.Abs(eventData.delta.y))
            routeToParent = true;
        else
            routeToParent = false;

        if (routeToParent)
            DoForParents<IBeginDragHandler>((parent) => { parent.OnBeginDrag(eventData); });
        else
            base.OnBeginDrag(eventData);
    }

    /// <summary>
    /// End drag event
    /// </summary>
    public override void OnEndDrag(UnityEngine.EventSystems.PointerEventData eventData)
    {
        if (routeToParent)
            DoForParents<IEndDragHandler>((parent) => { parent.OnEndDrag(eventData); });
        else
            base.OnEndDrag(eventData);
        routeToParent = false;
    }

    bool getActivate = false;
    float currentPosition = 0;
    float targetPosition = 0;
    float speed = 5000;
    float startTime;

    public void GetPosition1(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
        currentPosition = horizontalNormalizedPosition;
        targetPosition = 0;
    }

    public void GetPosition2(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
        currentPosition = horizontalNormalizedPosition;
        targetPosition = 0.25f;
    }

    public void GetPosition3(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
        currentPosition = horizontalNormalizedPosition;
        targetPosition = 0.5f;
    }

    public void GetPosition4(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
        currentPosition = horizontalNormalizedPosition;
        targetPosition = 0.75f;
    }

    public void GetPosition5(bool activate)
    {
        getActivate = activate;
        startTime = Time.time;
        currentPosition = horizontalNormalizedPosition;
        targetPosition = 1;
    }

    private void Update()
    {
        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / 500;

        // Set our position as a fraction of the distance between the markers.
        if (getActivate)
        {
            if (!horizontalNormalizedPosition.Equals(targetPosition))
            {
                horizontalNormalizedPosition = Mathf.Lerp(currentPosition, targetPosition, fracJourney);
            }
            else
            {
                getActivate = false;
            }
        }
        else
        {

        }
    }
}
