﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectComponents : MonoBehaviour 
{
    public GameObject objectSelf;
    public Transform objectPivot;
    public Transform objectHolder;
    public Transform objectVisual;
    public BoxCollider objectBoxCollider;
    public MeshRenderer objectRenderer;
    public MeshRenderer objectShadow;
    public Animator objectAnimator;
}
