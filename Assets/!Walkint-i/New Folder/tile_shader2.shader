 Shader "Custom/StandardTile2" {
     Properties {
         _Color ("Color", Color) = (1,1,1,1)
         _MainTex ("Albedo (RGB)", 2D) = "white" {}
         _BumpScale1("Scale 1", Float) = 1.0
         _BumpMap1 ("Normal Map 1", 2D) = "bump1" {}
         _BumpScale2("Scale 2", Float) = 1.0
         _BumpMap2 ("Normal Map 2", 2D) = "bump2" {}
         _Glossiness ("Smoothness", Range(0,1)) = 0.5
         _Metallic ("Metallic", Range(0,1)) = 0.0
         _Spec ("Specular Map", 2D) = "white" {}
         _AO ("Occlusion Intensity", Float) = 1.0
         _AOMap ("Occlusion Map", 2D) = "white" {}
         _EmitCol ("Emission Color", Color) = (1,1,1,1)
         _EmitInt ("Emission Intensity", Float) = 0.0
         _EmissionMap ("Emission", 2D) = "white" {}
     }
     SubShader {
         Tags { "RenderType"="Opaque" }
         LOD 200
         
         CGPROGRAM
         // Physically based Standard lighting model, and enable shadows on all light types
         #pragma surface surf Standard fullforwardshadows
 
         // Use shader model 3.0 target, to get nicer looking lighting
         #pragma target 3.0
 
         sampler2D _MainTex;
         sampler2D _BumpMap1;
         sampler2D _BumpMap2;
         sampler2D _Spec;
         sampler2D _AOMap;
         sampler2D _EmissionMap;
         float _BumpScale1;
         float _BumpScale2;
 
         struct Input {
             float2 uv_MainTex;
             float2 uv_BumpMap1;
             float2 uv_BumpMap2;
             float2 uv_Spec;
             float2 uv_AOMap;
             float2 uv_EmissionMap;
         };
 
         half _Glossiness;
         half _Metallic;
         half _AO;
         half _EmitInt;
         fixed4 _Color;
         fixed4 _EmitCol;
         
         fixed3 _Normal1;
         fixed3 _Normal2;
  
         void surf (Input IN, inout SurfaceOutputStandard o) {
             _Normal1 = UnpackNormal (tex2D (_BumpMap1, IN.uv_BumpMap1)) * _BumpScale1;
             _Normal2 = UnpackNormal (tex2D (_BumpMap2, IN.uv_BumpMap2)) * _BumpScale2;
             o.Normal = _Normal1 + _Normal2;
             // Albedo comes from a texture tinted by color
             fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
             fixed occlusion = pow (tex2D (_AOMap, IN.uv_AOMap).r, _AO);
             //c.rgb *= occlusion;
             o.Albedo = c.rgb;
             // Metallic and smoothness come from slider variables
             fixed4 spec = tex2D (_Spec, IN.uv_Spec);
             o.Metallic = _Metallic * spec.r;
             o.Smoothness = _Glossiness * spec.a;
             o.Occlusion = occlusion;
             o.Emission = tex2D (_EmissionMap, IN.uv_EmissionMap).rgb * _EmitInt * _EmitCol;
             o.Alpha = c.a;
         }
         ENDCG
     }
     FallBack "Diffuse"
 }