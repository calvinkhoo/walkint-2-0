﻿/// <summary>
/// Scroll rect mod001.
/// 
/// Function : Allows content to snap to each individual element.
/// </summary>

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace BMJ.UnityUIMods {
	public class ScrollRect_Mod001 : ScrollRect {
		public enum ScrollAlignment {
			NONE = 0,
			HORIZONTAL = 1,
			VERTICAL = 2,
			FREE = 3
		}
				
		ScrollAlignment SetScrollAlignment = ScrollAlignment.NONE;

		List<GameObject> Outlines = new List<GameObject> ();

		Vector2 SnapVector = Vector2.zero;

		public Vector2 SetSnapVector {
			set { 
				//Debug.Log ("Set Snap Vector : " + value);
				SnapVector = value;
			}
		}

		bool IsDragging = false;

		protected override void Start () {
			if (horizontal) {
				if (vertical) {
					SetScrollAlignment = ScrollAlignment.FREE;
				} else {
					SetScrollAlignment = ScrollAlignment.HORIZONTAL;
				}
			} else {
				if (vertical) {
					SetScrollAlignment = ScrollAlignment.VERTICAL;
				} else {
					SetScrollAlignment = ScrollAlignment.FREE;
				}
			}

			switch (SetScrollAlignment) {
			case ScrollAlignment.NONE:
				SnapVector = Vector2.zero;
				break;
			case ScrollAlignment.HORIZONTAL:
				SnapVector = Vector2.zero;
				break;
			case ScrollAlignment.VERTICAL:
				SnapVector = Vector2.up;
				break;
			case ScrollAlignment.FREE:
				SnapVector = Vector2.one * 0.5f;
				break;
			}

			base.Start ();
		}

		protected override void OnEnable () {
			base.OnEnable ();

			Outlines.Clear ();

			for (int a = 0; a < content.childCount; a++) {
				Outlines.Add (content.GetChild (a).Find ("Outline").gameObject);
			}
		}

		public override void OnBeginDrag (UnityEngine.EventSystems.PointerEventData eventData) {
			base.OnBeginDrag (eventData);

			IsDragging = true;
		}

		public override void OnEndDrag (UnityEngine.EventSystems.PointerEventData eventData) {
			base.OnEndDrag (eventData);

			if (horizontal) {
				SnapVector.x = Mathf.Clamp (Mathf.RoundToInt (normalizedPosition.x * (content.childCount - 1)) / (float)(content.childCount - 1), 0.0f, 1.0f);
			} else {
				SnapVector.x = 0.0f;
			}

			if (vertical) {
				SnapVector.y = Mathf.Clamp (Mathf.RoundToInt (normalizedPosition.y * (content.childCount - 1)) / (float)(content.childCount - 1), 0.0f, 1.0f);
			} else {
				SnapVector.y = 0.0f;
			}

			IsDragging = false;
		}

		protected override void LateUpdate () {
			base.LateUpdate ();

			if (IsDragging) {
				// DO NOTHING
			} else {
				switch (SetScrollAlignment) {
				case ScrollAlignment.HORIZONTAL:
					normalizedPosition = Vector2.right * normalizedPosition.x;
					normalizedPosition = Vector2.MoveTowards (normalizedPosition, SnapVector, 1.0f);
					break;
				case ScrollAlignment.VERTICAL:
					normalizedPosition = Vector2.up * normalizedPosition.y;
					normalizedPosition = Vector2.MoveTowards (normalizedPosition, SnapVector, 1.0f);
					break;
				default:
					break;
				}
				normalizedPosition = Vector2.MoveTowards (normalizedPosition, SnapVector, 1.0f);

				//Debug.Log ("Snapping : " + normalizedPosition + " > " + SnapVector);

				Outlines.RemoveAll (ParsedOutline => ParsedOutline == null);

				foreach (GameObject Outline in Outlines) {
					if (Outline == null) {
					} else {
						Outline.SetActive (false);
					}
				}

				int TargetOutlineIndex = Mathf.Clamp (Mathf.RoundToInt (normalizedPosition.x * (content.childCount - 1)), 0, Outlines.Count - 1);

				if (TargetOutlineIndex < Outlines.Count) {
					Outlines [TargetOutlineIndex].SetActive (true);
				}
			}
		}
	}
}