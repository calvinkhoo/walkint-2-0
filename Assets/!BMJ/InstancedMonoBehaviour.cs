﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace BMJ.Utilities {
	/// <summary>
	/// Do not define Awake () and Start () if inheriting from this, use OnInitialize () and OnStart () instead.
	/// </summary>
	public class InstancedMonoBehaviour<T> : MonoBehaviour where T : InstancedMonoBehaviour<T> {
		static T Instance;

		public static T GetInstance {
			get {
				return Instance;
			}
		}

		void Awake () {
			Initialize ();
		}

		void Initialize () {
			if (GetInstance == null) {
				Instance = gameObject.GetComponent<T> ();
				DontDestroyOnLoad (gameObject);
				OnInitialize ();
			} else {
				Destroy (gameObject);
			}
		}

		void Start () {
			OnStart ();
		}

		protected virtual void OnStart () {
			
		}

		protected virtual void OnInitialize () {
			
		}
	}
}
