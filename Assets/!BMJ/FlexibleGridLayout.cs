﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace BMJ.UnityUIMods {
	[ExecuteInEditMode]
	public class FlexibleGridLayout : GridLayoutGroup {
		Vector2 TargetCellSize = Vector3.one;

		protected void Update () {
			TargetCellSize = cellSize;

			switch (constraint) {
			case Constraint.Flexible:
				break;
			case Constraint.FixedColumnCount:
				TargetCellSize.x = rectTransform.rect.size.x / constraintCount;
				TargetCellSize.y = rectTransform.rect.size.y / Mathf.CeilToInt ((float)transform.childCount / constraintCount);
				break;
			case Constraint.FixedRowCount:
				TargetCellSize.y = rectTransform.rect.size.y / constraintCount;
				TargetCellSize.x = rectTransform.rect.size.x / Mathf.CeilToInt ((float)transform.childCount / constraintCount);
				break;
			}

			cellSize = TargetCellSize;
		}
	}
}