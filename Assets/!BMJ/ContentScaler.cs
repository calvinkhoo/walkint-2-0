﻿/// <summary>
/// Aspect ratio fitter mod001.
/// 
/// Function : Automatically adjusts the current aspect ratio to match the amount of children parented to the transform.
/// </summary>

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace BMJ.UnityUIMods {
	[ExecuteInEditMode]
	public class ContentScaler : MonoBehaviour {
		[SerializeField] Alignment SetAlignment = Alignment.NONE;

		protected void Update () {
			switch (SetAlignment) {
			case Alignment.NONE:
				break;
			case Alignment.HORIZONTAL:
				transform.localScale = Vector3.right * (transform.childCount) + Vector3.up + Vector3.forward;
				break;
			case Alignment.VERTICAL:
				transform.localScale = Vector3.right + Vector3.up * (transform.childCount) + Vector3.forward;
				break;
			case Alignment.ENVELOP:
				transform.localScale = Vector3.right * (transform.childCount) + Vector3.up * (transform.childCount) + Vector3.forward;
				break; 
			}
		}
	}

	public enum Alignment {
		NONE = 0,
		HORIZONTAL = 1,
		VERTICAL = 2,
		ENVELOP = 3 
	}
}
