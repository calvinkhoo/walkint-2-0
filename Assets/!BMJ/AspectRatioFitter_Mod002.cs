﻿/// <summary>
/// Aspect ratio fitter mod001.
/// 
/// Function : Automatically adjusts the current aspect ratio to match the amount of children parented to the transform.
/// </summary>

using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace BMJ.UnityUIMods {
	[ExecuteInEditMode]
	public class AspectRatioFitter_Mod002 : AspectRatioFitter {
		bool Horizontal = false;
		bool Vertical = false;

		protected override void Update () {
			base.Update ();

			Horizontal = !(gameObject.GetComponent<HorizontalLayoutGroup> () == null);
			Vertical = !(gameObject.GetComponent<VerticalLayoutGroup> () == null);

			if (Horizontal) {
				if (Vertical) {
					Debug.LogWarning ("Horizontal and Vertical layout groups found, behaviour undefined.");
				} else {
					ProcessAspectRatio (true);
				}
			} else if (Vertical) {
				ProcessAspectRatio (false);
			} else {
				Debug.LogWarning ("No layout groups found, behaviour undefined.");
			}
		}

		void ProcessAspectRatio (bool ProcessHorizontal) {
			switch (aspectMode) {
			case AspectMode.WidthControlsHeight:
				if (ProcessHorizontal) {
					Debug.LogWarning ("AspectMode [" + aspectMode + "] with HorizontalLayoutGroup, behaviour undefined.");
				} else {
					aspectRatio = transform.childCount;
				}
				break;
			case AspectMode.HeightControlsWidth:
				if (ProcessHorizontal) {
					aspectRatio = 1.0f / transform.childCount;
				} else {
					Debug.LogWarning ("AspectMode [" + aspectMode + "] with VerticalLayoutGroup, behaviour undefined.");
				}
				break;
			default:
				Debug.LogWarning ("Unsupported aspectMode, behaviour undefined.");
				break;
			}
		}
	}
}
