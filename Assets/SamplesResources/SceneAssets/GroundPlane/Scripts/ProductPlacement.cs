﻿/*============================================================================== 
Copyright (c) 2018 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other 
countries.   
==============================================================================*/

using System.Collections;
using UnityEngine;
using Vuforia;

public class ProductPlacement : MonoBehaviour
{
    #region PUBLIC_MEMBERS
    public bool IsPlaced { get; set; }
    #endregion // PUBLIC_MEMBERS

    #region PRIVATE_MEMBERS
    [Header("Augmentation Objects")]
    public GameObject augmentationObject;
    public Transform pivot;
    public Transform holder;
    public Transform visual;
    public BoxCollider boxCollider;
    public MeshRenderer objectRenderer;
    public MeshRenderer objectShadow;
    public Animator animator;

    [Header("Augmentation Size")]
    [Range(0.1f, 2.0f)]
    [SerializeField] float productSize = 1.0f;

    public bool selecting = true;

    GroundPlaneUI groundPlaneUI;
    Camera mainCamera;
    Ray cameraToPlaneRay;
    RaycastHit cameraToPlaneHit;
    Ray cameraToObjectRay;
    RaycastHit cameraToObjectHit;

    Vector3 productScale;
    Vector3 LocalOffset;
    Vector3 targetPosition;

    float augmentationScale;
    string floorName;
    bool hold = false;
    bool tap = true;

    // Property which returns whether chair visibility conditions are met
    bool ChairVisibilityConditionsMet
    {
        // The Chair should only be visible if the following conditions are met:
        // 1. Tracking Status is Tracked or Limited
        // 2. Ground Plane Hit was received for this frame
        // 3. The Plane Mode is equal to PLACEMENT
        get
        {
            return
                PlaneManager.TrackingStatusIsTrackedOrLimited &&
                PlaneManager.GroundPlaneHitReceived &&
                (PlaneManager.CurrentPlaneMode == PlaneManager.PlaneMode.PLACEMENT);
        }
    }
    #endregion // PRIVATE_MEMBERS


    #region MONOBEHAVIOUR_METHODS
    void Start()
    {
        this.mainCamera = Camera.main;
        this.groundPlaneUI = FindObjectOfType<GroundPlaneUI>();

        SetupFloor();

        this.augmentationScale = VuforiaRuntimeUtilities.IsPlayMode() ? 0.5f : this.productSize;

        this.productScale =
            new Vector3(this.augmentationScale,
                        this.augmentationScale,
                        this.augmentationScale);

        this.augmentationObject.transform.localScale = this.productScale;
    }


    void Update()
    {
        if (PlaneManager.CurrentPlaneMode == PlaneManager.PlaneMode.PLACEMENT)
        {
            if (!this.IsPlaced)
                UtilityHelper.RotateTowardCamera(this.augmentationObject);
        }

        if (selecting)
        {
            if (PlaneManager.CurrentPlaneMode == PlaneManager.PlaneMode.PLACEMENT && this.IsPlaced)
            {
                this.animator.enabled = true;

                this.boxCollider.enabled = false;

                if (TouchHandler.IsSingleFingerDragging || TouchHandler.IsSingleFingerStationary || (VuforiaRuntimeUtilities.IsPlayMode() && Input.GetMouseButton(0)))
                {
                    if (!this.groundPlaneUI.IsCanvasButtonPressed())
                    {
                        tap = false;

                        this.boxCollider.enabled = true;

                        this.cameraToObjectRay = this.mainCamera.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(this.cameraToObjectRay, out this.cameraToObjectHit))
                        {
                            if (this.cameraToObjectHit.collider.gameObject == this.visual.gameObject)
                            {
                                this.boxCollider.enabled = false;

                                this.cameraToPlaneRay = this.mainCamera.ScreenPointToRay(Input.mousePosition);

                                if (Physics.Raycast(this.cameraToPlaneRay, out this.cameraToPlaneHit))
                                {
                                    this.pivot.position = this.cameraToPlaneHit.point;
                                    this.visual.parent = this.pivot;

                                    if (hold)
                                    {
                                    }
                                    else
                                    {
                                        this.LocalOffset = this.pivot.localPosition;
                                        hold = true;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (hold)
                    {
                        if (Vector3.Distance(pivot.position, this.augmentationObject.transform.position) > 0.05f)
                        {
                            this.LocalOffset.y = 0.0f;
                            //this.augmentationObject.transform.position = this.visual.position;
                            targetPosition = this.visual.position;
                            this.visual.parent = this.holder;
                        }
                        this.augmentationObject.transform.position = Vector3.Lerp(this.augmentationObject.transform.position, targetPosition, 1000);
                        this.pivot.localPosition = Vector3.zero;
                        this.visual.localPosition = Vector3.zero;
                        hold = false;
                        tap = true;
                    }
                }

                if (tap)
                {
                    this.boxCollider.enabled = true;

                    if (!this.groundPlaneUI.IsCanvasButtonPressed())
                    {
                        this.cameraToObjectRay = this.mainCamera.ScreenPointToRay(Input.mousePosition);

                        if (Physics.Raycast(this.cameraToObjectRay, out this.cameraToObjectHit))
                        {
                            if (this.cameraToObjectHit.collider.gameObject == this.visual.gameObject)
                            {
                                if (TouchHandler.DoubleTap) //(Input.touchCount == 1 || Input.GetMouseButton(0))
                                {
                                    if (!this.groundPlaneUI.IsCanvasButtonPressed())
                                    {
                                        StartCoroutine(WaitForSecondsOff(0.2f));
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        else
        {
            this.animator.SetBool("Placed", true);

            this.boxCollider.enabled = true;

            if (!this.groundPlaneUI.IsCanvasButtonPressed())
            {
                this.cameraToObjectRay = this.mainCamera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(this.cameraToObjectRay, out this.cameraToObjectHit))
                {
                    if (this.cameraToObjectHit.collider.gameObject == this.visual.gameObject)
                    {
                        if (TouchHandler.DoubleTap)
                        {
                            if (!this.groundPlaneUI.IsCanvasButtonPressed())
                            {
                                StartCoroutine(WaitForSecondsOn(0.2f));
                            }
                        }

                    }
                }
            }
        }
    }

    void LateUpdate()
    {
        if (!this.IsPlaced)
        {
            SetVisible(this.ChairVisibilityConditionsMet);
        }
    }
    #endregion // MONOBEHAVIOUR_METHODS


    #region PUBLIC_METHODS

    IEnumerator WaitForSecondsOn(float time)
    {
        yield return new WaitForSeconds(time);
        selecting = true;
        this.animator.SetBool("Placed", false);
    }

    IEnumerator WaitForSecondsOff(float time)
    {
        yield return new WaitForSeconds(time);
        selecting = false;
    }

    public void SelectingOnOff(bool onoff)
    {
        selecting = onoff;
    }

    public void NotPlaced()
    {
        this.IsPlaced = false;
    }

    public void Reset()
    {
        this.augmentationObject.transform.position = Vector3.zero;
        this.augmentationObject.transform.localEulerAngles = Vector3.zero;
        this.augmentationObject.transform.localScale = this.productScale;
    }

    public void SetProductAnchor(Transform transform)
    {
        if (transform)
        {
            this.IsPlaced = true;
            this.augmentationObject.transform.SetParent(transform);
            this.augmentationObject.transform.localPosition = Vector3.zero;
            UtilityHelper.RotateTowardCamera(this.augmentationObject);
        }
        else
        {
            this.IsPlaced = false;
            this.augmentationObject.transform.SetParent(null);
        }
    }
    #endregion // PUBLIC_METHODS


    #region PRIVATE_METHODS
    void SetupFloor()
    {
        if (VuforiaRuntimeUtilities.IsPlayMode())
        {
            this.floorName = "Emulator Ground Plane";
        }
        else
        {
            this.floorName = "Floor";
            GameObject floor = new GameObject(this.floorName, typeof(BoxCollider));
            floor.transform.SetParent(this.augmentationObject.transform.parent);
            floor.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            floor.transform.localScale = Vector3.one;
            floor.GetComponent<BoxCollider>().size = new Vector3(1000f, 0.1f, 1000f);
        }
    }

    /// <summary>
    /// This method is used prior to chair being placed. Once placed, chair visibility is controlled
    /// by the DefaultTrackableEventHandler.
    /// </summary>
    /// <param name="visible">bool</param>
    public void SetVisible(bool visible)
    {
        // Set the visibility of the chair and it's shadow
        this.objectRenderer.enabled = this.objectShadow.enabled = visible;
    }
    #endregion // PRIVATE_METHODS

}
