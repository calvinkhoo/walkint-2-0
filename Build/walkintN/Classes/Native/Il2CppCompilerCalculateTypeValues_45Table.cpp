﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AboutScreenInfo
struct AboutScreenInfo_t2560997449;
// CameraSettings
struct CameraSettings_t3152619780;
// ColorChangedEvent
struct ColorChangedEvent_t3019780707;
// DrillController
struct DrillController_t3872647469;
// FadeObject
struct FadeObject_t1880495183;
// GroundPlaneUI
struct GroundPlaneUI_t2709539096;
// HSVChangedEvent
struct HSVChangedEvent_t911780251;
// MenuOptions
struct MenuOptions_t1951716431;
// PlaneManager
struct PlaneManager_t2021199913;
// ProductPlacement
struct ProductPlacement_t2927687625;
// RockPileController
struct RockPileController_t2893225296;
// SafeAreaManager
struct SafeAreaManager_t2083807765;
// System.Action
struct Action_t1264377477;
// System.Action`1<UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell>
struct Action_1_t1589570706;
// System.Action`1<UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell>
struct Action_1_t3383487634;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t898892918;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>>
struct Dictionary_2_t3417996176;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.Examples.Example01CellDto>
struct List_1_t872279796;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.Examples.Example02CellDto>
struct List_1_t1016655604;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.Examples.Example03CellDto>
struct List_1_t89911028;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.FancyScrollViewCell`2<UnityEngine.UI.Extensions.Examples.Example01CellDto,UnityEngine.UI.Extensions.FancyScrollViewNullContext>>
struct List_1_t2010330137;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.FancyScrollViewCell`2<UnityEngine.UI.Extensions.Examples.Example02CellDto,UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext>>
struct List_1_t1329068297;
// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.FancyScrollViewCell`2<UnityEngine.UI.Extensions.Examples.Example03CellDto,UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext>>
struct List_1_t815572830;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Func`2<System.Int32,UnityEngine.UI.Extensions.Examples.Example01CellDto>
struct Func_2_t603503607;
// System.Func`2<System.Int32,UnityEngine.UI.Extensions.Examples.Example02CellDto>
struct Func_2_t747879415;
// System.Func`2<System.Int32,UnityEngine.UI.Extensions.Examples.Example03CellDto>
struct Func_2_t4116102135;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.String
struct String_t;
// System.Timers.Timer
struct Timer_t1767341190;
// System.Void
struct Void_t1185182177;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// TouchHandler
struct TouchHandler_t3441426771;
// TrackableSettings
struct TrackableSettings_t2862243993;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Extensions.Accordion
struct Accordion_t2301662264;
// UnityEngine.UI.Extensions.AutoCompleteComboBox
struct AutoCompleteComboBox_t2765567798;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent
struct SelectionChangedEvent_t1822043360;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent
struct SelectionTextChangedEvent_t4051177638;
// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent
struct SelectionValidityChangedEvent_t954817928;
// UnityEngine.UI.Extensions.BoxSlider
struct BoxSlider_t3694973841;
// UnityEngine.UI.Extensions.BoxSlider/BoxSliderEvent
struct BoxSliderEvent_t4223315588;
// UnityEngine.UI.Extensions.Circle
struct Circle_t4191205477;
// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl
struct ColorPickerControl_t2793111723;
// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent
struct SelectionChangedEvent_t2252533886;
// UnityEngine.UI.Extensions.CooldownButton
struct CooldownButton_t2372397950;
// UnityEngine.UI.Extensions.CurvedText
struct CurvedText_t1522163716;
// UnityEngine.UI.Extensions.CylinderText
struct CylinderText_t364731485;
// UnityEngine.UI.Extensions.DropDownListItem
struct DropDownListItem_t337244270;
// UnityEngine.UI.Extensions.Examples.AwesomeMenu
struct AwesomeMenu_t320995884;
// UnityEngine.UI.Extensions.Examples.Example01ScrollView
struct Example01ScrollView_t2860006669;
// UnityEngine.UI.Extensions.Examples.Example02ScrollView
struct Example02ScrollView_t3654499597;
// UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext
struct Example02ScrollViewContext_t1179128608;
// UnityEngine.UI.Extensions.Examples.Example03ScrollView
struct Example03ScrollView_t2613198093;
// UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext
struct Example03ScrollViewContext_t47956341;
// UnityEngine.UI.Extensions.Examples.GameMenu
struct GameMenu_t2197707047;
// UnityEngine.UI.Extensions.Examples.MainMenu
struct MainMenu_t3811018904;
// UnityEngine.UI.Extensions.Examples.OptionsMenu
struct OptionsMenu_t3566679186;
// UnityEngine.UI.Extensions.Examples.PauseMenu
struct PauseMenu_t1705270178;
// UnityEngine.UI.Extensions.FancyScrollViewNullContext
struct FancyScrollViewNullContext_t3783020080;
// UnityEngine.UI.Extensions.Gradient2
struct Gradient2_t3786049496;
// UnityEngine.UI.Extensions.HorizontalScrollSnap
struct HorizontalScrollSnap_t1980761718;
// UnityEngine.UI.Extensions.LetterSpacing
struct LetterSpacing_t1421332419;
// UnityEngine.UI.Extensions.RadialSlider
struct RadialSlider_t2127270712;
// UnityEngine.UI.Extensions.ScrollPositionController
struct ScrollPositionController_t2221482878;
// UnityEngine.UI.Extensions.SoftMaskScript
struct SoftMaskScript_t2195956899;
// UnityEngine.UI.Extensions.TextPic
struct TextPic_t3289895869;
// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatFinishCallback
struct FloatFinishCallback_t3825696533;
// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t1418708507;
// UnityEngine.UI.Extensions.Tweens.TweenRunner`1<UnityEngine.UI.Extensions.Tweens.FloatTween>
struct TweenRunner_1_t2527600155;
// UnityEngine.UI.Extensions.UILineRenderer
struct UILineRenderer_t3861579578;
// UnityEngine.UI.Extensions.UIVerticalScroller
struct UIVerticalScroller_t3292120078;
// UnityEngine.UI.Extensions.VerticalScrollSnap
struct VerticalScrollSnap_t3018533569;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t2999697109;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t4137855814;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t1873685584;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t123837990;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// Vuforia.AnchorBehaviour
struct AnchorBehaviour_t2000812465;
// Vuforia.ContentPositioningBehaviour
struct ContentPositioningBehaviour_t532953367;
// Vuforia.PlaneFinderBehaviour
struct PlaneFinderBehaviour_t3756262673;
// Vuforia.PositionalDeviceTracker
struct PositionalDeviceTracker_t656722001;
// Vuforia.SmartTerrain
struct SmartTerrain_t256094413;
// Vuforia.StateManager
struct StateManager_t1982749557;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CWAITFORTHENU3EC__ITERATOR0_T751251344_H
#define U3CWAITFORTHENU3EC__ITERATOR0_T751251344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Augmentation/<WaitForThen>c__Iterator0
struct  U3CWaitForThenU3Ec__Iterator0_t751251344  : public RuntimeObject
{
public:
	// System.Single Augmentation/<WaitForThen>c__Iterator0::waitSeconds
	float ___waitSeconds_0;
	// System.Action Augmentation/<WaitForThen>c__Iterator0::action
	Action_t1264377477 * ___action_1;
	// System.Object Augmentation/<WaitForThen>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Augmentation/<WaitForThen>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Augmentation/<WaitForThen>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_waitSeconds_0() { return static_cast<int32_t>(offsetof(U3CWaitForThenU3Ec__Iterator0_t751251344, ___waitSeconds_0)); }
	inline float get_waitSeconds_0() const { return ___waitSeconds_0; }
	inline float* get_address_of_waitSeconds_0() { return &___waitSeconds_0; }
	inline void set_waitSeconds_0(float value)
	{
		___waitSeconds_0 = value;
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CWaitForThenU3Ec__Iterator0_t751251344, ___action_1)); }
	inline Action_t1264377477 * get_action_1() const { return ___action_1; }
	inline Action_t1264377477 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t1264377477 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForThenU3Ec__Iterator0_t751251344, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitForThenU3Ec__Iterator0_t751251344, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitForThenU3Ec__Iterator0_t751251344, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORTHENU3EC__ITERATOR0_T751251344_H
#ifndef U3COFFVUFORIAU3EC__ITERATOR0_T3838014700_H
#define U3COFFVUFORIAU3EC__ITERATOR0_T3838014700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroundPlaneUI/<OffVuforia>c__Iterator0
struct  U3COffVuforiaU3Ec__Iterator0_t3838014700  : public RuntimeObject
{
public:
	// GroundPlaneUI GroundPlaneUI/<OffVuforia>c__Iterator0::$this
	GroundPlaneUI_t2709539096 * ___U24this_0;
	// System.Object GroundPlaneUI/<OffVuforia>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GroundPlaneUI/<OffVuforia>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GroundPlaneUI/<OffVuforia>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3COffVuforiaU3Ec__Iterator0_t3838014700, ___U24this_0)); }
	inline GroundPlaneUI_t2709539096 * get_U24this_0() const { return ___U24this_0; }
	inline GroundPlaneUI_t2709539096 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GroundPlaneUI_t2709539096 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COffVuforiaU3Ec__Iterator0_t3838014700, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3COffVuforiaU3Ec__Iterator0_t3838014700, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3COffVuforiaU3Ec__Iterator0_t3838014700, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3COFFVUFORIAU3EC__ITERATOR0_T3838014700_H
#ifndef U3CONVUFORIAU3EC__ITERATOR1_T2073417735_H
#define U3CONVUFORIAU3EC__ITERATOR1_T2073417735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroundPlaneUI/<OnVuforia>c__Iterator1
struct  U3COnVuforiaU3Ec__Iterator1_t2073417735  : public RuntimeObject
{
public:
	// GroundPlaneUI GroundPlaneUI/<OnVuforia>c__Iterator1::$this
	GroundPlaneUI_t2709539096 * ___U24this_0;
	// System.Object GroundPlaneUI/<OnVuforia>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GroundPlaneUI/<OnVuforia>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 GroundPlaneUI/<OnVuforia>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3COnVuforiaU3Ec__Iterator1_t2073417735, ___U24this_0)); }
	inline GroundPlaneUI_t2709539096 * get_U24this_0() const { return ___U24this_0; }
	inline GroundPlaneUI_t2709539096 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GroundPlaneUI_t2709539096 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3COnVuforiaU3Ec__Iterator1_t2073417735, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3COnVuforiaU3Ec__Iterator1_t2073417735, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3COnVuforiaU3Ec__Iterator1_t2073417735, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONVUFORIAU3EC__ITERATOR1_T2073417735_H
#ifndef U3CWAITFORSECONDSOFFU3EC__ITERATOR1_T911118217_H
#define U3CWAITFORSECONDSOFFU3EC__ITERATOR1_T911118217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductPlacement/<WaitForSecondsOff>c__Iterator1
struct  U3CWaitForSecondsOffU3Ec__Iterator1_t911118217  : public RuntimeObject
{
public:
	// System.Single ProductPlacement/<WaitForSecondsOff>c__Iterator1::time
	float ___time_0;
	// ProductPlacement ProductPlacement/<WaitForSecondsOff>c__Iterator1::$this
	ProductPlacement_t2927687625 * ___U24this_1;
	// System.Object ProductPlacement/<WaitForSecondsOff>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ProductPlacement/<WaitForSecondsOff>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ProductPlacement/<WaitForSecondsOff>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOffU3Ec__Iterator1_t911118217, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOffU3Ec__Iterator1_t911118217, ___U24this_1)); }
	inline ProductPlacement_t2927687625 * get_U24this_1() const { return ___U24this_1; }
	inline ProductPlacement_t2927687625 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ProductPlacement_t2927687625 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOffU3Ec__Iterator1_t911118217, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOffU3Ec__Iterator1_t911118217, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOffU3Ec__Iterator1_t911118217, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSECONDSOFFU3EC__ITERATOR1_T911118217_H
#ifndef U3CWAITFORSECONDSONU3EC__ITERATOR0_T4215825553_H
#define U3CWAITFORSECONDSONU3EC__ITERATOR0_T4215825553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductPlacement/<WaitForSecondsOn>c__Iterator0
struct  U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553  : public RuntimeObject
{
public:
	// System.Single ProductPlacement/<WaitForSecondsOn>c__Iterator0::time
	float ___time_0;
	// ProductPlacement ProductPlacement/<WaitForSecondsOn>c__Iterator0::$this
	ProductPlacement_t2927687625 * ___U24this_1;
	// System.Object ProductPlacement/<WaitForSecondsOn>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ProductPlacement/<WaitForSecondsOn>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ProductPlacement/<WaitForSecondsOn>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553, ___U24this_1)); }
	inline ProductPlacement_t2927687625 * get_U24this_1() const { return ___U24this_1; }
	inline ProductPlacement_t2927687625 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ProductPlacement_t2927687625 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSECONDSONU3EC__ITERATOR0_T4215825553_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T1526519783_H
#define U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T1526519783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/<PruneItemsLinq>c__AnonStorey1
struct  U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox/<PruneItemsLinq>c__AnonStorey1::currText
	String_t* ___currText_0;

public:
	inline static int32_t get_offset_of_currText_0() { return static_cast<int32_t>(offsetof(U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783, ___currText_0)); }
	inline String_t* get_currText_0() const { return ___currText_0; }
	inline String_t** get_address_of_currText_0() { return &___currText_0; }
	inline void set_currText_0(String_t* value)
	{
		___currText_0 = value;
		Il2CppCodeGenWriteBarrier((&___currText_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRUNEITEMSLINQU3EC__ANONSTOREY1_T1526519783_H
#ifndef U3CREBUILDPANELU3EC__ANONSTOREY0_T1110430399_H
#define U3CREBUILDPANELU3EC__ANONSTOREY0_T1110430399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0
struct  U3CRebuildPanelU3Ec__AnonStorey0_t1110430399  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0::textOfItem
	String_t* ___textOfItem_0;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox UnityEngine.UI.Extensions.AutoCompleteComboBox/<RebuildPanel>c__AnonStorey0::$this
	AutoCompleteComboBox_t2765567798 * ___U24this_1;

public:
	inline static int32_t get_offset_of_textOfItem_0() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t1110430399, ___textOfItem_0)); }
	inline String_t* get_textOfItem_0() const { return ___textOfItem_0; }
	inline String_t** get_address_of_textOfItem_0() { return &___textOfItem_0; }
	inline void set_textOfItem_0(String_t* value)
	{
		___textOfItem_0 = value;
		Il2CppCodeGenWriteBarrier((&___textOfItem_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CRebuildPanelU3Ec__AnonStorey0_t1110430399, ___U24this_1)); }
	inline AutoCompleteComboBox_t2765567798 * get_U24this_1() const { return ___U24this_1; }
	inline AutoCompleteComboBox_t2765567798 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AutoCompleteComboBox_t2765567798 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREBUILDPANELU3EC__ANONSTOREY0_T1110430399_H
#ifndef HSVUTIL_T2354927206_H
#define HSVUTIL_T2354927206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HSVUtil
struct  HSVUtil_t2354927206  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVUTIL_T2354927206_H
#ifndef EXAMPLE01CELLDTO_T3695172350_H
#define EXAMPLE01CELLDTO_T3695172350_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example01CellDto
struct  Example01CellDto_t3695172350  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.Examples.Example01CellDto::Message
	String_t* ___Message_0;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(Example01CellDto_t3695172350, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE01CELLDTO_T3695172350_H
#ifndef EXAMPLE02CELLDTO_T3839548158_H
#define EXAMPLE02CELLDTO_T3839548158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example02CellDto
struct  Example02CellDto_t3839548158  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.Examples.Example02CellDto::Message
	String_t* ___Message_0;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(Example02CellDto_t3839548158, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE02CELLDTO_T3839548158_H
#ifndef EXAMPLE02SCROLLVIEWCONTEXT_T1179128608_H
#define EXAMPLE02SCROLLVIEWCONTEXT_T1179128608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext
struct  Example02ScrollViewContext_t1179128608  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell> UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext::OnPressedCell
	Action_1_t1589570706 * ___OnPressedCell_0;
	// System.Int32 UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext::SelectedIndex
	int32_t ___SelectedIndex_1;

public:
	inline static int32_t get_offset_of_OnPressedCell_0() { return static_cast<int32_t>(offsetof(Example02ScrollViewContext_t1179128608, ___OnPressedCell_0)); }
	inline Action_1_t1589570706 * get_OnPressedCell_0() const { return ___OnPressedCell_0; }
	inline Action_1_t1589570706 ** get_address_of_OnPressedCell_0() { return &___OnPressedCell_0; }
	inline void set_OnPressedCell_0(Action_1_t1589570706 * value)
	{
		___OnPressedCell_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressedCell_0), value);
	}

	inline static int32_t get_offset_of_SelectedIndex_1() { return static_cast<int32_t>(offsetof(Example02ScrollViewContext_t1179128608, ___SelectedIndex_1)); }
	inline int32_t get_SelectedIndex_1() const { return ___SelectedIndex_1; }
	inline int32_t* get_address_of_SelectedIndex_1() { return &___SelectedIndex_1; }
	inline void set_SelectedIndex_1(int32_t value)
	{
		___SelectedIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE02SCROLLVIEWCONTEXT_T1179128608_H
#ifndef EXAMPLE03CELLDTO_T2912803582_H
#define EXAMPLE03CELLDTO_T2912803582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example03CellDto
struct  Example03CellDto_t2912803582  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Extensions.Examples.Example03CellDto::Message
	String_t* ___Message_0;

public:
	inline static int32_t get_offset_of_Message_0() { return static_cast<int32_t>(offsetof(Example03CellDto_t2912803582, ___Message_0)); }
	inline String_t* get_Message_0() const { return ___Message_0; }
	inline String_t** get_address_of_Message_0() { return &___Message_0; }
	inline void set_Message_0(String_t* value)
	{
		___Message_0 = value;
		Il2CppCodeGenWriteBarrier((&___Message_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE03CELLDTO_T2912803582_H
#ifndef EXAMPLE03SCROLLVIEWCONTEXT_T47956341_H
#define EXAMPLE03SCROLLVIEWCONTEXT_T47956341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext
struct  Example03ScrollViewContext_t47956341  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell> UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext::OnPressedCell
	Action_1_t3383487634 * ___OnPressedCell_0;
	// System.Int32 UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext::SelectedIndex
	int32_t ___SelectedIndex_1;

public:
	inline static int32_t get_offset_of_OnPressedCell_0() { return static_cast<int32_t>(offsetof(Example03ScrollViewContext_t47956341, ___OnPressedCell_0)); }
	inline Action_1_t3383487634 * get_OnPressedCell_0() const { return ___OnPressedCell_0; }
	inline Action_1_t3383487634 ** get_address_of_OnPressedCell_0() { return &___OnPressedCell_0; }
	inline void set_OnPressedCell_0(Action_1_t3383487634 * value)
	{
		___OnPressedCell_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressedCell_0), value);
	}

	inline static int32_t get_offset_of_SelectedIndex_1() { return static_cast<int32_t>(offsetof(Example03ScrollViewContext_t47956341, ___SelectedIndex_1)); }
	inline int32_t get_SelectedIndex_1() const { return ___SelectedIndex_1; }
	inline int32_t* get_address_of_SelectedIndex_1() { return &___SelectedIndex_1; }
	inline void set_SelectedIndex_1(int32_t value)
	{
		___SelectedIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE03SCROLLVIEWCONTEXT_T47956341_H
#ifndef UTILITYHELPER_T1766679415_H
#define UTILITYHELPER_T1766679415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityHelper
struct  UtilityHelper_t1766679415  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITYHELPER_T1766679415_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t2562230146__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef UNITYEVENT_1_T978947469_H
#define UNITYEVENT_1_T978947469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t978947469  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t978947469, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T978947469_H
#ifndef UNITYEVENT_1_T2278926278_H
#define UNITYEVENT_1_T2278926278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2278926278  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2278926278, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2278926278_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef UNITYEVENT_1_T3437345828_H
#define UNITYEVENT_1_T3437345828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t3437345828  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3437345828, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3437345828_H
#ifndef UNITYEVENT_2_T1827368229_H
#define UNITYEVENT_2_T1827368229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Single,System.Single>
struct  UnityEvent_2_t1827368229  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1827368229, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1827368229_H
#ifndef UNITYEVENT_2_T364267509_H
#define UNITYEVENT_2_T364267509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.String,System.Boolean>
struct  UnityEvent_2_t364267509  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t364267509, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T364267509_H
#ifndef UNITYEVENT_3_T1697774568_H
#define UNITYEVENT_3_T1697774568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.Single,System.Single,System.Single>
struct  UnityEvent_3_t1697774568  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1697774568, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1697774568_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef HSVCOLOR_T3316572990_H
#define HSVCOLOR_T3316572990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HsvColor
struct  HsvColor_t3316572990 
{
public:
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::H
	double ___H_0;
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::S
	double ___S_1;
	// System.Double UnityEngine.UI.Extensions.ColorPicker.HsvColor::V
	double ___V_2;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(HsvColor_t3316572990, ___H_0)); }
	inline double get_H_0() const { return ___H_0; }
	inline double* get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(double value)
	{
		___H_0 = value;
	}

	inline static int32_t get_offset_of_S_1() { return static_cast<int32_t>(offsetof(HsvColor_t3316572990, ___S_1)); }
	inline double get_S_1() const { return ___S_1; }
	inline double* get_address_of_S_1() { return &___S_1; }
	inline void set_S_1(double value)
	{
		___S_1 = value;
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HsvColor_t3316572990, ___V_2)); }
	inline double get_V_2() const { return ___V_2; }
	inline double* get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(double value)
	{
		___V_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCOLOR_T3316572990_H
#ifndef FLOATTWEEN_T916090420_H
#define FLOATTWEEN_T916090420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Tweens.FloatTween
struct  FloatTween_t916090420 
{
public:
	// System.Single UnityEngine.UI.Extensions.Tweens.FloatTween::m_StartFloat
	float ___m_StartFloat_0;
	// System.Single UnityEngine.UI.Extensions.Tweens.FloatTween::m_TargetFloat
	float ___m_TargetFloat_1;
	// System.Single UnityEngine.UI.Extensions.Tweens.FloatTween::m_Duration
	float ___m_Duration_2;
	// System.Boolean UnityEngine.UI.Extensions.Tweens.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_3;
	// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatTweenCallback UnityEngine.UI.Extensions.Tweens.FloatTween::m_Target
	FloatTweenCallback_t1418708507 * ___m_Target_4;
	// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatFinishCallback UnityEngine.UI.Extensions.Tweens.FloatTween::m_Finish
	FloatFinishCallback_t3825696533 * ___m_Finish_5;

public:
	inline static int32_t get_offset_of_m_StartFloat_0() { return static_cast<int32_t>(offsetof(FloatTween_t916090420, ___m_StartFloat_0)); }
	inline float get_m_StartFloat_0() const { return ___m_StartFloat_0; }
	inline float* get_address_of_m_StartFloat_0() { return &___m_StartFloat_0; }
	inline void set_m_StartFloat_0(float value)
	{
		___m_StartFloat_0 = value;
	}

	inline static int32_t get_offset_of_m_TargetFloat_1() { return static_cast<int32_t>(offsetof(FloatTween_t916090420, ___m_TargetFloat_1)); }
	inline float get_m_TargetFloat_1() const { return ___m_TargetFloat_1; }
	inline float* get_address_of_m_TargetFloat_1() { return &___m_TargetFloat_1; }
	inline void set_m_TargetFloat_1(float value)
	{
		___m_TargetFloat_1 = value;
	}

	inline static int32_t get_offset_of_m_Duration_2() { return static_cast<int32_t>(offsetof(FloatTween_t916090420, ___m_Duration_2)); }
	inline float get_m_Duration_2() const { return ___m_Duration_2; }
	inline float* get_address_of_m_Duration_2() { return &___m_Duration_2; }
	inline void set_m_Duration_2(float value)
	{
		___m_Duration_2 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_3() { return static_cast<int32_t>(offsetof(FloatTween_t916090420, ___m_IgnoreTimeScale_3)); }
	inline bool get_m_IgnoreTimeScale_3() const { return ___m_IgnoreTimeScale_3; }
	inline bool* get_address_of_m_IgnoreTimeScale_3() { return &___m_IgnoreTimeScale_3; }
	inline void set_m_IgnoreTimeScale_3(bool value)
	{
		___m_IgnoreTimeScale_3 = value;
	}

	inline static int32_t get_offset_of_m_Target_4() { return static_cast<int32_t>(offsetof(FloatTween_t916090420, ___m_Target_4)); }
	inline FloatTweenCallback_t1418708507 * get_m_Target_4() const { return ___m_Target_4; }
	inline FloatTweenCallback_t1418708507 ** get_address_of_m_Target_4() { return &___m_Target_4; }
	inline void set_m_Target_4(FloatTweenCallback_t1418708507 * value)
	{
		___m_Target_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_4), value);
	}

	inline static int32_t get_offset_of_m_Finish_5() { return static_cast<int32_t>(offsetof(FloatTween_t916090420, ___m_Finish_5)); }
	inline FloatFinishCallback_t3825696533 * get_m_Finish_5() const { return ___m_Finish_5; }
	inline FloatFinishCallback_t3825696533 ** get_address_of_m_Finish_5() { return &___m_Finish_5; }
	inline void set_m_Finish_5(FloatFinishCallback_t3825696533 * value)
	{
		___m_Finish_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Finish_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Extensions.Tweens.FloatTween
struct FloatTween_t916090420_marshaled_pinvoke
{
	float ___m_StartFloat_0;
	float ___m_TargetFloat_1;
	float ___m_Duration_2;
	int32_t ___m_IgnoreTimeScale_3;
	FloatTweenCallback_t1418708507 * ___m_Target_4;
	FloatFinishCallback_t3825696533 * ___m_Finish_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Extensions.Tweens.FloatTween
struct FloatTween_t916090420_marshaled_com
{
	float ___m_StartFloat_0;
	float ___m_TargetFloat_1;
	float ___m_Duration_2;
	int32_t ___m_IgnoreTimeScale_3;
	FloatTweenCallback_t1418708507 * ___m_Target_4;
	FloatFinishCallback_t3825696533 * ___m_Finish_5;
};
#endif // FLOATTWEEN_T916090420_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef COLORCHANGEDEVENT_T3019780707_H
#define COLORCHANGEDEVENT_T3019780707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorChangedEvent
struct  ColorChangedEvent_t3019780707  : public UnityEvent_1_t3437345828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORCHANGEDEVENT_T3019780707_H
#ifndef HSVCHANGEDEVENT_T911780251_H
#define HSVCHANGEDEVENT_T911780251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HSVChangedEvent
struct  HSVChangedEvent_t911780251  : public UnityEvent_3_t1697774568
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HSVCHANGEDEVENT_T911780251_H
#ifndef PLANEMODE_T282559100_H
#define PLANEMODE_T282559100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneManager/PlaneMode
struct  PlaneMode_t282559100 
{
public:
	// System.Int32 PlaneManager/PlaneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneMode_t282559100, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEMODE_T282559100_H
#ifndef MENUITEM_T4061609034_H
#define MENUITEM_T4061609034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SamplesMainMenu/MenuItem
struct  MenuItem_t4061609034 
{
public:
	// System.Int32 SamplesMainMenu/MenuItem::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MenuItem_t4061609034, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUITEM_T4061609034_H
#ifndef ORIENTATION_T3671038586_H
#define ORIENTATION_T3671038586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneOrientation/Orientation
struct  Orientation_t3671038586 
{
public:
	// System.Int32 SceneOrientation/Orientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Orientation_t3671038586, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATION_T3671038586_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef TRANSITION_T1740872941_H
#define TRANSITION_T1740872941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Accordion/Transition
struct  Transition_t1740872941 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.Accordion/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1740872941, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1740872941_H
#ifndef SELECTIONCHANGEDEVENT_T1822043360_H
#define SELECTIONCHANGEDEVENT_T1822043360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent
struct  SelectionChangedEvent_t1822043360  : public UnityEvent_2_t364267509
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONCHANGEDEVENT_T1822043360_H
#ifndef SELECTIONTEXTCHANGEDEVENT_T4051177638_H
#define SELECTIONTEXTCHANGEDEVENT_T4051177638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent
struct  SelectionTextChangedEvent_t4051177638  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONTEXTCHANGEDEVENT_T4051177638_H
#ifndef SELECTIONVALIDITYCHANGEDEVENT_T954817928_H
#define SELECTIONVALIDITYCHANGEDEVENT_T954817928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent
struct  SelectionValidityChangedEvent_t954817928  : public UnityEvent_1_t978947469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONVALIDITYCHANGEDEVENT_T954817928_H
#ifndef AUTOCOMPLETESEARCHTYPE_T2628651075_H
#define AUTOCOMPLETESEARCHTYPE_T2628651075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteSearchType
struct  AutoCompleteSearchType_t2628651075 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.AutoCompleteSearchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoCompleteSearchType_t2628651075, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCOMPLETESEARCHTYPE_T2628651075_H
#ifndef AXIS_T1696420063_H
#define AXIS_T1696420063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider/Axis
struct  Axis_t1696420063 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.BoxSlider/Axis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Axis_t1696420063, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1696420063_H
#ifndef BOXSLIDEREVENT_T4223315588_H
#define BOXSLIDEREVENT_T4223315588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider/BoxSliderEvent
struct  BoxSliderEvent_t4223315588  : public UnityEvent_2_t1827368229
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDEREVENT_T4223315588_H
#ifndef DIRECTION_T1135146939_H
#define DIRECTION_T1135146939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider/Direction
struct  Direction_t1135146939 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.BoxSlider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_t1135146939, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1135146939_H
#ifndef COLORVALUES_T950091080_H
#define COLORVALUES_T950091080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorValues
struct  ColorValues_t950091080 
{
public:
	// System.Int32 UnityEngine.UI.Extensions.ColorPicker.ColorValues::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorValues_t950091080, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORVALUES_T950091080_H
#ifndef FLOATFINISHCALLBACK_T3825696533_H
#define FLOATFINISHCALLBACK_T3825696533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatFinishCallback
struct  FloatFinishCallback_t3825696533  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFINISHCALLBACK_T3825696533_H
#ifndef FLOATTWEENCALLBACK_T1418708507_H
#define FLOATTWEENCALLBACK_T1418708507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Tweens.FloatTween/FloatTweenCallback
struct  FloatTweenCallback_t1418708507  : public UnityEvent_1_t2278926278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATTWEENCALLBACK_T1418708507_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef TOGGLETRANSITION_T3587297765_H
#define TOGGLETRANSITION_T3587297765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle/ToggleTransition
struct  ToggleTransition_t3587297765 
{
public:
	// System.Int32 UnityEngine.UI.Toggle/ToggleTransition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToggleTransition_t3587297765, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRANSITION_T3587297765_H
#ifndef STATUS_T1100905814_H
#define STATUS_T1100905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1100905814 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t1100905814, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1100905814_H
#ifndef STATUSINFO_T1633251416_H
#define STATUSINFO_T1633251416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/StatusInfo
struct  StatusInfo_t1633251416 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/StatusInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StatusInfo_t1633251416, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSINFO_T1633251416_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef STATEMACHINEBEHAVIOUR_T957311111_H
#define STATEMACHINEBEHAVIOUR_T957311111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t957311111  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T957311111_H
#ifndef AUGMENTATIONSTATEMACHINEBEHAVIOUR_T3849818102_H
#define AUGMENTATIONSTATEMACHINEBEHAVIOUR_T3849818102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AugmentationStateMachineBehaviour
struct  AugmentationStateMachineBehaviour_t3849818102  : public StateMachineBehaviour_t957311111
{
public:
	// System.String AugmentationStateMachineBehaviour::m_OnEnterMethodName
	String_t* ___m_OnEnterMethodName_4;
	// System.String AugmentationStateMachineBehaviour::m_OnUpdateMethodName
	String_t* ___m_OnUpdateMethodName_5;
	// System.String AugmentationStateMachineBehaviour::m_OnExitMethodName
	String_t* ___m_OnExitMethodName_6;

public:
	inline static int32_t get_offset_of_m_OnEnterMethodName_4() { return static_cast<int32_t>(offsetof(AugmentationStateMachineBehaviour_t3849818102, ___m_OnEnterMethodName_4)); }
	inline String_t* get_m_OnEnterMethodName_4() const { return ___m_OnEnterMethodName_4; }
	inline String_t** get_address_of_m_OnEnterMethodName_4() { return &___m_OnEnterMethodName_4; }
	inline void set_m_OnEnterMethodName_4(String_t* value)
	{
		___m_OnEnterMethodName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEnterMethodName_4), value);
	}

	inline static int32_t get_offset_of_m_OnUpdateMethodName_5() { return static_cast<int32_t>(offsetof(AugmentationStateMachineBehaviour_t3849818102, ___m_OnUpdateMethodName_5)); }
	inline String_t* get_m_OnUpdateMethodName_5() const { return ___m_OnUpdateMethodName_5; }
	inline String_t** get_address_of_m_OnUpdateMethodName_5() { return &___m_OnUpdateMethodName_5; }
	inline void set_m_OnUpdateMethodName_5(String_t* value)
	{
		___m_OnUpdateMethodName_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnUpdateMethodName_5), value);
	}

	inline static int32_t get_offset_of_m_OnExitMethodName_6() { return static_cast<int32_t>(offsetof(AugmentationStateMachineBehaviour_t3849818102, ___m_OnExitMethodName_6)); }
	inline String_t* get_m_OnExitMethodName_6() const { return ___m_OnExitMethodName_6; }
	inline String_t** get_address_of_m_OnExitMethodName_6() { return &___m_OnExitMethodName_6; }
	inline void set_m_OnExitMethodName_6(String_t* value)
	{
		___m_OnExitMethodName_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnExitMethodName_6), value);
	}
};

struct AugmentationStateMachineBehaviour_t3849818102_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>> AugmentationStateMachineBehaviour::cachedDelegates
	Dictionary_2_t3417996176 * ___cachedDelegates_7;

public:
	inline static int32_t get_offset_of_cachedDelegates_7() { return static_cast<int32_t>(offsetof(AugmentationStateMachineBehaviour_t3849818102_StaticFields, ___cachedDelegates_7)); }
	inline Dictionary_2_t3417996176 * get_cachedDelegates_7() const { return ___cachedDelegates_7; }
	inline Dictionary_2_t3417996176 ** get_address_of_cachedDelegates_7() { return &___cachedDelegates_7; }
	inline void set_cachedDelegates_7(Dictionary_2_t3417996176 * value)
	{
		___cachedDelegates_7 = value;
		Il2CppCodeGenWriteBarrier((&___cachedDelegates_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTATIONSTATEMACHINEBEHAVIOUR_T3849818102_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ASTRONAUTSTATEMACHINEBEHAVIOUR_T3422698390_H
#define ASTRONAUTSTATEMACHINEBEHAVIOUR_T3422698390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AstronautStateMachineBehaviour
struct  AstronautStateMachineBehaviour_t3422698390  : public AugmentationStateMachineBehaviour_t3849818102
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTRONAUTSTATEMACHINEBEHAVIOUR_T3422698390_H
#ifndef AUGMENTATION_T2596699517_H
#define AUGMENTATION_T2596699517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Augmentation
struct  Augmentation_t2596699517  : public MonoBehaviour_t3962482529
{
public:
	// System.Action Augmentation::m_EvtOnEnter
	Action_t1264377477 * ___m_EvtOnEnter_4;
	// System.Action Augmentation::m_EvtOnExit
	Action_t1264377477 * ___m_EvtOnExit_5;
	// UnityEngine.Animator Augmentation::animator
	Animator_t434523843 * ___animator_6;
	// System.Boolean Augmentation::active
	bool ___active_7;
	// System.Collections.IEnumerator Augmentation::waitCoroutine
	RuntimeObject* ___waitCoroutine_8;

public:
	inline static int32_t get_offset_of_m_EvtOnEnter_4() { return static_cast<int32_t>(offsetof(Augmentation_t2596699517, ___m_EvtOnEnter_4)); }
	inline Action_t1264377477 * get_m_EvtOnEnter_4() const { return ___m_EvtOnEnter_4; }
	inline Action_t1264377477 ** get_address_of_m_EvtOnEnter_4() { return &___m_EvtOnEnter_4; }
	inline void set_m_EvtOnEnter_4(Action_t1264377477 * value)
	{
		___m_EvtOnEnter_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EvtOnEnter_4), value);
	}

	inline static int32_t get_offset_of_m_EvtOnExit_5() { return static_cast<int32_t>(offsetof(Augmentation_t2596699517, ___m_EvtOnExit_5)); }
	inline Action_t1264377477 * get_m_EvtOnExit_5() const { return ___m_EvtOnExit_5; }
	inline Action_t1264377477 ** get_address_of_m_EvtOnExit_5() { return &___m_EvtOnExit_5; }
	inline void set_m_EvtOnExit_5(Action_t1264377477 * value)
	{
		___m_EvtOnExit_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EvtOnExit_5), value);
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(Augmentation_t2596699517, ___animator_6)); }
	inline Animator_t434523843 * get_animator_6() const { return ___animator_6; }
	inline Animator_t434523843 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t434523843 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((&___animator_6), value);
	}

	inline static int32_t get_offset_of_active_7() { return static_cast<int32_t>(offsetof(Augmentation_t2596699517, ___active_7)); }
	inline bool get_active_7() const { return ___active_7; }
	inline bool* get_address_of_active_7() { return &___active_7; }
	inline void set_active_7(bool value)
	{
		___active_7 = value;
	}

	inline static int32_t get_offset_of_waitCoroutine_8() { return static_cast<int32_t>(offsetof(Augmentation_t2596699517, ___waitCoroutine_8)); }
	inline RuntimeObject* get_waitCoroutine_8() const { return ___waitCoroutine_8; }
	inline RuntimeObject** get_address_of_waitCoroutine_8() { return &___waitCoroutine_8; }
	inline void set_waitCoroutine_8(RuntimeObject* value)
	{
		___waitCoroutine_8 = value;
		Il2CppCodeGenWriteBarrier((&___waitCoroutine_8), value);
	}
};

struct Augmentation_t2596699517_StaticFields
{
public:
	// System.Action Augmentation::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_9;
	// System.Action Augmentation::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(Augmentation_t2596699517_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(Augmentation_t2596699517_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUGMENTATION_T2596699517_H
#ifndef CAMERAFOCUSCONTROLLER_T1033776956_H
#define CAMERAFOCUSCONTROLLER_T1033776956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFocusController
struct  CameraFocusController_t1033776956  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CameraFocusController::mVuforiaStarted
	bool ___mVuforiaStarted_4;

public:
	inline static int32_t get_offset_of_mVuforiaStarted_4() { return static_cast<int32_t>(offsetof(CameraFocusController_t1033776956, ___mVuforiaStarted_4)); }
	inline bool get_mVuforiaStarted_4() const { return ___mVuforiaStarted_4; }
	inline bool* get_address_of_mVuforiaStarted_4() { return &___mVuforiaStarted_4; }
	inline void set_mVuforiaStarted_4(bool value)
	{
		___mVuforiaStarted_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOCUSCONTROLLER_T1033776956_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#define DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t1588957063  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t1113559212 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_6;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t1113559212 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t1113559212 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t1588957063, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T1588957063_H
#ifndef DRILLCONTROLLER_T3872647469_H
#define DRILLCONTROLLER_T3872647469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrillController
struct  DrillController_t3872647469  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean DrillController::m_IsDrilling
	bool ___m_IsDrilling_4;
	// System.Single DrillController::drillLerpPercentage
	float ___drillLerpPercentage_5;

public:
	inline static int32_t get_offset_of_m_IsDrilling_4() { return static_cast<int32_t>(offsetof(DrillController_t3872647469, ___m_IsDrilling_4)); }
	inline bool get_m_IsDrilling_4() const { return ___m_IsDrilling_4; }
	inline bool* get_address_of_m_IsDrilling_4() { return &___m_IsDrilling_4; }
	inline void set_m_IsDrilling_4(bool value)
	{
		___m_IsDrilling_4 = value;
	}

	inline static int32_t get_offset_of_drillLerpPercentage_5() { return static_cast<int32_t>(offsetof(DrillController_t3872647469, ___drillLerpPercentage_5)); }
	inline float get_drillLerpPercentage_5() const { return ___drillLerpPercentage_5; }
	inline float* get_address_of_drillLerpPercentage_5() { return &___drillLerpPercentage_5; }
	inline void set_drillLerpPercentage_5(float value)
	{
		___drillLerpPercentage_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRILLCONTROLLER_T3872647469_H
#ifndef FADEOBJECT_T1880495183_H
#define FADEOBJECT_T1880495183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadeObject
struct  FadeObject_t1880495183  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean FadeObject::m_IsVisible
	bool ___m_IsVisible_4;
	// System.Single FadeObject::m_FadeDuration
	float ___m_FadeDuration_5;
	// UnityEngine.Renderer[] FadeObject::m_RenderersToFade
	RendererU5BU5D_t3210418286* ___m_RenderersToFade_6;
	// System.Single FadeObject::fadeRatio
	float ___fadeRatio_7;
	// System.Boolean FadeObject::isInitialOpacitySet
	bool ___isInitialOpacitySet_8;

public:
	inline static int32_t get_offset_of_m_IsVisible_4() { return static_cast<int32_t>(offsetof(FadeObject_t1880495183, ___m_IsVisible_4)); }
	inline bool get_m_IsVisible_4() const { return ___m_IsVisible_4; }
	inline bool* get_address_of_m_IsVisible_4() { return &___m_IsVisible_4; }
	inline void set_m_IsVisible_4(bool value)
	{
		___m_IsVisible_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(FadeObject_t1880495183, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}

	inline static int32_t get_offset_of_m_RenderersToFade_6() { return static_cast<int32_t>(offsetof(FadeObject_t1880495183, ___m_RenderersToFade_6)); }
	inline RendererU5BU5D_t3210418286* get_m_RenderersToFade_6() const { return ___m_RenderersToFade_6; }
	inline RendererU5BU5D_t3210418286** get_address_of_m_RenderersToFade_6() { return &___m_RenderersToFade_6; }
	inline void set_m_RenderersToFade_6(RendererU5BU5D_t3210418286* value)
	{
		___m_RenderersToFade_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RenderersToFade_6), value);
	}

	inline static int32_t get_offset_of_fadeRatio_7() { return static_cast<int32_t>(offsetof(FadeObject_t1880495183, ___fadeRatio_7)); }
	inline float get_fadeRatio_7() const { return ___fadeRatio_7; }
	inline float* get_address_of_fadeRatio_7() { return &___fadeRatio_7; }
	inline void set_fadeRatio_7(float value)
	{
		___fadeRatio_7 = value;
	}

	inline static int32_t get_offset_of_isInitialOpacitySet_8() { return static_cast<int32_t>(offsetof(FadeObject_t1880495183, ___isInitialOpacitySet_8)); }
	inline bool get_isInitialOpacitySet_8() const { return ___isInitialOpacitySet_8; }
	inline bool* get_address_of_isInitialOpacitySet_8() { return &___isInitialOpacitySet_8; }
	inline void set_isInitialOpacitySet_8(bool value)
	{
		___isInitialOpacitySet_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEOBJECT_T1880495183_H
#ifndef FISSURE_T4281376855_H
#define FISSURE_T4281376855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fissure
struct  Fissure_t4281376855  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Gradient Fissure::particleGradientNormal
	Gradient_t3067099924 * ___particleGradientNormal_4;
	// UnityEngine.Gradient Fissure::particleGradientPressed
	Gradient_t3067099924 * ___particleGradientPressed_5;

public:
	inline static int32_t get_offset_of_particleGradientNormal_4() { return static_cast<int32_t>(offsetof(Fissure_t4281376855, ___particleGradientNormal_4)); }
	inline Gradient_t3067099924 * get_particleGradientNormal_4() const { return ___particleGradientNormal_4; }
	inline Gradient_t3067099924 ** get_address_of_particleGradientNormal_4() { return &___particleGradientNormal_4; }
	inline void set_particleGradientNormal_4(Gradient_t3067099924 * value)
	{
		___particleGradientNormal_4 = value;
		Il2CppCodeGenWriteBarrier((&___particleGradientNormal_4), value);
	}

	inline static int32_t get_offset_of_particleGradientPressed_5() { return static_cast<int32_t>(offsetof(Fissure_t4281376855, ___particleGradientPressed_5)); }
	inline Gradient_t3067099924 * get_particleGradientPressed_5() const { return ___particleGradientPressed_5; }
	inline Gradient_t3067099924 ** get_address_of_particleGradientPressed_5() { return &___particleGradientPressed_5; }
	inline void set_particleGradientPressed_5(Gradient_t3067099924 * value)
	{
		___particleGradientPressed_5 = value;
		Il2CppCodeGenWriteBarrier((&___particleGradientPressed_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FISSURE_T4281376855_H
#ifndef GROUNDPLANEUI_T2709539096_H
#define GROUNDPLANEUI_T2709539096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroundPlaneUI
struct  GroundPlaneUI_t2709539096  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform GroundPlaneUI::productPanel
	RectTransform_t3704657025 * ___productPanel_4;
	// PlaneManager GroundPlaneUI::planeManager
	PlaneManager_t2021199913 * ___planeManager_5;
	// ProductPlacement GroundPlaneUI::productPlacement
	ProductPlacement_t2927687625 * ___productPlacement_6;
	// UnityEngine.UI.GraphicRaycaster GroundPlaneUI::graphicRayCaster
	GraphicRaycaster_t2999697109 * ___graphicRayCaster_7;
	// UnityEngine.EventSystems.PointerEventData GroundPlaneUI::pointerEventData
	PointerEventData_t3807901092 * ___pointerEventData_8;
	// UnityEngine.EventSystems.EventSystem GroundPlaneUI::eventSystem
	EventSystem_t1003666588 * ___eventSystem_9;
	// System.Boolean GroundPlaneUI::showMessage
	bool ___showMessage_10;
	// System.Boolean GroundPlaneUI::notPlaced
	bool ___notPlaced_11;
	// System.Boolean GroundPlaneUI::inVuforia
	bool ___inVuforia_12;

public:
	inline static int32_t get_offset_of_productPanel_4() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___productPanel_4)); }
	inline RectTransform_t3704657025 * get_productPanel_4() const { return ___productPanel_4; }
	inline RectTransform_t3704657025 ** get_address_of_productPanel_4() { return &___productPanel_4; }
	inline void set_productPanel_4(RectTransform_t3704657025 * value)
	{
		___productPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___productPanel_4), value);
	}

	inline static int32_t get_offset_of_planeManager_5() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___planeManager_5)); }
	inline PlaneManager_t2021199913 * get_planeManager_5() const { return ___planeManager_5; }
	inline PlaneManager_t2021199913 ** get_address_of_planeManager_5() { return &___planeManager_5; }
	inline void set_planeManager_5(PlaneManager_t2021199913 * value)
	{
		___planeManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___planeManager_5), value);
	}

	inline static int32_t get_offset_of_productPlacement_6() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___productPlacement_6)); }
	inline ProductPlacement_t2927687625 * get_productPlacement_6() const { return ___productPlacement_6; }
	inline ProductPlacement_t2927687625 ** get_address_of_productPlacement_6() { return &___productPlacement_6; }
	inline void set_productPlacement_6(ProductPlacement_t2927687625 * value)
	{
		___productPlacement_6 = value;
		Il2CppCodeGenWriteBarrier((&___productPlacement_6), value);
	}

	inline static int32_t get_offset_of_graphicRayCaster_7() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___graphicRayCaster_7)); }
	inline GraphicRaycaster_t2999697109 * get_graphicRayCaster_7() const { return ___graphicRayCaster_7; }
	inline GraphicRaycaster_t2999697109 ** get_address_of_graphicRayCaster_7() { return &___graphicRayCaster_7; }
	inline void set_graphicRayCaster_7(GraphicRaycaster_t2999697109 * value)
	{
		___graphicRayCaster_7 = value;
		Il2CppCodeGenWriteBarrier((&___graphicRayCaster_7), value);
	}

	inline static int32_t get_offset_of_pointerEventData_8() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___pointerEventData_8)); }
	inline PointerEventData_t3807901092 * get_pointerEventData_8() const { return ___pointerEventData_8; }
	inline PointerEventData_t3807901092 ** get_address_of_pointerEventData_8() { return &___pointerEventData_8; }
	inline void set_pointerEventData_8(PointerEventData_t3807901092 * value)
	{
		___pointerEventData_8 = value;
		Il2CppCodeGenWriteBarrier((&___pointerEventData_8), value);
	}

	inline static int32_t get_offset_of_eventSystem_9() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___eventSystem_9)); }
	inline EventSystem_t1003666588 * get_eventSystem_9() const { return ___eventSystem_9; }
	inline EventSystem_t1003666588 ** get_address_of_eventSystem_9() { return &___eventSystem_9; }
	inline void set_eventSystem_9(EventSystem_t1003666588 * value)
	{
		___eventSystem_9 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_9), value);
	}

	inline static int32_t get_offset_of_showMessage_10() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___showMessage_10)); }
	inline bool get_showMessage_10() const { return ___showMessage_10; }
	inline bool* get_address_of_showMessage_10() { return &___showMessage_10; }
	inline void set_showMessage_10(bool value)
	{
		___showMessage_10 = value;
	}

	inline static int32_t get_offset_of_notPlaced_11() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___notPlaced_11)); }
	inline bool get_notPlaced_11() const { return ___notPlaced_11; }
	inline bool* get_address_of_notPlaced_11() { return &___notPlaced_11; }
	inline void set_notPlaced_11(bool value)
	{
		___notPlaced_11 = value;
	}

	inline static int32_t get_offset_of_inVuforia_12() { return static_cast<int32_t>(offsetof(GroundPlaneUI_t2709539096, ___inVuforia_12)); }
	inline bool get_inVuforia_12() const { return ___inVuforia_12; }
	inline bool* get_address_of_inVuforia_12() { return &___inVuforia_12; }
	inline void set_inVuforia_12(bool value)
	{
		___inVuforia_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUNDPLANEUI_T2709539096_H
#ifndef MODELSWAP_T1632145241_H
#define MODELSWAP_T1632145241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelSwap
struct  ModelSwap_t1632145241  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ModelSwap::m_DefaultModel
	GameObject_t1113636619 * ___m_DefaultModel_4;
	// UnityEngine.GameObject ModelSwap::m_ExtendedModel
	GameObject_t1113636619 * ___m_ExtendedModel_5;
	// UnityEngine.GameObject ModelSwap::m_ActiveModel
	GameObject_t1113636619 * ___m_ActiveModel_6;
	// TrackableSettings ModelSwap::m_TrackableSettings
	TrackableSettings_t2862243993 * ___m_TrackableSettings_7;

public:
	inline static int32_t get_offset_of_m_DefaultModel_4() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___m_DefaultModel_4)); }
	inline GameObject_t1113636619 * get_m_DefaultModel_4() const { return ___m_DefaultModel_4; }
	inline GameObject_t1113636619 ** get_address_of_m_DefaultModel_4() { return &___m_DefaultModel_4; }
	inline void set_m_DefaultModel_4(GameObject_t1113636619 * value)
	{
		___m_DefaultModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultModel_4), value);
	}

	inline static int32_t get_offset_of_m_ExtendedModel_5() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___m_ExtendedModel_5)); }
	inline GameObject_t1113636619 * get_m_ExtendedModel_5() const { return ___m_ExtendedModel_5; }
	inline GameObject_t1113636619 ** get_address_of_m_ExtendedModel_5() { return &___m_ExtendedModel_5; }
	inline void set_m_ExtendedModel_5(GameObject_t1113636619 * value)
	{
		___m_ExtendedModel_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ExtendedModel_5), value);
	}

	inline static int32_t get_offset_of_m_ActiveModel_6() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___m_ActiveModel_6)); }
	inline GameObject_t1113636619 * get_m_ActiveModel_6() const { return ___m_ActiveModel_6; }
	inline GameObject_t1113636619 ** get_address_of_m_ActiveModel_6() { return &___m_ActiveModel_6; }
	inline void set_m_ActiveModel_6(GameObject_t1113636619 * value)
	{
		___m_ActiveModel_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveModel_6), value);
	}

	inline static int32_t get_offset_of_m_TrackableSettings_7() { return static_cast<int32_t>(offsetof(ModelSwap_t1632145241, ___m_TrackableSettings_7)); }
	inline TrackableSettings_t2862243993 * get_m_TrackableSettings_7() const { return ___m_TrackableSettings_7; }
	inline TrackableSettings_t2862243993 ** get_address_of_m_TrackableSettings_7() { return &___m_TrackableSettings_7; }
	inline void set_m_TrackableSettings_7(TrackableSettings_t2862243993 * value)
	{
		___m_TrackableSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableSettings_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELSWAP_T1632145241_H
#ifndef PLANEMANAGER_T2021199913_H
#define PLANEMANAGER_T2021199913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneManager
struct  PlaneManager_t2021199913  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlaneManager::anchorPlacement
	GameObject_t1113636619 * ___anchorPlacement_7;
	// ProductPlacement PlaneManager::productPlacement
	ProductPlacement_t2927687625 * ___productPlacement_8;
	// UnityEngine.GameObject PlaneManager::placementAugmentation
	GameObject_t1113636619 * ___placementAugmentation_9;
	// Vuforia.StateManager PlaneManager::stateManager
	StateManager_t1982749557 * ___stateManager_12;
	// Vuforia.SmartTerrain PlaneManager::smartTerrain
	SmartTerrain_t256094413 * ___smartTerrain_13;
	// Vuforia.PositionalDeviceTracker PlaneManager::positionalDeviceTracker
	PositionalDeviceTracker_t656722001 * ___positionalDeviceTracker_14;
	// Vuforia.ContentPositioningBehaviour PlaneManager::contentPositioningBehaviour
	ContentPositioningBehaviour_t532953367 * ___contentPositioningBehaviour_15;
	// TouchHandler PlaneManager::touchHandler
	TouchHandler_t3441426771 * ___touchHandler_16;
	// GroundPlaneUI PlaneManager::groundPlaneUI
	GroundPlaneUI_t2709539096 * ___groundPlaneUI_17;
	// Vuforia.PlaneFinderBehaviour PlaneManager::planeFinder
	PlaneFinderBehaviour_t3756262673 * ___planeFinder_18;
	// Vuforia.AnchorBehaviour PlaneManager::placementAnchor
	AnchorBehaviour_t2000812465 * ___placementAnchor_19;
	// System.Int32 PlaneManager::automaticHitTestFrameCount
	int32_t ___automaticHitTestFrameCount_20;
	// System.Timers.Timer PlaneManager::timer
	Timer_t1767341190 * ___timer_23;
	// System.Boolean PlaneManager::timerFinished
	bool ___timerFinished_24;
	// UnityEngine.GameObject PlaneManager::messageBoxPrefab
	GameObject_t1113636619 * ___messageBoxPrefab_25;

public:
	inline static int32_t get_offset_of_anchorPlacement_7() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___anchorPlacement_7)); }
	inline GameObject_t1113636619 * get_anchorPlacement_7() const { return ___anchorPlacement_7; }
	inline GameObject_t1113636619 ** get_address_of_anchorPlacement_7() { return &___anchorPlacement_7; }
	inline void set_anchorPlacement_7(GameObject_t1113636619 * value)
	{
		___anchorPlacement_7 = value;
		Il2CppCodeGenWriteBarrier((&___anchorPlacement_7), value);
	}

	inline static int32_t get_offset_of_productPlacement_8() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___productPlacement_8)); }
	inline ProductPlacement_t2927687625 * get_productPlacement_8() const { return ___productPlacement_8; }
	inline ProductPlacement_t2927687625 ** get_address_of_productPlacement_8() { return &___productPlacement_8; }
	inline void set_productPlacement_8(ProductPlacement_t2927687625 * value)
	{
		___productPlacement_8 = value;
		Il2CppCodeGenWriteBarrier((&___productPlacement_8), value);
	}

	inline static int32_t get_offset_of_placementAugmentation_9() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___placementAugmentation_9)); }
	inline GameObject_t1113636619 * get_placementAugmentation_9() const { return ___placementAugmentation_9; }
	inline GameObject_t1113636619 ** get_address_of_placementAugmentation_9() { return &___placementAugmentation_9; }
	inline void set_placementAugmentation_9(GameObject_t1113636619 * value)
	{
		___placementAugmentation_9 = value;
		Il2CppCodeGenWriteBarrier((&___placementAugmentation_9), value);
	}

	inline static int32_t get_offset_of_stateManager_12() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___stateManager_12)); }
	inline StateManager_t1982749557 * get_stateManager_12() const { return ___stateManager_12; }
	inline StateManager_t1982749557 ** get_address_of_stateManager_12() { return &___stateManager_12; }
	inline void set_stateManager_12(StateManager_t1982749557 * value)
	{
		___stateManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___stateManager_12), value);
	}

	inline static int32_t get_offset_of_smartTerrain_13() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___smartTerrain_13)); }
	inline SmartTerrain_t256094413 * get_smartTerrain_13() const { return ___smartTerrain_13; }
	inline SmartTerrain_t256094413 ** get_address_of_smartTerrain_13() { return &___smartTerrain_13; }
	inline void set_smartTerrain_13(SmartTerrain_t256094413 * value)
	{
		___smartTerrain_13 = value;
		Il2CppCodeGenWriteBarrier((&___smartTerrain_13), value);
	}

	inline static int32_t get_offset_of_positionalDeviceTracker_14() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___positionalDeviceTracker_14)); }
	inline PositionalDeviceTracker_t656722001 * get_positionalDeviceTracker_14() const { return ___positionalDeviceTracker_14; }
	inline PositionalDeviceTracker_t656722001 ** get_address_of_positionalDeviceTracker_14() { return &___positionalDeviceTracker_14; }
	inline void set_positionalDeviceTracker_14(PositionalDeviceTracker_t656722001 * value)
	{
		___positionalDeviceTracker_14 = value;
		Il2CppCodeGenWriteBarrier((&___positionalDeviceTracker_14), value);
	}

	inline static int32_t get_offset_of_contentPositioningBehaviour_15() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___contentPositioningBehaviour_15)); }
	inline ContentPositioningBehaviour_t532953367 * get_contentPositioningBehaviour_15() const { return ___contentPositioningBehaviour_15; }
	inline ContentPositioningBehaviour_t532953367 ** get_address_of_contentPositioningBehaviour_15() { return &___contentPositioningBehaviour_15; }
	inline void set_contentPositioningBehaviour_15(ContentPositioningBehaviour_t532953367 * value)
	{
		___contentPositioningBehaviour_15 = value;
		Il2CppCodeGenWriteBarrier((&___contentPositioningBehaviour_15), value);
	}

	inline static int32_t get_offset_of_touchHandler_16() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___touchHandler_16)); }
	inline TouchHandler_t3441426771 * get_touchHandler_16() const { return ___touchHandler_16; }
	inline TouchHandler_t3441426771 ** get_address_of_touchHandler_16() { return &___touchHandler_16; }
	inline void set_touchHandler_16(TouchHandler_t3441426771 * value)
	{
		___touchHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___touchHandler_16), value);
	}

	inline static int32_t get_offset_of_groundPlaneUI_17() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___groundPlaneUI_17)); }
	inline GroundPlaneUI_t2709539096 * get_groundPlaneUI_17() const { return ___groundPlaneUI_17; }
	inline GroundPlaneUI_t2709539096 ** get_address_of_groundPlaneUI_17() { return &___groundPlaneUI_17; }
	inline void set_groundPlaneUI_17(GroundPlaneUI_t2709539096 * value)
	{
		___groundPlaneUI_17 = value;
		Il2CppCodeGenWriteBarrier((&___groundPlaneUI_17), value);
	}

	inline static int32_t get_offset_of_planeFinder_18() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___planeFinder_18)); }
	inline PlaneFinderBehaviour_t3756262673 * get_planeFinder_18() const { return ___planeFinder_18; }
	inline PlaneFinderBehaviour_t3756262673 ** get_address_of_planeFinder_18() { return &___planeFinder_18; }
	inline void set_planeFinder_18(PlaneFinderBehaviour_t3756262673 * value)
	{
		___planeFinder_18 = value;
		Il2CppCodeGenWriteBarrier((&___planeFinder_18), value);
	}

	inline static int32_t get_offset_of_placementAnchor_19() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___placementAnchor_19)); }
	inline AnchorBehaviour_t2000812465 * get_placementAnchor_19() const { return ___placementAnchor_19; }
	inline AnchorBehaviour_t2000812465 ** get_address_of_placementAnchor_19() { return &___placementAnchor_19; }
	inline void set_placementAnchor_19(AnchorBehaviour_t2000812465 * value)
	{
		___placementAnchor_19 = value;
		Il2CppCodeGenWriteBarrier((&___placementAnchor_19), value);
	}

	inline static int32_t get_offset_of_automaticHitTestFrameCount_20() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___automaticHitTestFrameCount_20)); }
	inline int32_t get_automaticHitTestFrameCount_20() const { return ___automaticHitTestFrameCount_20; }
	inline int32_t* get_address_of_automaticHitTestFrameCount_20() { return &___automaticHitTestFrameCount_20; }
	inline void set_automaticHitTestFrameCount_20(int32_t value)
	{
		___automaticHitTestFrameCount_20 = value;
	}

	inline static int32_t get_offset_of_timer_23() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___timer_23)); }
	inline Timer_t1767341190 * get_timer_23() const { return ___timer_23; }
	inline Timer_t1767341190 ** get_address_of_timer_23() { return &___timer_23; }
	inline void set_timer_23(Timer_t1767341190 * value)
	{
		___timer_23 = value;
		Il2CppCodeGenWriteBarrier((&___timer_23), value);
	}

	inline static int32_t get_offset_of_timerFinished_24() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___timerFinished_24)); }
	inline bool get_timerFinished_24() const { return ___timerFinished_24; }
	inline bool* get_address_of_timerFinished_24() { return &___timerFinished_24; }
	inline void set_timerFinished_24(bool value)
	{
		___timerFinished_24 = value;
	}

	inline static int32_t get_offset_of_messageBoxPrefab_25() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913, ___messageBoxPrefab_25)); }
	inline GameObject_t1113636619 * get_messageBoxPrefab_25() const { return ___messageBoxPrefab_25; }
	inline GameObject_t1113636619 ** get_address_of_messageBoxPrefab_25() { return &___messageBoxPrefab_25; }
	inline void set_messageBoxPrefab_25(GameObject_t1113636619 * value)
	{
		___messageBoxPrefab_25 = value;
		Il2CppCodeGenWriteBarrier((&___messageBoxPrefab_25), value);
	}
};

struct PlaneManager_t2021199913_StaticFields
{
public:
	// PlaneManager/PlaneMode PlaneManager::CurrentPlaneMode
	int32_t ___CurrentPlaneMode_4;
	// System.Boolean PlaneManager::<GroundPlaneHitReceived>k__BackingField
	bool ___U3CGroundPlaneHitReceivedU3Ek__BackingField_5;
	// System.Boolean PlaneManager::<AnchorExists>k__BackingField
	bool ___U3CAnchorExistsU3Ek__BackingField_6;
	// Vuforia.TrackableBehaviour/Status PlaneManager::StatusCached
	int32_t ___StatusCached_21;
	// Vuforia.TrackableBehaviour/StatusInfo PlaneManager::StatusInfoCached
	int32_t ___StatusInfoCached_22;

public:
	inline static int32_t get_offset_of_CurrentPlaneMode_4() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913_StaticFields, ___CurrentPlaneMode_4)); }
	inline int32_t get_CurrentPlaneMode_4() const { return ___CurrentPlaneMode_4; }
	inline int32_t* get_address_of_CurrentPlaneMode_4() { return &___CurrentPlaneMode_4; }
	inline void set_CurrentPlaneMode_4(int32_t value)
	{
		___CurrentPlaneMode_4 = value;
	}

	inline static int32_t get_offset_of_U3CGroundPlaneHitReceivedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913_StaticFields, ___U3CGroundPlaneHitReceivedU3Ek__BackingField_5)); }
	inline bool get_U3CGroundPlaneHitReceivedU3Ek__BackingField_5() const { return ___U3CGroundPlaneHitReceivedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CGroundPlaneHitReceivedU3Ek__BackingField_5() { return &___U3CGroundPlaneHitReceivedU3Ek__BackingField_5; }
	inline void set_U3CGroundPlaneHitReceivedU3Ek__BackingField_5(bool value)
	{
		___U3CGroundPlaneHitReceivedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CAnchorExistsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913_StaticFields, ___U3CAnchorExistsU3Ek__BackingField_6)); }
	inline bool get_U3CAnchorExistsU3Ek__BackingField_6() const { return ___U3CAnchorExistsU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CAnchorExistsU3Ek__BackingField_6() { return &___U3CAnchorExistsU3Ek__BackingField_6; }
	inline void set_U3CAnchorExistsU3Ek__BackingField_6(bool value)
	{
		___U3CAnchorExistsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_StatusCached_21() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913_StaticFields, ___StatusCached_21)); }
	inline int32_t get_StatusCached_21() const { return ___StatusCached_21; }
	inline int32_t* get_address_of_StatusCached_21() { return &___StatusCached_21; }
	inline void set_StatusCached_21(int32_t value)
	{
		___StatusCached_21 = value;
	}

	inline static int32_t get_offset_of_StatusInfoCached_22() { return static_cast<int32_t>(offsetof(PlaneManager_t2021199913_StaticFields, ___StatusInfoCached_22)); }
	inline int32_t get_StatusInfoCached_22() const { return ___StatusInfoCached_22; }
	inline int32_t* get_address_of_StatusInfoCached_22() { return &___StatusInfoCached_22; }
	inline void set_StatusInfoCached_22(int32_t value)
	{
		___StatusInfoCached_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEMANAGER_T2021199913_H
#ifndef PRODUCTPLACEMENT_T2927687625_H
#define PRODUCTPLACEMENT_T2927687625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductPlacement
struct  ProductPlacement_t2927687625  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean ProductPlacement::<IsPlaced>k__BackingField
	bool ___U3CIsPlacedU3Ek__BackingField_4;
	// UnityEngine.GameObject ProductPlacement::augmentationObject
	GameObject_t1113636619 * ___augmentationObject_5;
	// UnityEngine.Transform ProductPlacement::pivot
	Transform_t3600365921 * ___pivot_6;
	// UnityEngine.Transform ProductPlacement::holder
	Transform_t3600365921 * ___holder_7;
	// UnityEngine.Transform ProductPlacement::visual
	Transform_t3600365921 * ___visual_8;
	// UnityEngine.BoxCollider ProductPlacement::boxCollider
	BoxCollider_t1640800422 * ___boxCollider_9;
	// UnityEngine.MeshRenderer ProductPlacement::objectRenderer
	MeshRenderer_t587009260 * ___objectRenderer_10;
	// UnityEngine.MeshRenderer ProductPlacement::objectShadow
	MeshRenderer_t587009260 * ___objectShadow_11;
	// UnityEngine.Animator ProductPlacement::animator
	Animator_t434523843 * ___animator_12;
	// System.Single ProductPlacement::productSize
	float ___productSize_13;
	// System.Boolean ProductPlacement::selecting
	bool ___selecting_14;
	// GroundPlaneUI ProductPlacement::groundPlaneUI
	GroundPlaneUI_t2709539096 * ___groundPlaneUI_15;
	// UnityEngine.Camera ProductPlacement::mainCamera
	Camera_t4157153871 * ___mainCamera_16;
	// UnityEngine.Ray ProductPlacement::cameraToPlaneRay
	Ray_t3785851493  ___cameraToPlaneRay_17;
	// UnityEngine.RaycastHit ProductPlacement::cameraToPlaneHit
	RaycastHit_t1056001966  ___cameraToPlaneHit_18;
	// UnityEngine.Ray ProductPlacement::cameraToObjectRay
	Ray_t3785851493  ___cameraToObjectRay_19;
	// UnityEngine.RaycastHit ProductPlacement::cameraToObjectHit
	RaycastHit_t1056001966  ___cameraToObjectHit_20;
	// UnityEngine.Vector3 ProductPlacement::productScale
	Vector3_t3722313464  ___productScale_21;
	// UnityEngine.Vector3 ProductPlacement::LocalOffset
	Vector3_t3722313464  ___LocalOffset_22;
	// UnityEngine.Vector3 ProductPlacement::targetPosition
	Vector3_t3722313464  ___targetPosition_23;
	// System.Single ProductPlacement::augmentationScale
	float ___augmentationScale_24;
	// System.String ProductPlacement::floorName
	String_t* ___floorName_25;
	// System.Boolean ProductPlacement::hold
	bool ___hold_26;
	// System.Boolean ProductPlacement::tap
	bool ___tap_27;

public:
	inline static int32_t get_offset_of_U3CIsPlacedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___U3CIsPlacedU3Ek__BackingField_4)); }
	inline bool get_U3CIsPlacedU3Ek__BackingField_4() const { return ___U3CIsPlacedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsPlacedU3Ek__BackingField_4() { return &___U3CIsPlacedU3Ek__BackingField_4; }
	inline void set_U3CIsPlacedU3Ek__BackingField_4(bool value)
	{
		___U3CIsPlacedU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_augmentationObject_5() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___augmentationObject_5)); }
	inline GameObject_t1113636619 * get_augmentationObject_5() const { return ___augmentationObject_5; }
	inline GameObject_t1113636619 ** get_address_of_augmentationObject_5() { return &___augmentationObject_5; }
	inline void set_augmentationObject_5(GameObject_t1113636619 * value)
	{
		___augmentationObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___augmentationObject_5), value);
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___pivot_6)); }
	inline Transform_t3600365921 * get_pivot_6() const { return ___pivot_6; }
	inline Transform_t3600365921 ** get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Transform_t3600365921 * value)
	{
		___pivot_6 = value;
		Il2CppCodeGenWriteBarrier((&___pivot_6), value);
	}

	inline static int32_t get_offset_of_holder_7() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___holder_7)); }
	inline Transform_t3600365921 * get_holder_7() const { return ___holder_7; }
	inline Transform_t3600365921 ** get_address_of_holder_7() { return &___holder_7; }
	inline void set_holder_7(Transform_t3600365921 * value)
	{
		___holder_7 = value;
		Il2CppCodeGenWriteBarrier((&___holder_7), value);
	}

	inline static int32_t get_offset_of_visual_8() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___visual_8)); }
	inline Transform_t3600365921 * get_visual_8() const { return ___visual_8; }
	inline Transform_t3600365921 ** get_address_of_visual_8() { return &___visual_8; }
	inline void set_visual_8(Transform_t3600365921 * value)
	{
		___visual_8 = value;
		Il2CppCodeGenWriteBarrier((&___visual_8), value);
	}

	inline static int32_t get_offset_of_boxCollider_9() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___boxCollider_9)); }
	inline BoxCollider_t1640800422 * get_boxCollider_9() const { return ___boxCollider_9; }
	inline BoxCollider_t1640800422 ** get_address_of_boxCollider_9() { return &___boxCollider_9; }
	inline void set_boxCollider_9(BoxCollider_t1640800422 * value)
	{
		___boxCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&___boxCollider_9), value);
	}

	inline static int32_t get_offset_of_objectRenderer_10() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___objectRenderer_10)); }
	inline MeshRenderer_t587009260 * get_objectRenderer_10() const { return ___objectRenderer_10; }
	inline MeshRenderer_t587009260 ** get_address_of_objectRenderer_10() { return &___objectRenderer_10; }
	inline void set_objectRenderer_10(MeshRenderer_t587009260 * value)
	{
		___objectRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___objectRenderer_10), value);
	}

	inline static int32_t get_offset_of_objectShadow_11() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___objectShadow_11)); }
	inline MeshRenderer_t587009260 * get_objectShadow_11() const { return ___objectShadow_11; }
	inline MeshRenderer_t587009260 ** get_address_of_objectShadow_11() { return &___objectShadow_11; }
	inline void set_objectShadow_11(MeshRenderer_t587009260 * value)
	{
		___objectShadow_11 = value;
		Il2CppCodeGenWriteBarrier((&___objectShadow_11), value);
	}

	inline static int32_t get_offset_of_animator_12() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___animator_12)); }
	inline Animator_t434523843 * get_animator_12() const { return ___animator_12; }
	inline Animator_t434523843 ** get_address_of_animator_12() { return &___animator_12; }
	inline void set_animator_12(Animator_t434523843 * value)
	{
		___animator_12 = value;
		Il2CppCodeGenWriteBarrier((&___animator_12), value);
	}

	inline static int32_t get_offset_of_productSize_13() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___productSize_13)); }
	inline float get_productSize_13() const { return ___productSize_13; }
	inline float* get_address_of_productSize_13() { return &___productSize_13; }
	inline void set_productSize_13(float value)
	{
		___productSize_13 = value;
	}

	inline static int32_t get_offset_of_selecting_14() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___selecting_14)); }
	inline bool get_selecting_14() const { return ___selecting_14; }
	inline bool* get_address_of_selecting_14() { return &___selecting_14; }
	inline void set_selecting_14(bool value)
	{
		___selecting_14 = value;
	}

	inline static int32_t get_offset_of_groundPlaneUI_15() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___groundPlaneUI_15)); }
	inline GroundPlaneUI_t2709539096 * get_groundPlaneUI_15() const { return ___groundPlaneUI_15; }
	inline GroundPlaneUI_t2709539096 ** get_address_of_groundPlaneUI_15() { return &___groundPlaneUI_15; }
	inline void set_groundPlaneUI_15(GroundPlaneUI_t2709539096 * value)
	{
		___groundPlaneUI_15 = value;
		Il2CppCodeGenWriteBarrier((&___groundPlaneUI_15), value);
	}

	inline static int32_t get_offset_of_mainCamera_16() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___mainCamera_16)); }
	inline Camera_t4157153871 * get_mainCamera_16() const { return ___mainCamera_16; }
	inline Camera_t4157153871 ** get_address_of_mainCamera_16() { return &___mainCamera_16; }
	inline void set_mainCamera_16(Camera_t4157153871 * value)
	{
		___mainCamera_16 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_16), value);
	}

	inline static int32_t get_offset_of_cameraToPlaneRay_17() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___cameraToPlaneRay_17)); }
	inline Ray_t3785851493  get_cameraToPlaneRay_17() const { return ___cameraToPlaneRay_17; }
	inline Ray_t3785851493 * get_address_of_cameraToPlaneRay_17() { return &___cameraToPlaneRay_17; }
	inline void set_cameraToPlaneRay_17(Ray_t3785851493  value)
	{
		___cameraToPlaneRay_17 = value;
	}

	inline static int32_t get_offset_of_cameraToPlaneHit_18() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___cameraToPlaneHit_18)); }
	inline RaycastHit_t1056001966  get_cameraToPlaneHit_18() const { return ___cameraToPlaneHit_18; }
	inline RaycastHit_t1056001966 * get_address_of_cameraToPlaneHit_18() { return &___cameraToPlaneHit_18; }
	inline void set_cameraToPlaneHit_18(RaycastHit_t1056001966  value)
	{
		___cameraToPlaneHit_18 = value;
	}

	inline static int32_t get_offset_of_cameraToObjectRay_19() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___cameraToObjectRay_19)); }
	inline Ray_t3785851493  get_cameraToObjectRay_19() const { return ___cameraToObjectRay_19; }
	inline Ray_t3785851493 * get_address_of_cameraToObjectRay_19() { return &___cameraToObjectRay_19; }
	inline void set_cameraToObjectRay_19(Ray_t3785851493  value)
	{
		___cameraToObjectRay_19 = value;
	}

	inline static int32_t get_offset_of_cameraToObjectHit_20() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___cameraToObjectHit_20)); }
	inline RaycastHit_t1056001966  get_cameraToObjectHit_20() const { return ___cameraToObjectHit_20; }
	inline RaycastHit_t1056001966 * get_address_of_cameraToObjectHit_20() { return &___cameraToObjectHit_20; }
	inline void set_cameraToObjectHit_20(RaycastHit_t1056001966  value)
	{
		___cameraToObjectHit_20 = value;
	}

	inline static int32_t get_offset_of_productScale_21() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___productScale_21)); }
	inline Vector3_t3722313464  get_productScale_21() const { return ___productScale_21; }
	inline Vector3_t3722313464 * get_address_of_productScale_21() { return &___productScale_21; }
	inline void set_productScale_21(Vector3_t3722313464  value)
	{
		___productScale_21 = value;
	}

	inline static int32_t get_offset_of_LocalOffset_22() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___LocalOffset_22)); }
	inline Vector3_t3722313464  get_LocalOffset_22() const { return ___LocalOffset_22; }
	inline Vector3_t3722313464 * get_address_of_LocalOffset_22() { return &___LocalOffset_22; }
	inline void set_LocalOffset_22(Vector3_t3722313464  value)
	{
		___LocalOffset_22 = value;
	}

	inline static int32_t get_offset_of_targetPosition_23() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___targetPosition_23)); }
	inline Vector3_t3722313464  get_targetPosition_23() const { return ___targetPosition_23; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_23() { return &___targetPosition_23; }
	inline void set_targetPosition_23(Vector3_t3722313464  value)
	{
		___targetPosition_23 = value;
	}

	inline static int32_t get_offset_of_augmentationScale_24() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___augmentationScale_24)); }
	inline float get_augmentationScale_24() const { return ___augmentationScale_24; }
	inline float* get_address_of_augmentationScale_24() { return &___augmentationScale_24; }
	inline void set_augmentationScale_24(float value)
	{
		___augmentationScale_24 = value;
	}

	inline static int32_t get_offset_of_floorName_25() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___floorName_25)); }
	inline String_t* get_floorName_25() const { return ___floorName_25; }
	inline String_t** get_address_of_floorName_25() { return &___floorName_25; }
	inline void set_floorName_25(String_t* value)
	{
		___floorName_25 = value;
		Il2CppCodeGenWriteBarrier((&___floorName_25), value);
	}

	inline static int32_t get_offset_of_hold_26() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___hold_26)); }
	inline bool get_hold_26() const { return ___hold_26; }
	inline bool* get_address_of_hold_26() { return &___hold_26; }
	inline void set_hold_26(bool value)
	{
		___hold_26 = value;
	}

	inline static int32_t get_offset_of_tap_27() { return static_cast<int32_t>(offsetof(ProductPlacement_t2927687625, ___tap_27)); }
	inline bool get_tap_27() const { return ___tap_27; }
	inline bool* get_address_of_tap_27() { return &___tap_27; }
	inline void set_tap_27(bool value)
	{
		___tap_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTPLACEMENT_T2927687625_H
#ifndef ROCKPILECONTROLLER_T2893225296_H
#define ROCKPILECONTROLLER_T2893225296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RockPileController
struct  RockPileController_t2893225296  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform RockPileController::m_GrabbableRock
	Transform_t3600365921 * ___m_GrabbableRock_4;
	// FadeObject RockPileController::fadeController
	FadeObject_t1880495183 * ___fadeController_5;

public:
	inline static int32_t get_offset_of_m_GrabbableRock_4() { return static_cast<int32_t>(offsetof(RockPileController_t2893225296, ___m_GrabbableRock_4)); }
	inline Transform_t3600365921 * get_m_GrabbableRock_4() const { return ___m_GrabbableRock_4; }
	inline Transform_t3600365921 ** get_address_of_m_GrabbableRock_4() { return &___m_GrabbableRock_4; }
	inline void set_m_GrabbableRock_4(Transform_t3600365921 * value)
	{
		___m_GrabbableRock_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GrabbableRock_4), value);
	}

	inline static int32_t get_offset_of_fadeController_5() { return static_cast<int32_t>(offsetof(RockPileController_t2893225296, ___fadeController_5)); }
	inline FadeObject_t1880495183 * get_fadeController_5() const { return ___fadeController_5; }
	inline FadeObject_t1880495183 ** get_address_of_fadeController_5() { return &___fadeController_5; }
	inline void set_fadeController_5(FadeObject_t1880495183 * value)
	{
		___fadeController_5 = value;
		Il2CppCodeGenWriteBarrier((&___fadeController_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROCKPILECONTROLLER_T2893225296_H
#ifndef SAMPLESMAINMENU_T83803002_H
#define SAMPLESMAINMENU_T83803002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SamplesMainMenu
struct  SamplesMainMenu_t83803002  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas SamplesMainMenu::aboutCanvas
	Canvas_t3310196443 * ___aboutCanvas_8;
	// UnityEngine.UI.Text SamplesMainMenu::aboutTitle
	Text_t1901882714 * ___aboutTitle_9;
	// TMPro.TextMeshProUGUI SamplesMainMenu::aboutDescription
	TextMeshProUGUI_t529313277 * ___aboutDescription_10;
	// AboutScreenInfo SamplesMainMenu::aboutScreenInfo
	AboutScreenInfo_t2560997449 * ___aboutScreenInfo_11;
	// SafeAreaManager SamplesMainMenu::safeAreaManager
	SafeAreaManager_t2083807765 * ___safeAreaManager_12;
	// UnityEngine.Color SamplesMainMenu::lightGrey
	Color_t2555686324  ___lightGrey_13;

public:
	inline static int32_t get_offset_of_aboutCanvas_8() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002, ___aboutCanvas_8)); }
	inline Canvas_t3310196443 * get_aboutCanvas_8() const { return ___aboutCanvas_8; }
	inline Canvas_t3310196443 ** get_address_of_aboutCanvas_8() { return &___aboutCanvas_8; }
	inline void set_aboutCanvas_8(Canvas_t3310196443 * value)
	{
		___aboutCanvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___aboutCanvas_8), value);
	}

	inline static int32_t get_offset_of_aboutTitle_9() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002, ___aboutTitle_9)); }
	inline Text_t1901882714 * get_aboutTitle_9() const { return ___aboutTitle_9; }
	inline Text_t1901882714 ** get_address_of_aboutTitle_9() { return &___aboutTitle_9; }
	inline void set_aboutTitle_9(Text_t1901882714 * value)
	{
		___aboutTitle_9 = value;
		Il2CppCodeGenWriteBarrier((&___aboutTitle_9), value);
	}

	inline static int32_t get_offset_of_aboutDescription_10() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002, ___aboutDescription_10)); }
	inline TextMeshProUGUI_t529313277 * get_aboutDescription_10() const { return ___aboutDescription_10; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_aboutDescription_10() { return &___aboutDescription_10; }
	inline void set_aboutDescription_10(TextMeshProUGUI_t529313277 * value)
	{
		___aboutDescription_10 = value;
		Il2CppCodeGenWriteBarrier((&___aboutDescription_10), value);
	}

	inline static int32_t get_offset_of_aboutScreenInfo_11() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002, ___aboutScreenInfo_11)); }
	inline AboutScreenInfo_t2560997449 * get_aboutScreenInfo_11() const { return ___aboutScreenInfo_11; }
	inline AboutScreenInfo_t2560997449 ** get_address_of_aboutScreenInfo_11() { return &___aboutScreenInfo_11; }
	inline void set_aboutScreenInfo_11(AboutScreenInfo_t2560997449 * value)
	{
		___aboutScreenInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___aboutScreenInfo_11), value);
	}

	inline static int32_t get_offset_of_safeAreaManager_12() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002, ___safeAreaManager_12)); }
	inline SafeAreaManager_t2083807765 * get_safeAreaManager_12() const { return ___safeAreaManager_12; }
	inline SafeAreaManager_t2083807765 ** get_address_of_safeAreaManager_12() { return &___safeAreaManager_12; }
	inline void set_safeAreaManager_12(SafeAreaManager_t2083807765 * value)
	{
		___safeAreaManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___safeAreaManager_12), value);
	}

	inline static int32_t get_offset_of_lightGrey_13() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002, ___lightGrey_13)); }
	inline Color_t2555686324  get_lightGrey_13() const { return ___lightGrey_13; }
	inline Color_t2555686324 * get_address_of_lightGrey_13() { return &___lightGrey_13; }
	inline void set_lightGrey_13(Color_t2555686324  value)
	{
		___lightGrey_13 = value;
	}
};

struct SamplesMainMenu_t83803002_StaticFields
{
public:
	// SamplesMainMenu/MenuItem SamplesMainMenu::menuItem
	int32_t ___menuItem_4;
	// System.Boolean SamplesMainMenu::isAboutScreenVisible
	bool ___isAboutScreenVisible_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SamplesMainMenu::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_14;

public:
	inline static int32_t get_offset_of_menuItem_4() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002_StaticFields, ___menuItem_4)); }
	inline int32_t get_menuItem_4() const { return ___menuItem_4; }
	inline int32_t* get_address_of_menuItem_4() { return &___menuItem_4; }
	inline void set_menuItem_4(int32_t value)
	{
		___menuItem_4 = value;
	}

	inline static int32_t get_offset_of_isAboutScreenVisible_7() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002_StaticFields, ___isAboutScreenVisible_7)); }
	inline bool get_isAboutScreenVisible_7() const { return ___isAboutScreenVisible_7; }
	inline bool* get_address_of_isAboutScreenVisible_7() { return &___isAboutScreenVisible_7; }
	inline void set_isAboutScreenVisible_7(bool value)
	{
		___isAboutScreenVisible_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_14() { return static_cast<int32_t>(offsetof(SamplesMainMenu_t83803002_StaticFields, ___U3CU3Ef__switchU24map0_14)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_14() const { return ___U3CU3Ef__switchU24map0_14; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_14() { return &___U3CU3Ef__switchU24map0_14; }
	inline void set_U3CU3Ef__switchU24map0_14(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLESMAINMENU_T83803002_H
#ifndef SAMPLESNAVIGATIONHANDLER_T3426315339_H
#define SAMPLESNAVIGATIONHANDLER_T3426315339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SamplesNavigationHandler
struct  SamplesNavigationHandler_t3426315339  : public MonoBehaviour_t3962482529
{
public:
	// System.String SamplesNavigationHandler::currentSceneName
	String_t* ___currentSceneName_4;

public:
	inline static int32_t get_offset_of_currentSceneName_4() { return static_cast<int32_t>(offsetof(SamplesNavigationHandler_t3426315339, ___currentSceneName_4)); }
	inline String_t* get_currentSceneName_4() const { return ___currentSceneName_4; }
	inline String_t** get_address_of_currentSceneName_4() { return &___currentSceneName_4; }
	inline void set_currentSceneName_4(String_t* value)
	{
		___currentSceneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentSceneName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLESNAVIGATIONHANDLER_T3426315339_H
#ifndef SCENEORIENTATION_T160940122_H
#define SCENEORIENTATION_T160940122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneOrientation
struct  SceneOrientation_t160940122  : public MonoBehaviour_t3962482529
{
public:
	// SceneOrientation/Orientation SceneOrientation::sceneOrientation
	int32_t ___sceneOrientation_4;

public:
	inline static int32_t get_offset_of_sceneOrientation_4() { return static_cast<int32_t>(offsetof(SceneOrientation_t160940122, ___sceneOrientation_4)); }
	inline int32_t get_sceneOrientation_4() const { return ___sceneOrientation_4; }
	inline int32_t* get_address_of_sceneOrientation_4() { return &___sceneOrientation_4; }
	inline void set_sceneOrientation_4(int32_t value)
	{
		___sceneOrientation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEORIENTATION_T160940122_H
#ifndef STATUSMESSAGE_T696038889_H
#define STATUSMESSAGE_T696038889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StatusMessage
struct  StatusMessage_t696038889  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.CanvasGroup StatusMessage::canvasGroup
	CanvasGroup_t4083511760 * ___canvasGroup_4;
	// UnityEngine.UI.Text StatusMessage::message
	Text_t1901882714 * ___message_5;
	// System.Boolean StatusMessage::initialized
	bool ___initialized_6;
	// System.Boolean StatusMessage::showing
	bool ___showing_7;

public:
	inline static int32_t get_offset_of_canvasGroup_4() { return static_cast<int32_t>(offsetof(StatusMessage_t696038889, ___canvasGroup_4)); }
	inline CanvasGroup_t4083511760 * get_canvasGroup_4() const { return ___canvasGroup_4; }
	inline CanvasGroup_t4083511760 ** get_address_of_canvasGroup_4() { return &___canvasGroup_4; }
	inline void set_canvasGroup_4(CanvasGroup_t4083511760 * value)
	{
		___canvasGroup_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_4), value);
	}

	inline static int32_t get_offset_of_message_5() { return static_cast<int32_t>(offsetof(StatusMessage_t696038889, ___message_5)); }
	inline Text_t1901882714 * get_message_5() const { return ___message_5; }
	inline Text_t1901882714 ** get_address_of_message_5() { return &___message_5; }
	inline void set_message_5(Text_t1901882714 * value)
	{
		___message_5 = value;
		Il2CppCodeGenWriteBarrier((&___message_5), value);
	}

	inline static int32_t get_offset_of_initialized_6() { return static_cast<int32_t>(offsetof(StatusMessage_t696038889, ___initialized_6)); }
	inline bool get_initialized_6() const { return ___initialized_6; }
	inline bool* get_address_of_initialized_6() { return &___initialized_6; }
	inline void set_initialized_6(bool value)
	{
		___initialized_6 = value;
	}

	inline static int32_t get_offset_of_showing_7() { return static_cast<int32_t>(offsetof(StatusMessage_t696038889, ___showing_7)); }
	inline bool get_showing_7() const { return ___showing_7; }
	inline bool* get_address_of_showing_7() { return &___showing_7; }
	inline void set_showing_7(bool value)
	{
		___showing_7 = value;
	}
};

struct StatusMessage_t696038889_StaticFields
{
public:
	// StatusMessage StatusMessage::statusMessage
	StatusMessage_t696038889 * ___statusMessage_8;

public:
	inline static int32_t get_offset_of_statusMessage_8() { return static_cast<int32_t>(offsetof(StatusMessage_t696038889_StaticFields, ___statusMessage_8)); }
	inline StatusMessage_t696038889 * get_statusMessage_8() const { return ___statusMessage_8; }
	inline StatusMessage_t696038889 ** get_address_of_statusMessage_8() { return &___statusMessage_8; }
	inline void set_statusMessage_8(StatusMessage_t696038889 * value)
	{
		___statusMessage_8 = value;
		Il2CppCodeGenWriteBarrier((&___statusMessage_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSMESSAGE_T696038889_H
#ifndef TAPHANDLER_T334234343_H
#define TAPHANDLER_T334234343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TapHandler
struct  TapHandler_t334234343  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TapHandler::mTimeSinceLastTap
	float ___mTimeSinceLastTap_5;
	// MenuOptions TapHandler::m_MenuOptions
	MenuOptions_t1951716431 * ___m_MenuOptions_6;
	// CameraSettings TapHandler::m_CameraSettings
	CameraSettings_t3152619780 * ___m_CameraSettings_7;
	// System.Int32 TapHandler::mTapCount
	int32_t ___mTapCount_8;

public:
	inline static int32_t get_offset_of_mTimeSinceLastTap_5() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTimeSinceLastTap_5)); }
	inline float get_mTimeSinceLastTap_5() const { return ___mTimeSinceLastTap_5; }
	inline float* get_address_of_mTimeSinceLastTap_5() { return &___mTimeSinceLastTap_5; }
	inline void set_mTimeSinceLastTap_5(float value)
	{
		___mTimeSinceLastTap_5 = value;
	}

	inline static int32_t get_offset_of_m_MenuOptions_6() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___m_MenuOptions_6)); }
	inline MenuOptions_t1951716431 * get_m_MenuOptions_6() const { return ___m_MenuOptions_6; }
	inline MenuOptions_t1951716431 ** get_address_of_m_MenuOptions_6() { return &___m_MenuOptions_6; }
	inline void set_m_MenuOptions_6(MenuOptions_t1951716431 * value)
	{
		___m_MenuOptions_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MenuOptions_6), value);
	}

	inline static int32_t get_offset_of_m_CameraSettings_7() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___m_CameraSettings_7)); }
	inline CameraSettings_t3152619780 * get_m_CameraSettings_7() const { return ___m_CameraSettings_7; }
	inline CameraSettings_t3152619780 ** get_address_of_m_CameraSettings_7() { return &___m_CameraSettings_7; }
	inline void set_m_CameraSettings_7(CameraSettings_t3152619780 * value)
	{
		___m_CameraSettings_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraSettings_7), value);
	}

	inline static int32_t get_offset_of_mTapCount_8() { return static_cast<int32_t>(offsetof(TapHandler_t334234343, ___mTapCount_8)); }
	inline int32_t get_mTapCount_8() const { return ___mTapCount_8; }
	inline int32_t* get_address_of_mTapCount_8() { return &___mTapCount_8; }
	inline void set_mTapCount_8(int32_t value)
	{
		___mTapCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPHANDLER_T334234343_H
#ifndef TOUCHHANDLER_T3441426771_H
#define TOUCHHANDLER_T3441426771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchHandler
struct  TouchHandler_t3441426771  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TouchHandler::augmentationObject
	Transform_t3600365921 * ___augmentationObject_4;
	// System.Boolean TouchHandler::enableRotation
	bool ___enableRotation_5;
	// System.Boolean TouchHandler::enablePinchScaling
	bool ___enablePinchScaling_6;
	// UnityEngine.Touch[] TouchHandler::touches
	TouchU5BU5D_t1849554061* ___touches_9;
	// System.Boolean TouchHandler::isFirstFrameWithTwoTouches
	bool ___isFirstFrameWithTwoTouches_11;
	// System.Single TouchHandler::cachedTouchAngle
	float ___cachedTouchAngle_12;
	// System.Single TouchHandler::cachedTouchDistance
	float ___cachedTouchDistance_13;
	// System.Single TouchHandler::cachedAugmentationScale
	float ___cachedAugmentationScale_14;
	// UnityEngine.Vector3 TouchHandler::cachedAugmentationRotation
	Vector3_t3722313464  ___cachedAugmentationRotation_15;

public:
	inline static int32_t get_offset_of_augmentationObject_4() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___augmentationObject_4)); }
	inline Transform_t3600365921 * get_augmentationObject_4() const { return ___augmentationObject_4; }
	inline Transform_t3600365921 ** get_address_of_augmentationObject_4() { return &___augmentationObject_4; }
	inline void set_augmentationObject_4(Transform_t3600365921 * value)
	{
		___augmentationObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___augmentationObject_4), value);
	}

	inline static int32_t get_offset_of_enableRotation_5() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___enableRotation_5)); }
	inline bool get_enableRotation_5() const { return ___enableRotation_5; }
	inline bool* get_address_of_enableRotation_5() { return &___enableRotation_5; }
	inline void set_enableRotation_5(bool value)
	{
		___enableRotation_5 = value;
	}

	inline static int32_t get_offset_of_enablePinchScaling_6() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___enablePinchScaling_6)); }
	inline bool get_enablePinchScaling_6() const { return ___enablePinchScaling_6; }
	inline bool* get_address_of_enablePinchScaling_6() { return &___enablePinchScaling_6; }
	inline void set_enablePinchScaling_6(bool value)
	{
		___enablePinchScaling_6 = value;
	}

	inline static int32_t get_offset_of_touches_9() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___touches_9)); }
	inline TouchU5BU5D_t1849554061* get_touches_9() const { return ___touches_9; }
	inline TouchU5BU5D_t1849554061** get_address_of_touches_9() { return &___touches_9; }
	inline void set_touches_9(TouchU5BU5D_t1849554061* value)
	{
		___touches_9 = value;
		Il2CppCodeGenWriteBarrier((&___touches_9), value);
	}

	inline static int32_t get_offset_of_isFirstFrameWithTwoTouches_11() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___isFirstFrameWithTwoTouches_11)); }
	inline bool get_isFirstFrameWithTwoTouches_11() const { return ___isFirstFrameWithTwoTouches_11; }
	inline bool* get_address_of_isFirstFrameWithTwoTouches_11() { return &___isFirstFrameWithTwoTouches_11; }
	inline void set_isFirstFrameWithTwoTouches_11(bool value)
	{
		___isFirstFrameWithTwoTouches_11 = value;
	}

	inline static int32_t get_offset_of_cachedTouchAngle_12() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___cachedTouchAngle_12)); }
	inline float get_cachedTouchAngle_12() const { return ___cachedTouchAngle_12; }
	inline float* get_address_of_cachedTouchAngle_12() { return &___cachedTouchAngle_12; }
	inline void set_cachedTouchAngle_12(float value)
	{
		___cachedTouchAngle_12 = value;
	}

	inline static int32_t get_offset_of_cachedTouchDistance_13() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___cachedTouchDistance_13)); }
	inline float get_cachedTouchDistance_13() const { return ___cachedTouchDistance_13; }
	inline float* get_address_of_cachedTouchDistance_13() { return &___cachedTouchDistance_13; }
	inline void set_cachedTouchDistance_13(float value)
	{
		___cachedTouchDistance_13 = value;
	}

	inline static int32_t get_offset_of_cachedAugmentationScale_14() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___cachedAugmentationScale_14)); }
	inline float get_cachedAugmentationScale_14() const { return ___cachedAugmentationScale_14; }
	inline float* get_address_of_cachedAugmentationScale_14() { return &___cachedAugmentationScale_14; }
	inline void set_cachedAugmentationScale_14(float value)
	{
		___cachedAugmentationScale_14 = value;
	}

	inline static int32_t get_offset_of_cachedAugmentationRotation_15() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771, ___cachedAugmentationRotation_15)); }
	inline Vector3_t3722313464  get_cachedAugmentationRotation_15() const { return ___cachedAugmentationRotation_15; }
	inline Vector3_t3722313464 * get_address_of_cachedAugmentationRotation_15() { return &___cachedAugmentationRotation_15; }
	inline void set_cachedAugmentationRotation_15(Vector3_t3722313464  value)
	{
		___cachedAugmentationRotation_15 = value;
	}
};

struct TouchHandler_t3441426771_StaticFields
{
public:
	// System.Int32 TouchHandler::lastTouchCount
	int32_t ___lastTouchCount_10;

public:
	inline static int32_t get_offset_of_lastTouchCount_10() { return static_cast<int32_t>(offsetof(TouchHandler_t3441426771_StaticFields, ___lastTouchCount_10)); }
	inline int32_t get_lastTouchCount_10() const { return ___lastTouchCount_10; }
	inline int32_t* get_address_of_lastTouchCount_10() { return &___lastTouchCount_10; }
	inline void set_lastTouchCount_10(int32_t value)
	{
		___lastTouchCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHANDLER_T3441426771_H
#ifndef TRACKABLESETTINGS_T2862243993_H
#define TRACKABLESETTINGS_T2862243993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrackableSettings
struct  TrackableSettings_t2862243993  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TrackableSettings::deviceTrackerEnabled
	bool ___deviceTrackerEnabled_4;
	// Vuforia.PositionalDeviceTracker TrackableSettings::positionalDeviceTracker
	PositionalDeviceTracker_t656722001 * ___positionalDeviceTracker_5;
	// System.Timers.Timer TrackableSettings::relocalizationStatusDelayTimer
	Timer_t1767341190 * ___relocalizationStatusDelayTimer_6;
	// System.Timers.Timer TrackableSettings::resetDeviceTrackerTimer
	Timer_t1767341190 * ___resetDeviceTrackerTimer_7;

public:
	inline static int32_t get_offset_of_deviceTrackerEnabled_4() { return static_cast<int32_t>(offsetof(TrackableSettings_t2862243993, ___deviceTrackerEnabled_4)); }
	inline bool get_deviceTrackerEnabled_4() const { return ___deviceTrackerEnabled_4; }
	inline bool* get_address_of_deviceTrackerEnabled_4() { return &___deviceTrackerEnabled_4; }
	inline void set_deviceTrackerEnabled_4(bool value)
	{
		___deviceTrackerEnabled_4 = value;
	}

	inline static int32_t get_offset_of_positionalDeviceTracker_5() { return static_cast<int32_t>(offsetof(TrackableSettings_t2862243993, ___positionalDeviceTracker_5)); }
	inline PositionalDeviceTracker_t656722001 * get_positionalDeviceTracker_5() const { return ___positionalDeviceTracker_5; }
	inline PositionalDeviceTracker_t656722001 ** get_address_of_positionalDeviceTracker_5() { return &___positionalDeviceTracker_5; }
	inline void set_positionalDeviceTracker_5(PositionalDeviceTracker_t656722001 * value)
	{
		___positionalDeviceTracker_5 = value;
		Il2CppCodeGenWriteBarrier((&___positionalDeviceTracker_5), value);
	}

	inline static int32_t get_offset_of_relocalizationStatusDelayTimer_6() { return static_cast<int32_t>(offsetof(TrackableSettings_t2862243993, ___relocalizationStatusDelayTimer_6)); }
	inline Timer_t1767341190 * get_relocalizationStatusDelayTimer_6() const { return ___relocalizationStatusDelayTimer_6; }
	inline Timer_t1767341190 ** get_address_of_relocalizationStatusDelayTimer_6() { return &___relocalizationStatusDelayTimer_6; }
	inline void set_relocalizationStatusDelayTimer_6(Timer_t1767341190 * value)
	{
		___relocalizationStatusDelayTimer_6 = value;
		Il2CppCodeGenWriteBarrier((&___relocalizationStatusDelayTimer_6), value);
	}

	inline static int32_t get_offset_of_resetDeviceTrackerTimer_7() { return static_cast<int32_t>(offsetof(TrackableSettings_t2862243993, ___resetDeviceTrackerTimer_7)); }
	inline Timer_t1767341190 * get_resetDeviceTrackerTimer_7() const { return ___resetDeviceTrackerTimer_7; }
	inline Timer_t1767341190 ** get_address_of_resetDeviceTrackerTimer_7() { return &___resetDeviceTrackerTimer_7; }
	inline void set_resetDeviceTrackerTimer_7(Timer_t1767341190 * value)
	{
		___resetDeviceTrackerTimer_7 = value;
		Il2CppCodeGenWriteBarrier((&___resetDeviceTrackerTimer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLESETTINGS_T2862243993_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef ACCORDION_T2301662264_H
#define ACCORDION_T2301662264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Accordion
struct  Accordion_t2301662264  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.Accordion/Transition UnityEngine.UI.Extensions.Accordion::m_Transition
	int32_t ___m_Transition_4;
	// System.Single UnityEngine.UI.Extensions.Accordion::m_TransitionDuration
	float ___m_TransitionDuration_5;

public:
	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Accordion_t2301662264, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_TransitionDuration_5() { return static_cast<int32_t>(offsetof(Accordion_t2301662264, ___m_TransitionDuration_5)); }
	inline float get_m_TransitionDuration_5() const { return ___m_TransitionDuration_5; }
	inline float* get_address_of_m_TransitionDuration_5() { return &___m_TransitionDuration_5; }
	inline void set_m_TransitionDuration_5(float value)
	{
		___m_TransitionDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCORDION_T2301662264_H
#ifndef AUTOCOMPLETECOMBOBOX_T2765567798_H
#define AUTOCOMPLETECOMBOBOX_T2765567798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AutoCompleteComboBox
struct  AutoCompleteComboBox_t2765567798  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::disabledTextColor
	Color_t2555686324  ___disabledTextColor_4;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.AutoCompleteComboBox::<SelectedItem>k__BackingField
	DropDownListItem_t337244270 * ___U3CSelectedItemU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::AvailableOptions
	List_1_t3319525431 * ___AvailableOptions_6;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_isPanelActive
	bool ____isPanelActive_7;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_hasDrawnOnce
	bool ____hasDrawnOnce_8;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.AutoCompleteComboBox::_mainInput
	InputField_t3762917431 * ____mainInput_9;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_inputRT
	RectTransform_t3704657025 * ____inputRT_10;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_11;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_overlayRT
	RectTransform_t3704657025 * ____overlayRT_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollPanelRT
	RectTransform_t3704657025 * ____scrollPanelRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollBarRT
	RectTransform_t3704657025 * ____scrollBarRT_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_slidingAreaRT
	RectTransform_t3704657025 * ____slidingAreaRT_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_itemsPanelRT
	RectTransform_t3704657025 * ____itemsPanelRT_16;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.AutoCompleteComboBox::_canvas
	Canvas_t3310196443 * ____canvas_17;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AutoCompleteComboBox::_canvasRT
	RectTransform_t3704657025 * ____canvasRT_18;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollRect
	ScrollRect_t4137855814 * ____scrollRect_19;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::_panelItems
	List_1_t3319525431 * ____panelItems_20;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.AutoCompleteComboBox::_prunedPanelItems
	List_1_t3319525431 * ____prunedPanelItems_21;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> UnityEngine.UI.Extensions.AutoCompleteComboBox::panelObjects
	Dictionary_2_t898892918 * ___panelObjects_22;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.AutoCompleteComboBox::itemTemplate
	GameObject_t1113636619 * ___itemTemplate_23;
	// System.String UnityEngine.UI.Extensions.AutoCompleteComboBox::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_24;
	// System.Single UnityEngine.UI.Extensions.AutoCompleteComboBox::_scrollBarWidth
	float ____scrollBarWidth_25;
	// System.Int32 UnityEngine.UI.Extensions.AutoCompleteComboBox::_itemsToDisplay
	int32_t ____itemsToDisplay_26;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::SelectFirstItemOnStart
	bool ___SelectFirstItemOnStart_27;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_ChangeInputTextColorBasedOnMatchingItems
	bool ____ChangeInputTextColorBasedOnMatchingItems_28;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::ValidSelectionTextColor
	Color_t2555686324  ___ValidSelectionTextColor_29;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::MatchingItemsRemainingTextColor
	Color_t2555686324  ___MatchingItemsRemainingTextColor_30;
	// UnityEngine.Color UnityEngine.UI.Extensions.AutoCompleteComboBox::NoItemsRemainingTextColor
	Color_t2555686324  ___NoItemsRemainingTextColor_31;
	// UnityEngine.UI.Extensions.AutoCompleteSearchType UnityEngine.UI.Extensions.AutoCompleteComboBox::autocompleteSearchType
	int32_t ___autocompleteSearchType_32;
	// System.Boolean UnityEngine.UI.Extensions.AutoCompleteComboBox::_selectionIsValid
	bool ____selectionIsValid_33;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionTextChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionTextChanged
	SelectionTextChangedEvent_t4051177638 * ___OnSelectionTextChanged_34;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionValidityChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionValidityChanged
	SelectionValidityChangedEvent_t954817928 * ___OnSelectionValidityChanged_35;
	// UnityEngine.UI.Extensions.AutoCompleteComboBox/SelectionChangedEvent UnityEngine.UI.Extensions.AutoCompleteComboBox::OnSelectionChanged
	SelectionChangedEvent_t1822043360 * ___OnSelectionChanged_36;

public:
	inline static int32_t get_offset_of_disabledTextColor_4() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___disabledTextColor_4)); }
	inline Color_t2555686324  get_disabledTextColor_4() const { return ___disabledTextColor_4; }
	inline Color_t2555686324 * get_address_of_disabledTextColor_4() { return &___disabledTextColor_4; }
	inline void set_disabledTextColor_4(Color_t2555686324  value)
	{
		___disabledTextColor_4 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___U3CSelectedItemU3Ek__BackingField_5)); }
	inline DropDownListItem_t337244270 * get_U3CSelectedItemU3Ek__BackingField_5() const { return ___U3CSelectedItemU3Ek__BackingField_5; }
	inline DropDownListItem_t337244270 ** get_address_of_U3CSelectedItemU3Ek__BackingField_5() { return &___U3CSelectedItemU3Ek__BackingField_5; }
	inline void set_U3CSelectedItemU3Ek__BackingField_5(DropDownListItem_t337244270 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_AvailableOptions_6() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___AvailableOptions_6)); }
	inline List_1_t3319525431 * get_AvailableOptions_6() const { return ___AvailableOptions_6; }
	inline List_1_t3319525431 ** get_address_of_AvailableOptions_6() { return &___AvailableOptions_6; }
	inline void set_AvailableOptions_6(List_1_t3319525431 * value)
	{
		___AvailableOptions_6 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableOptions_6), value);
	}

	inline static int32_t get_offset_of__isPanelActive_7() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____isPanelActive_7)); }
	inline bool get__isPanelActive_7() const { return ____isPanelActive_7; }
	inline bool* get_address_of__isPanelActive_7() { return &____isPanelActive_7; }
	inline void set__isPanelActive_7(bool value)
	{
		____isPanelActive_7 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_8() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____hasDrawnOnce_8)); }
	inline bool get__hasDrawnOnce_8() const { return ____hasDrawnOnce_8; }
	inline bool* get_address_of__hasDrawnOnce_8() { return &____hasDrawnOnce_8; }
	inline void set__hasDrawnOnce_8(bool value)
	{
		____hasDrawnOnce_8 = value;
	}

	inline static int32_t get_offset_of__mainInput_9() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____mainInput_9)); }
	inline InputField_t3762917431 * get__mainInput_9() const { return ____mainInput_9; }
	inline InputField_t3762917431 ** get_address_of__mainInput_9() { return &____mainInput_9; }
	inline void set__mainInput_9(InputField_t3762917431 * value)
	{
		____mainInput_9 = value;
		Il2CppCodeGenWriteBarrier((&____mainInput_9), value);
	}

	inline static int32_t get_offset_of__inputRT_10() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____inputRT_10)); }
	inline RectTransform_t3704657025 * get__inputRT_10() const { return ____inputRT_10; }
	inline RectTransform_t3704657025 ** get_address_of__inputRT_10() { return &____inputRT_10; }
	inline void set__inputRT_10(RectTransform_t3704657025 * value)
	{
		____inputRT_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputRT_10), value);
	}

	inline static int32_t get_offset_of__rectTransform_11() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____rectTransform_11)); }
	inline RectTransform_t3704657025 * get__rectTransform_11() const { return ____rectTransform_11; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_11() { return &____rectTransform_11; }
	inline void set__rectTransform_11(RectTransform_t3704657025 * value)
	{
		____rectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_11), value);
	}

	inline static int32_t get_offset_of__overlayRT_12() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____overlayRT_12)); }
	inline RectTransform_t3704657025 * get__overlayRT_12() const { return ____overlayRT_12; }
	inline RectTransform_t3704657025 ** get_address_of__overlayRT_12() { return &____overlayRT_12; }
	inline void set__overlayRT_12(RectTransform_t3704657025 * value)
	{
		____overlayRT_12 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_12), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_13() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollPanelRT_13)); }
	inline RectTransform_t3704657025 * get__scrollPanelRT_13() const { return ____scrollPanelRT_13; }
	inline RectTransform_t3704657025 ** get_address_of__scrollPanelRT_13() { return &____scrollPanelRT_13; }
	inline void set__scrollPanelRT_13(RectTransform_t3704657025 * value)
	{
		____scrollPanelRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_13), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_14() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollBarRT_14)); }
	inline RectTransform_t3704657025 * get__scrollBarRT_14() const { return ____scrollBarRT_14; }
	inline RectTransform_t3704657025 ** get_address_of__scrollBarRT_14() { return &____scrollBarRT_14; }
	inline void set__scrollBarRT_14(RectTransform_t3704657025 * value)
	{
		____scrollBarRT_14 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_14), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_15() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____slidingAreaRT_15)); }
	inline RectTransform_t3704657025 * get__slidingAreaRT_15() const { return ____slidingAreaRT_15; }
	inline RectTransform_t3704657025 ** get_address_of__slidingAreaRT_15() { return &____slidingAreaRT_15; }
	inline void set__slidingAreaRT_15(RectTransform_t3704657025 * value)
	{
		____slidingAreaRT_15 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_15), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_16() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____itemsPanelRT_16)); }
	inline RectTransform_t3704657025 * get__itemsPanelRT_16() const { return ____itemsPanelRT_16; }
	inline RectTransform_t3704657025 ** get_address_of__itemsPanelRT_16() { return &____itemsPanelRT_16; }
	inline void set__itemsPanelRT_16(RectTransform_t3704657025 * value)
	{
		____itemsPanelRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_16), value);
	}

	inline static int32_t get_offset_of__canvas_17() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____canvas_17)); }
	inline Canvas_t3310196443 * get__canvas_17() const { return ____canvas_17; }
	inline Canvas_t3310196443 ** get_address_of__canvas_17() { return &____canvas_17; }
	inline void set__canvas_17(Canvas_t3310196443 * value)
	{
		____canvas_17 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_17), value);
	}

	inline static int32_t get_offset_of__canvasRT_18() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____canvasRT_18)); }
	inline RectTransform_t3704657025 * get__canvasRT_18() const { return ____canvasRT_18; }
	inline RectTransform_t3704657025 ** get_address_of__canvasRT_18() { return &____canvasRT_18; }
	inline void set__canvasRT_18(RectTransform_t3704657025 * value)
	{
		____canvasRT_18 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_18), value);
	}

	inline static int32_t get_offset_of__scrollRect_19() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollRect_19)); }
	inline ScrollRect_t4137855814 * get__scrollRect_19() const { return ____scrollRect_19; }
	inline ScrollRect_t4137855814 ** get_address_of__scrollRect_19() { return &____scrollRect_19; }
	inline void set__scrollRect_19(ScrollRect_t4137855814 * value)
	{
		____scrollRect_19 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_19), value);
	}

	inline static int32_t get_offset_of__panelItems_20() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____panelItems_20)); }
	inline List_1_t3319525431 * get__panelItems_20() const { return ____panelItems_20; }
	inline List_1_t3319525431 ** get_address_of__panelItems_20() { return &____panelItems_20; }
	inline void set__panelItems_20(List_1_t3319525431 * value)
	{
		____panelItems_20 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_20), value);
	}

	inline static int32_t get_offset_of__prunedPanelItems_21() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____prunedPanelItems_21)); }
	inline List_1_t3319525431 * get__prunedPanelItems_21() const { return ____prunedPanelItems_21; }
	inline List_1_t3319525431 ** get_address_of__prunedPanelItems_21() { return &____prunedPanelItems_21; }
	inline void set__prunedPanelItems_21(List_1_t3319525431 * value)
	{
		____prunedPanelItems_21 = value;
		Il2CppCodeGenWriteBarrier((&____prunedPanelItems_21), value);
	}

	inline static int32_t get_offset_of_panelObjects_22() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___panelObjects_22)); }
	inline Dictionary_2_t898892918 * get_panelObjects_22() const { return ___panelObjects_22; }
	inline Dictionary_2_t898892918 ** get_address_of_panelObjects_22() { return &___panelObjects_22; }
	inline void set_panelObjects_22(Dictionary_2_t898892918 * value)
	{
		___panelObjects_22 = value;
		Il2CppCodeGenWriteBarrier((&___panelObjects_22), value);
	}

	inline static int32_t get_offset_of_itemTemplate_23() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___itemTemplate_23)); }
	inline GameObject_t1113636619 * get_itemTemplate_23() const { return ___itemTemplate_23; }
	inline GameObject_t1113636619 ** get_address_of_itemTemplate_23() { return &___itemTemplate_23; }
	inline void set_itemTemplate_23(GameObject_t1113636619 * value)
	{
		___itemTemplate_23 = value;
		Il2CppCodeGenWriteBarrier((&___itemTemplate_23), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___U3CTextU3Ek__BackingField_24)); }
	inline String_t* get_U3CTextU3Ek__BackingField_24() const { return ___U3CTextU3Ek__BackingField_24; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_24() { return &___U3CTextU3Ek__BackingField_24; }
	inline void set_U3CTextU3Ek__BackingField_24(String_t* value)
	{
		___U3CTextU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_25() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____scrollBarWidth_25)); }
	inline float get__scrollBarWidth_25() const { return ____scrollBarWidth_25; }
	inline float* get_address_of__scrollBarWidth_25() { return &____scrollBarWidth_25; }
	inline void set__scrollBarWidth_25(float value)
	{
		____scrollBarWidth_25 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_26() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____itemsToDisplay_26)); }
	inline int32_t get__itemsToDisplay_26() const { return ____itemsToDisplay_26; }
	inline int32_t* get_address_of__itemsToDisplay_26() { return &____itemsToDisplay_26; }
	inline void set__itemsToDisplay_26(int32_t value)
	{
		____itemsToDisplay_26 = value;
	}

	inline static int32_t get_offset_of_SelectFirstItemOnStart_27() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___SelectFirstItemOnStart_27)); }
	inline bool get_SelectFirstItemOnStart_27() const { return ___SelectFirstItemOnStart_27; }
	inline bool* get_address_of_SelectFirstItemOnStart_27() { return &___SelectFirstItemOnStart_27; }
	inline void set_SelectFirstItemOnStart_27(bool value)
	{
		___SelectFirstItemOnStart_27 = value;
	}

	inline static int32_t get_offset_of__ChangeInputTextColorBasedOnMatchingItems_28() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____ChangeInputTextColorBasedOnMatchingItems_28)); }
	inline bool get__ChangeInputTextColorBasedOnMatchingItems_28() const { return ____ChangeInputTextColorBasedOnMatchingItems_28; }
	inline bool* get_address_of__ChangeInputTextColorBasedOnMatchingItems_28() { return &____ChangeInputTextColorBasedOnMatchingItems_28; }
	inline void set__ChangeInputTextColorBasedOnMatchingItems_28(bool value)
	{
		____ChangeInputTextColorBasedOnMatchingItems_28 = value;
	}

	inline static int32_t get_offset_of_ValidSelectionTextColor_29() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___ValidSelectionTextColor_29)); }
	inline Color_t2555686324  get_ValidSelectionTextColor_29() const { return ___ValidSelectionTextColor_29; }
	inline Color_t2555686324 * get_address_of_ValidSelectionTextColor_29() { return &___ValidSelectionTextColor_29; }
	inline void set_ValidSelectionTextColor_29(Color_t2555686324  value)
	{
		___ValidSelectionTextColor_29 = value;
	}

	inline static int32_t get_offset_of_MatchingItemsRemainingTextColor_30() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___MatchingItemsRemainingTextColor_30)); }
	inline Color_t2555686324  get_MatchingItemsRemainingTextColor_30() const { return ___MatchingItemsRemainingTextColor_30; }
	inline Color_t2555686324 * get_address_of_MatchingItemsRemainingTextColor_30() { return &___MatchingItemsRemainingTextColor_30; }
	inline void set_MatchingItemsRemainingTextColor_30(Color_t2555686324  value)
	{
		___MatchingItemsRemainingTextColor_30 = value;
	}

	inline static int32_t get_offset_of_NoItemsRemainingTextColor_31() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___NoItemsRemainingTextColor_31)); }
	inline Color_t2555686324  get_NoItemsRemainingTextColor_31() const { return ___NoItemsRemainingTextColor_31; }
	inline Color_t2555686324 * get_address_of_NoItemsRemainingTextColor_31() { return &___NoItemsRemainingTextColor_31; }
	inline void set_NoItemsRemainingTextColor_31(Color_t2555686324  value)
	{
		___NoItemsRemainingTextColor_31 = value;
	}

	inline static int32_t get_offset_of_autocompleteSearchType_32() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___autocompleteSearchType_32)); }
	inline int32_t get_autocompleteSearchType_32() const { return ___autocompleteSearchType_32; }
	inline int32_t* get_address_of_autocompleteSearchType_32() { return &___autocompleteSearchType_32; }
	inline void set_autocompleteSearchType_32(int32_t value)
	{
		___autocompleteSearchType_32 = value;
	}

	inline static int32_t get_offset_of__selectionIsValid_33() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ____selectionIsValid_33)); }
	inline bool get__selectionIsValid_33() const { return ____selectionIsValid_33; }
	inline bool* get_address_of__selectionIsValid_33() { return &____selectionIsValid_33; }
	inline void set__selectionIsValid_33(bool value)
	{
		____selectionIsValid_33 = value;
	}

	inline static int32_t get_offset_of_OnSelectionTextChanged_34() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___OnSelectionTextChanged_34)); }
	inline SelectionTextChangedEvent_t4051177638 * get_OnSelectionTextChanged_34() const { return ___OnSelectionTextChanged_34; }
	inline SelectionTextChangedEvent_t4051177638 ** get_address_of_OnSelectionTextChanged_34() { return &___OnSelectionTextChanged_34; }
	inline void set_OnSelectionTextChanged_34(SelectionTextChangedEvent_t4051177638 * value)
	{
		___OnSelectionTextChanged_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionTextChanged_34), value);
	}

	inline static int32_t get_offset_of_OnSelectionValidityChanged_35() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___OnSelectionValidityChanged_35)); }
	inline SelectionValidityChangedEvent_t954817928 * get_OnSelectionValidityChanged_35() const { return ___OnSelectionValidityChanged_35; }
	inline SelectionValidityChangedEvent_t954817928 ** get_address_of_OnSelectionValidityChanged_35() { return &___OnSelectionValidityChanged_35; }
	inline void set_OnSelectionValidityChanged_35(SelectionValidityChangedEvent_t954817928 * value)
	{
		___OnSelectionValidityChanged_35 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionValidityChanged_35), value);
	}

	inline static int32_t get_offset_of_OnSelectionChanged_36() { return static_cast<int32_t>(offsetof(AutoCompleteComboBox_t2765567798, ___OnSelectionChanged_36)); }
	inline SelectionChangedEvent_t1822043360 * get_OnSelectionChanged_36() const { return ___OnSelectionChanged_36; }
	inline SelectionChangedEvent_t1822043360 ** get_address_of_OnSelectionChanged_36() { return &___OnSelectionChanged_36; }
	inline void set_OnSelectionChanged_36(SelectionChangedEvent_t1822043360 * value)
	{
		___OnSelectionChanged_36 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOCOMPLETECOMBOBOX_T2765567798_H
#ifndef COLORIMAGE_T1780977718_H
#define COLORIMAGE_T1780977718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorImage
struct  ColorImage_t1780977718  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorImage::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.ColorPicker.ColorImage::image
	Image_t2670269651 * ___image_5;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(ColorImage_t1780977718, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(ColorImage_t1780977718, ___image_5)); }
	inline Image_t2670269651 * get_image_5() const { return ___image_5; }
	inline Image_t2670269651 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Image_t2670269651 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORIMAGE_T1780977718_H
#ifndef COLORLABEL_T1657108856_H
#define COLORLABEL_T1657108856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorLabel
struct  ColorLabel_t1657108856  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorLabel::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorLabel::type
	int32_t ___type_5;
	// System.String UnityEngine.UI.Extensions.ColorPicker.ColorLabel::prefix
	String_t* ___prefix_6;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorLabel::minValue
	float ___minValue_7;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorLabel::maxValue
	float ___maxValue_8;
	// System.Int32 UnityEngine.UI.Extensions.ColorPicker.ColorLabel::precision
	int32_t ___precision_9;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.ColorPicker.ColorLabel::label
	Text_t1901882714 * ___label_10;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_prefix_6() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___prefix_6)); }
	inline String_t* get_prefix_6() const { return ___prefix_6; }
	inline String_t** get_address_of_prefix_6() { return &___prefix_6; }
	inline void set_prefix_6(String_t* value)
	{
		___prefix_6 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_6), value);
	}

	inline static int32_t get_offset_of_minValue_7() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___minValue_7)); }
	inline float get_minValue_7() const { return ___minValue_7; }
	inline float* get_address_of_minValue_7() { return &___minValue_7; }
	inline void set_minValue_7(float value)
	{
		___minValue_7 = value;
	}

	inline static int32_t get_offset_of_maxValue_8() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___maxValue_8)); }
	inline float get_maxValue_8() const { return ___maxValue_8; }
	inline float* get_address_of_maxValue_8() { return &___maxValue_8; }
	inline void set_maxValue_8(float value)
	{
		___maxValue_8 = value;
	}

	inline static int32_t get_offset_of_precision_9() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___precision_9)); }
	inline int32_t get_precision_9() const { return ___precision_9; }
	inline int32_t* get_address_of_precision_9() { return &___precision_9; }
	inline void set_precision_9(int32_t value)
	{
		___precision_9 = value;
	}

	inline static int32_t get_offset_of_label_10() { return static_cast<int32_t>(offsetof(ColorLabel_t1657108856, ___label_10)); }
	inline Text_t1901882714 * get_label_10() const { return ___label_10; }
	inline Text_t1901882714 ** get_address_of_label_10() { return &___label_10; }
	inline void set_label_10(Text_t1901882714 * value)
	{
		___label_10 = value;
		Il2CppCodeGenWriteBarrier((&___label_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORLABEL_T1657108856_H
#ifndef COLORPICKERCONTROL_T2793111723_H
#define COLORPICKERCONTROL_T2793111723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl
struct  ColorPickerControl_t2793111723  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_hue
	float ____hue_4;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_saturation
	float ____saturation_5;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_brightness
	float ____brightness_6;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_red
	float ____red_7;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_green
	float ____green_8;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_blue
	float ____blue_9;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::_alpha
	float ____alpha_10;
	// ColorChangedEvent UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::onValueChanged
	ColorChangedEvent_t3019780707 * ___onValueChanged_11;
	// HSVChangedEvent UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl::onHSVChanged
	HSVChangedEvent_t911780251 * ___onHSVChanged_12;

public:
	inline static int32_t get_offset_of__hue_4() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____hue_4)); }
	inline float get__hue_4() const { return ____hue_4; }
	inline float* get_address_of__hue_4() { return &____hue_4; }
	inline void set__hue_4(float value)
	{
		____hue_4 = value;
	}

	inline static int32_t get_offset_of__saturation_5() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____saturation_5)); }
	inline float get__saturation_5() const { return ____saturation_5; }
	inline float* get_address_of__saturation_5() { return &____saturation_5; }
	inline void set__saturation_5(float value)
	{
		____saturation_5 = value;
	}

	inline static int32_t get_offset_of__brightness_6() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____brightness_6)); }
	inline float get__brightness_6() const { return ____brightness_6; }
	inline float* get_address_of__brightness_6() { return &____brightness_6; }
	inline void set__brightness_6(float value)
	{
		____brightness_6 = value;
	}

	inline static int32_t get_offset_of__red_7() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____red_7)); }
	inline float get__red_7() const { return ____red_7; }
	inline float* get_address_of__red_7() { return &____red_7; }
	inline void set__red_7(float value)
	{
		____red_7 = value;
	}

	inline static int32_t get_offset_of__green_8() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____green_8)); }
	inline float get__green_8() const { return ____green_8; }
	inline float* get_address_of__green_8() { return &____green_8; }
	inline void set__green_8(float value)
	{
		____green_8 = value;
	}

	inline static int32_t get_offset_of__blue_9() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____blue_9)); }
	inline float get__blue_9() const { return ____blue_9; }
	inline float* get_address_of__blue_9() { return &____blue_9; }
	inline void set__blue_9(float value)
	{
		____blue_9 = value;
	}

	inline static int32_t get_offset_of__alpha_10() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ____alpha_10)); }
	inline float get__alpha_10() const { return ____alpha_10; }
	inline float* get_address_of__alpha_10() { return &____alpha_10; }
	inline void set__alpha_10(float value)
	{
		____alpha_10 = value;
	}

	inline static int32_t get_offset_of_onValueChanged_11() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ___onValueChanged_11)); }
	inline ColorChangedEvent_t3019780707 * get_onValueChanged_11() const { return ___onValueChanged_11; }
	inline ColorChangedEvent_t3019780707 ** get_address_of_onValueChanged_11() { return &___onValueChanged_11; }
	inline void set_onValueChanged_11(ColorChangedEvent_t3019780707 * value)
	{
		___onValueChanged_11 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_11), value);
	}

	inline static int32_t get_offset_of_onHSVChanged_12() { return static_cast<int32_t>(offsetof(ColorPickerControl_t2793111723, ___onHSVChanged_12)); }
	inline HSVChangedEvent_t911780251 * get_onHSVChanged_12() const { return ___onHSVChanged_12; }
	inline HSVChangedEvent_t911780251 ** get_address_of_onHSVChanged_12() { return &___onHSVChanged_12; }
	inline void set_onHSVChanged_12(HSVChangedEvent_t911780251 * value)
	{
		___onHSVChanged_12 = value;
		Il2CppCodeGenWriteBarrier((&___onHSVChanged_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERCONTROL_T2793111723_H
#ifndef COLORPICKERPRESETS_T2164031678_H
#define COLORPICKERPRESETS_T2164031678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets
struct  ColorPickerPresets_t2164031678  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::presets
	GameObjectU5BU5D_t3328599146* ___presets_5;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.ColorPicker.ColorPickerPresets::createPresetImage
	Image_t2670269651 * ___createPresetImage_6;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t2164031678, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_presets_5() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t2164031678, ___presets_5)); }
	inline GameObjectU5BU5D_t3328599146* get_presets_5() const { return ___presets_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_presets_5() { return &___presets_5; }
	inline void set_presets_5(GameObjectU5BU5D_t3328599146* value)
	{
		___presets_5 = value;
		Il2CppCodeGenWriteBarrier((&___presets_5), value);
	}

	inline static int32_t get_offset_of_createPresetImage_6() { return static_cast<int32_t>(offsetof(ColorPickerPresets_t2164031678, ___createPresetImage_6)); }
	inline Image_t2670269651 * get_createPresetImage_6() const { return ___createPresetImage_6; }
	inline Image_t2670269651 ** get_address_of_createPresetImage_6() { return &___createPresetImage_6; }
	inline void set_createPresetImage_6(Image_t2670269651 * value)
	{
		___createPresetImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___createPresetImage_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERPRESETS_T2164031678_H
#ifndef COLORPICKERTESTER_T1752022104_H
#define COLORPICKERTESTER_T1752022104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester
struct  ColorPickerTester_t1752022104  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester::pickerRenderer
	Renderer_t2627027031 * ___pickerRenderer_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorPickerTester::picker
	ColorPickerControl_t2793111723 * ___picker_5;

public:
	inline static int32_t get_offset_of_pickerRenderer_4() { return static_cast<int32_t>(offsetof(ColorPickerTester_t1752022104, ___pickerRenderer_4)); }
	inline Renderer_t2627027031 * get_pickerRenderer_4() const { return ___pickerRenderer_4; }
	inline Renderer_t2627027031 ** get_address_of_pickerRenderer_4() { return &___pickerRenderer_4; }
	inline void set_pickerRenderer_4(Renderer_t2627027031 * value)
	{
		___pickerRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___pickerRenderer_4), value);
	}

	inline static int32_t get_offset_of_picker_5() { return static_cast<int32_t>(offsetof(ColorPickerTester_t1752022104, ___picker_5)); }
	inline ColorPickerControl_t2793111723 * get_picker_5() const { return ___picker_5; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_5() { return &___picker_5; }
	inline void set_picker_5(ColorPickerControl_t2793111723 * value)
	{
		___picker_5 = value;
		Il2CppCodeGenWriteBarrier((&___picker_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPICKERTESTER_T1752022104_H
#ifndef COLORSLIDER_T188049029_H
#define COLORSLIDER_T188049029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorSlider
struct  ColorSlider_t188049029  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorSlider::ColorPicker
	ColorPickerControl_t2793111723 * ___ColorPicker_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorSlider::type
	int32_t ___type_5;
	// UnityEngine.UI.Slider UnityEngine.UI.Extensions.ColorPicker.ColorSlider::slider
	Slider_t3903728902 * ___slider_6;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.ColorSlider::listen
	bool ___listen_7;

public:
	inline static int32_t get_offset_of_ColorPicker_4() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___ColorPicker_4)); }
	inline ColorPickerControl_t2793111723 * get_ColorPicker_4() const { return ___ColorPicker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_ColorPicker_4() { return &___ColorPicker_4; }
	inline void set_ColorPicker_4(ColorPickerControl_t2793111723 * value)
	{
		___ColorPicker_4 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPicker_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_slider_6() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___slider_6)); }
	inline Slider_t3903728902 * get_slider_6() const { return ___slider_6; }
	inline Slider_t3903728902 ** get_address_of_slider_6() { return &___slider_6; }
	inline void set_slider_6(Slider_t3903728902 * value)
	{
		___slider_6 = value;
		Il2CppCodeGenWriteBarrier((&___slider_6), value);
	}

	inline static int32_t get_offset_of_listen_7() { return static_cast<int32_t>(offsetof(ColorSlider_t188049029, ___listen_7)); }
	inline bool get_listen_7() const { return ___listen_7; }
	inline bool* get_address_of_listen_7() { return &___listen_7; }
	inline void set_listen_7(bool value)
	{
		___listen_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDER_T188049029_H
#ifndef COLORSLIDERIMAGE_T1534316151_H
#define COLORSLIDERIMAGE_T1534316151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage
struct  ColorSliderImage_t1534316151  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.UI.Extensions.ColorPicker.ColorValues UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::type
	int32_t ___type_5;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::direction
	int32_t ___direction_6;
	// UnityEngine.UI.RawImage UnityEngine.UI.Extensions.ColorPicker.ColorSliderImage::image
	RawImage_t3182918964 * ___image_7;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___type_5)); }
	inline int32_t get_type_5() const { return ___type_5; }
	inline int32_t* get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(int32_t value)
	{
		___type_5 = value;
	}

	inline static int32_t get_offset_of_direction_6() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___direction_6)); }
	inline int32_t get_direction_6() const { return ___direction_6; }
	inline int32_t* get_address_of_direction_6() { return &___direction_6; }
	inline void set_direction_6(int32_t value)
	{
		___direction_6 = value;
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(ColorSliderImage_t1534316151, ___image_7)); }
	inline RawImage_t3182918964 * get_image_7() const { return ___image_7; }
	inline RawImage_t3182918964 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(RawImage_t3182918964 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSLIDERIMAGE_T1534316151_H
#ifndef HEXCOLORFIELD_T106756477_H
#define HEXCOLORFIELD_T106756477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.HexColorField
struct  HexColorField_t106756477  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.HexColorField::ColorPicker
	ColorPickerControl_t2793111723 * ___ColorPicker_4;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.HexColorField::displayAlpha
	bool ___displayAlpha_5;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.ColorPicker.HexColorField::hexInputField
	InputField_t3762917431 * ___hexInputField_6;

public:
	inline static int32_t get_offset_of_ColorPicker_4() { return static_cast<int32_t>(offsetof(HexColorField_t106756477, ___ColorPicker_4)); }
	inline ColorPickerControl_t2793111723 * get_ColorPicker_4() const { return ___ColorPicker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_ColorPicker_4() { return &___ColorPicker_4; }
	inline void set_ColorPicker_4(ColorPickerControl_t2793111723 * value)
	{
		___ColorPicker_4 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPicker_4), value);
	}

	inline static int32_t get_offset_of_displayAlpha_5() { return static_cast<int32_t>(offsetof(HexColorField_t106756477, ___displayAlpha_5)); }
	inline bool get_displayAlpha_5() const { return ___displayAlpha_5; }
	inline bool* get_address_of_displayAlpha_5() { return &___displayAlpha_5; }
	inline void set_displayAlpha_5(bool value)
	{
		___displayAlpha_5 = value;
	}

	inline static int32_t get_offset_of_hexInputField_6() { return static_cast<int32_t>(offsetof(HexColorField_t106756477, ___hexInputField_6)); }
	inline InputField_t3762917431 * get_hexInputField_6() const { return ___hexInputField_6; }
	inline InputField_t3762917431 ** get_address_of_hexInputField_6() { return &___hexInputField_6; }
	inline void set_hexInputField_6(InputField_t3762917431 * value)
	{
		___hexInputField_6 = value;
		Il2CppCodeGenWriteBarrier((&___hexInputField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXCOLORFIELD_T106756477_H
#ifndef SVBOXSLIDER_T1594361808_H
#define SVBOXSLIDER_T1594361808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider
struct  SVBoxSlider_t1594361808  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.ColorPicker.ColorPickerControl UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::picker
	ColorPickerControl_t2793111723 * ___picker_4;
	// UnityEngine.UI.Extensions.BoxSlider UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::slider
	BoxSlider_t3694973841 * ___slider_5;
	// UnityEngine.UI.RawImage UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::image
	RawImage_t3182918964 * ___image_6;
	// System.Single UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::lastH
	float ___lastH_7;
	// System.Boolean UnityEngine.UI.Extensions.ColorPicker.SVBoxSlider::listen
	bool ___listen_8;

public:
	inline static int32_t get_offset_of_picker_4() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___picker_4)); }
	inline ColorPickerControl_t2793111723 * get_picker_4() const { return ___picker_4; }
	inline ColorPickerControl_t2793111723 ** get_address_of_picker_4() { return &___picker_4; }
	inline void set_picker_4(ColorPickerControl_t2793111723 * value)
	{
		___picker_4 = value;
		Il2CppCodeGenWriteBarrier((&___picker_4), value);
	}

	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___slider_5)); }
	inline BoxSlider_t3694973841 * get_slider_5() const { return ___slider_5; }
	inline BoxSlider_t3694973841 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(BoxSlider_t3694973841 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___slider_5), value);
	}

	inline static int32_t get_offset_of_image_6() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___image_6)); }
	inline RawImage_t3182918964 * get_image_6() const { return ___image_6; }
	inline RawImage_t3182918964 ** get_address_of_image_6() { return &___image_6; }
	inline void set_image_6(RawImage_t3182918964 * value)
	{
		___image_6 = value;
		Il2CppCodeGenWriteBarrier((&___image_6), value);
	}

	inline static int32_t get_offset_of_lastH_7() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___lastH_7)); }
	inline float get_lastH_7() const { return ___lastH_7; }
	inline float* get_address_of_lastH_7() { return &___lastH_7; }
	inline void set_lastH_7(float value)
	{
		___lastH_7 = value;
	}

	inline static int32_t get_offset_of_listen_8() { return static_cast<int32_t>(offsetof(SVBoxSlider_t1594361808, ___listen_8)); }
	inline bool get_listen_8() const { return ___listen_8; }
	inline bool* get_address_of_listen_8() { return &___listen_8; }
	inline void set_listen_8(bool value)
	{
		___listen_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVBOXSLIDER_T1594361808_H
#ifndef COMBOBOX_T4216213764_H
#define COMBOBOX_T4216213764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.ComboBox
struct  ComboBox_t4216213764  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color UnityEngine.UI.Extensions.ComboBox::disabledTextColor
	Color_t2555686324  ___disabledTextColor_4;
	// UnityEngine.UI.Extensions.DropDownListItem UnityEngine.UI.Extensions.ComboBox::<SelectedItem>k__BackingField
	DropDownListItem_t337244270 * ___U3CSelectedItemU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.ComboBox::AvailableOptions
	List_1_t3319525431 * ___AvailableOptions_6;
	// System.Single UnityEngine.UI.Extensions.ComboBox::_scrollBarWidth
	float ____scrollBarWidth_7;
	// System.Int32 UnityEngine.UI.Extensions.ComboBox::_itemsToDisplay
	int32_t ____itemsToDisplay_8;
	// UnityEngine.UI.Extensions.ComboBox/SelectionChangedEvent UnityEngine.UI.Extensions.ComboBox::OnSelectionChanged
	SelectionChangedEvent_t2252533886 * ___OnSelectionChanged_9;
	// System.Boolean UnityEngine.UI.Extensions.ComboBox::_isPanelActive
	bool ____isPanelActive_10;
	// System.Boolean UnityEngine.UI.Extensions.ComboBox::_hasDrawnOnce
	bool ____hasDrawnOnce_11;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.ComboBox::_mainInput
	InputField_t3762917431 * ____mainInput_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_inputRT
	RectTransform_t3704657025 * ____inputRT_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_overlayRT
	RectTransform_t3704657025 * ____overlayRT_15;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_scrollPanelRT
	RectTransform_t3704657025 * ____scrollPanelRT_16;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_scrollBarRT
	RectTransform_t3704657025 * ____scrollBarRT_17;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_slidingAreaRT
	RectTransform_t3704657025 * ____slidingAreaRT_18;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_itemsPanelRT
	RectTransform_t3704657025 * ____itemsPanelRT_19;
	// UnityEngine.Canvas UnityEngine.UI.Extensions.ComboBox::_canvas
	Canvas_t3310196443 * ____canvas_20;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.ComboBox::_canvasRT
	RectTransform_t3704657025 * ____canvasRT_21;
	// UnityEngine.UI.ScrollRect UnityEngine.UI.Extensions.ComboBox::_scrollRect
	ScrollRect_t4137855814 * ____scrollRect_22;
	// System.Collections.Generic.List`1<System.String> UnityEngine.UI.Extensions.ComboBox::_panelItems
	List_1_t3319525431 * ____panelItems_23;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> UnityEngine.UI.Extensions.ComboBox::panelObjects
	Dictionary_2_t898892918 * ___panelObjects_24;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.ComboBox::itemTemplate
	GameObject_t1113636619 * ___itemTemplate_25;
	// System.String UnityEngine.UI.Extensions.ComboBox::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of_disabledTextColor_4() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___disabledTextColor_4)); }
	inline Color_t2555686324  get_disabledTextColor_4() const { return ___disabledTextColor_4; }
	inline Color_t2555686324 * get_address_of_disabledTextColor_4() { return &___disabledTextColor_4; }
	inline void set_disabledTextColor_4(Color_t2555686324  value)
	{
		___disabledTextColor_4 = value;
	}

	inline static int32_t get_offset_of_U3CSelectedItemU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___U3CSelectedItemU3Ek__BackingField_5)); }
	inline DropDownListItem_t337244270 * get_U3CSelectedItemU3Ek__BackingField_5() const { return ___U3CSelectedItemU3Ek__BackingField_5; }
	inline DropDownListItem_t337244270 ** get_address_of_U3CSelectedItemU3Ek__BackingField_5() { return &___U3CSelectedItemU3Ek__BackingField_5; }
	inline void set_U3CSelectedItemU3Ek__BackingField_5(DropDownListItem_t337244270 * value)
	{
		___U3CSelectedItemU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSelectedItemU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_AvailableOptions_6() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___AvailableOptions_6)); }
	inline List_1_t3319525431 * get_AvailableOptions_6() const { return ___AvailableOptions_6; }
	inline List_1_t3319525431 ** get_address_of_AvailableOptions_6() { return &___AvailableOptions_6; }
	inline void set_AvailableOptions_6(List_1_t3319525431 * value)
	{
		___AvailableOptions_6 = value;
		Il2CppCodeGenWriteBarrier((&___AvailableOptions_6), value);
	}

	inline static int32_t get_offset_of__scrollBarWidth_7() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollBarWidth_7)); }
	inline float get__scrollBarWidth_7() const { return ____scrollBarWidth_7; }
	inline float* get_address_of__scrollBarWidth_7() { return &____scrollBarWidth_7; }
	inline void set__scrollBarWidth_7(float value)
	{
		____scrollBarWidth_7 = value;
	}

	inline static int32_t get_offset_of__itemsToDisplay_8() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____itemsToDisplay_8)); }
	inline int32_t get__itemsToDisplay_8() const { return ____itemsToDisplay_8; }
	inline int32_t* get_address_of__itemsToDisplay_8() { return &____itemsToDisplay_8; }
	inline void set__itemsToDisplay_8(int32_t value)
	{
		____itemsToDisplay_8 = value;
	}

	inline static int32_t get_offset_of_OnSelectionChanged_9() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___OnSelectionChanged_9)); }
	inline SelectionChangedEvent_t2252533886 * get_OnSelectionChanged_9() const { return ___OnSelectionChanged_9; }
	inline SelectionChangedEvent_t2252533886 ** get_address_of_OnSelectionChanged_9() { return &___OnSelectionChanged_9; }
	inline void set_OnSelectionChanged_9(SelectionChangedEvent_t2252533886 * value)
	{
		___OnSelectionChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectionChanged_9), value);
	}

	inline static int32_t get_offset_of__isPanelActive_10() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____isPanelActive_10)); }
	inline bool get__isPanelActive_10() const { return ____isPanelActive_10; }
	inline bool* get_address_of__isPanelActive_10() { return &____isPanelActive_10; }
	inline void set__isPanelActive_10(bool value)
	{
		____isPanelActive_10 = value;
	}

	inline static int32_t get_offset_of__hasDrawnOnce_11() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____hasDrawnOnce_11)); }
	inline bool get__hasDrawnOnce_11() const { return ____hasDrawnOnce_11; }
	inline bool* get_address_of__hasDrawnOnce_11() { return &____hasDrawnOnce_11; }
	inline void set__hasDrawnOnce_11(bool value)
	{
		____hasDrawnOnce_11 = value;
	}

	inline static int32_t get_offset_of__mainInput_12() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____mainInput_12)); }
	inline InputField_t3762917431 * get__mainInput_12() const { return ____mainInput_12; }
	inline InputField_t3762917431 ** get_address_of__mainInput_12() { return &____mainInput_12; }
	inline void set__mainInput_12(InputField_t3762917431 * value)
	{
		____mainInput_12 = value;
		Il2CppCodeGenWriteBarrier((&____mainInput_12), value);
	}

	inline static int32_t get_offset_of__inputRT_13() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____inputRT_13)); }
	inline RectTransform_t3704657025 * get__inputRT_13() const { return ____inputRT_13; }
	inline RectTransform_t3704657025 ** get_address_of__inputRT_13() { return &____inputRT_13; }
	inline void set__inputRT_13(RectTransform_t3704657025 * value)
	{
		____inputRT_13 = value;
		Il2CppCodeGenWriteBarrier((&____inputRT_13), value);
	}

	inline static int32_t get_offset_of__rectTransform_14() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____rectTransform_14)); }
	inline RectTransform_t3704657025 * get__rectTransform_14() const { return ____rectTransform_14; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_14() { return &____rectTransform_14; }
	inline void set__rectTransform_14(RectTransform_t3704657025 * value)
	{
		____rectTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_14), value);
	}

	inline static int32_t get_offset_of__overlayRT_15() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____overlayRT_15)); }
	inline RectTransform_t3704657025 * get__overlayRT_15() const { return ____overlayRT_15; }
	inline RectTransform_t3704657025 ** get_address_of__overlayRT_15() { return &____overlayRT_15; }
	inline void set__overlayRT_15(RectTransform_t3704657025 * value)
	{
		____overlayRT_15 = value;
		Il2CppCodeGenWriteBarrier((&____overlayRT_15), value);
	}

	inline static int32_t get_offset_of__scrollPanelRT_16() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollPanelRT_16)); }
	inline RectTransform_t3704657025 * get__scrollPanelRT_16() const { return ____scrollPanelRT_16; }
	inline RectTransform_t3704657025 ** get_address_of__scrollPanelRT_16() { return &____scrollPanelRT_16; }
	inline void set__scrollPanelRT_16(RectTransform_t3704657025 * value)
	{
		____scrollPanelRT_16 = value;
		Il2CppCodeGenWriteBarrier((&____scrollPanelRT_16), value);
	}

	inline static int32_t get_offset_of__scrollBarRT_17() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollBarRT_17)); }
	inline RectTransform_t3704657025 * get__scrollBarRT_17() const { return ____scrollBarRT_17; }
	inline RectTransform_t3704657025 ** get_address_of__scrollBarRT_17() { return &____scrollBarRT_17; }
	inline void set__scrollBarRT_17(RectTransform_t3704657025 * value)
	{
		____scrollBarRT_17 = value;
		Il2CppCodeGenWriteBarrier((&____scrollBarRT_17), value);
	}

	inline static int32_t get_offset_of__slidingAreaRT_18() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____slidingAreaRT_18)); }
	inline RectTransform_t3704657025 * get__slidingAreaRT_18() const { return ____slidingAreaRT_18; }
	inline RectTransform_t3704657025 ** get_address_of__slidingAreaRT_18() { return &____slidingAreaRT_18; }
	inline void set__slidingAreaRT_18(RectTransform_t3704657025 * value)
	{
		____slidingAreaRT_18 = value;
		Il2CppCodeGenWriteBarrier((&____slidingAreaRT_18), value);
	}

	inline static int32_t get_offset_of__itemsPanelRT_19() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____itemsPanelRT_19)); }
	inline RectTransform_t3704657025 * get__itemsPanelRT_19() const { return ____itemsPanelRT_19; }
	inline RectTransform_t3704657025 ** get_address_of__itemsPanelRT_19() { return &____itemsPanelRT_19; }
	inline void set__itemsPanelRT_19(RectTransform_t3704657025 * value)
	{
		____itemsPanelRT_19 = value;
		Il2CppCodeGenWriteBarrier((&____itemsPanelRT_19), value);
	}

	inline static int32_t get_offset_of__canvas_20() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____canvas_20)); }
	inline Canvas_t3310196443 * get__canvas_20() const { return ____canvas_20; }
	inline Canvas_t3310196443 ** get_address_of__canvas_20() { return &____canvas_20; }
	inline void set__canvas_20(Canvas_t3310196443 * value)
	{
		____canvas_20 = value;
		Il2CppCodeGenWriteBarrier((&____canvas_20), value);
	}

	inline static int32_t get_offset_of__canvasRT_21() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____canvasRT_21)); }
	inline RectTransform_t3704657025 * get__canvasRT_21() const { return ____canvasRT_21; }
	inline RectTransform_t3704657025 ** get_address_of__canvasRT_21() { return &____canvasRT_21; }
	inline void set__canvasRT_21(RectTransform_t3704657025 * value)
	{
		____canvasRT_21 = value;
		Il2CppCodeGenWriteBarrier((&____canvasRT_21), value);
	}

	inline static int32_t get_offset_of__scrollRect_22() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____scrollRect_22)); }
	inline ScrollRect_t4137855814 * get__scrollRect_22() const { return ____scrollRect_22; }
	inline ScrollRect_t4137855814 ** get_address_of__scrollRect_22() { return &____scrollRect_22; }
	inline void set__scrollRect_22(ScrollRect_t4137855814 * value)
	{
		____scrollRect_22 = value;
		Il2CppCodeGenWriteBarrier((&____scrollRect_22), value);
	}

	inline static int32_t get_offset_of__panelItems_23() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ____panelItems_23)); }
	inline List_1_t3319525431 * get__panelItems_23() const { return ____panelItems_23; }
	inline List_1_t3319525431 ** get_address_of__panelItems_23() { return &____panelItems_23; }
	inline void set__panelItems_23(List_1_t3319525431 * value)
	{
		____panelItems_23 = value;
		Il2CppCodeGenWriteBarrier((&____panelItems_23), value);
	}

	inline static int32_t get_offset_of_panelObjects_24() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___panelObjects_24)); }
	inline Dictionary_2_t898892918 * get_panelObjects_24() const { return ___panelObjects_24; }
	inline Dictionary_2_t898892918 ** get_address_of_panelObjects_24() { return &___panelObjects_24; }
	inline void set_panelObjects_24(Dictionary_2_t898892918 * value)
	{
		___panelObjects_24 = value;
		Il2CppCodeGenWriteBarrier((&___panelObjects_24), value);
	}

	inline static int32_t get_offset_of_itemTemplate_25() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___itemTemplate_25)); }
	inline GameObject_t1113636619 * get_itemTemplate_25() const { return ___itemTemplate_25; }
	inline GameObject_t1113636619 ** get_address_of_itemTemplate_25() { return &___itemTemplate_25; }
	inline void set_itemTemplate_25(GameObject_t1113636619 * value)
	{
		___itemTemplate_25 = value;
		Il2CppCodeGenWriteBarrier((&___itemTemplate_25), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(ComboBox_t4216213764, ___U3CTextU3Ek__BackingField_26)); }
	inline String_t* get_U3CTextU3Ek__BackingField_26() const { return ___U3CTextU3Ek__BackingField_26; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_26() { return &___U3CTextU3Ek__BackingField_26; }
	inline void set_U3CTextU3Ek__BackingField_26(String_t* value)
	{
		___U3CTextU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBOBOX_T4216213764_H
#ifndef ANIMATEEFFECTS_T2267068298_H
#define ANIMATEEFFECTS_T2267068298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.AnimateEffects
struct  AnimateEffects_t2267068298  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.LetterSpacing UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacing
	LetterSpacing_t1421332419 * ___letterSpacing_4;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacingMax
	float ___letterSpacingMax_5;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacingMin
	float ___letterSpacingMin_6;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::letterSpacingModifier
	float ___letterSpacingModifier_7;
	// UnityEngine.UI.Extensions.CurvedText UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedText
	CurvedText_t1522163716 * ___curvedText_8;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedTextMax
	float ___curvedTextMax_9;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedTextMin
	float ___curvedTextMin_10;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::curvedTextModifier
	float ___curvedTextModifier_11;
	// UnityEngine.UI.Extensions.Gradient2 UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2
	Gradient2_t3786049496 * ___gradient2_12;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2Max
	float ___gradient2Max_13;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2Min
	float ___gradient2Min_14;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::gradient2Modifier
	float ___gradient2Modifier_15;
	// UnityEngine.UI.Extensions.CylinderText UnityEngine.UI.Extensions.Examples.AnimateEffects::cylinderText
	CylinderText_t364731485 * ___cylinderText_16;
	// UnityEngine.Transform UnityEngine.UI.Extensions.Examples.AnimateEffects::cylinderTextRT
	Transform_t3600365921 * ___cylinderTextRT_17;
	// UnityEngine.Vector3 UnityEngine.UI.Extensions.Examples.AnimateEffects::cylinderRotation
	Vector3_t3722313464  ___cylinderRotation_18;
	// UnityEngine.UI.Extensions.SoftMaskScript UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIM
	SoftMaskScript_t2195956899 * ___SAUIM_19;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIMMax
	float ___SAUIMMax_20;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIMMin
	float ___SAUIMMin_21;
	// System.Single UnityEngine.UI.Extensions.Examples.AnimateEffects::SAUIMModifier
	float ___SAUIMModifier_22;

public:
	inline static int32_t get_offset_of_letterSpacing_4() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___letterSpacing_4)); }
	inline LetterSpacing_t1421332419 * get_letterSpacing_4() const { return ___letterSpacing_4; }
	inline LetterSpacing_t1421332419 ** get_address_of_letterSpacing_4() { return &___letterSpacing_4; }
	inline void set_letterSpacing_4(LetterSpacing_t1421332419 * value)
	{
		___letterSpacing_4 = value;
		Il2CppCodeGenWriteBarrier((&___letterSpacing_4), value);
	}

	inline static int32_t get_offset_of_letterSpacingMax_5() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___letterSpacingMax_5)); }
	inline float get_letterSpacingMax_5() const { return ___letterSpacingMax_5; }
	inline float* get_address_of_letterSpacingMax_5() { return &___letterSpacingMax_5; }
	inline void set_letterSpacingMax_5(float value)
	{
		___letterSpacingMax_5 = value;
	}

	inline static int32_t get_offset_of_letterSpacingMin_6() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___letterSpacingMin_6)); }
	inline float get_letterSpacingMin_6() const { return ___letterSpacingMin_6; }
	inline float* get_address_of_letterSpacingMin_6() { return &___letterSpacingMin_6; }
	inline void set_letterSpacingMin_6(float value)
	{
		___letterSpacingMin_6 = value;
	}

	inline static int32_t get_offset_of_letterSpacingModifier_7() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___letterSpacingModifier_7)); }
	inline float get_letterSpacingModifier_7() const { return ___letterSpacingModifier_7; }
	inline float* get_address_of_letterSpacingModifier_7() { return &___letterSpacingModifier_7; }
	inline void set_letterSpacingModifier_7(float value)
	{
		___letterSpacingModifier_7 = value;
	}

	inline static int32_t get_offset_of_curvedText_8() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___curvedText_8)); }
	inline CurvedText_t1522163716 * get_curvedText_8() const { return ___curvedText_8; }
	inline CurvedText_t1522163716 ** get_address_of_curvedText_8() { return &___curvedText_8; }
	inline void set_curvedText_8(CurvedText_t1522163716 * value)
	{
		___curvedText_8 = value;
		Il2CppCodeGenWriteBarrier((&___curvedText_8), value);
	}

	inline static int32_t get_offset_of_curvedTextMax_9() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___curvedTextMax_9)); }
	inline float get_curvedTextMax_9() const { return ___curvedTextMax_9; }
	inline float* get_address_of_curvedTextMax_9() { return &___curvedTextMax_9; }
	inline void set_curvedTextMax_9(float value)
	{
		___curvedTextMax_9 = value;
	}

	inline static int32_t get_offset_of_curvedTextMin_10() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___curvedTextMin_10)); }
	inline float get_curvedTextMin_10() const { return ___curvedTextMin_10; }
	inline float* get_address_of_curvedTextMin_10() { return &___curvedTextMin_10; }
	inline void set_curvedTextMin_10(float value)
	{
		___curvedTextMin_10 = value;
	}

	inline static int32_t get_offset_of_curvedTextModifier_11() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___curvedTextModifier_11)); }
	inline float get_curvedTextModifier_11() const { return ___curvedTextModifier_11; }
	inline float* get_address_of_curvedTextModifier_11() { return &___curvedTextModifier_11; }
	inline void set_curvedTextModifier_11(float value)
	{
		___curvedTextModifier_11 = value;
	}

	inline static int32_t get_offset_of_gradient2_12() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___gradient2_12)); }
	inline Gradient2_t3786049496 * get_gradient2_12() const { return ___gradient2_12; }
	inline Gradient2_t3786049496 ** get_address_of_gradient2_12() { return &___gradient2_12; }
	inline void set_gradient2_12(Gradient2_t3786049496 * value)
	{
		___gradient2_12 = value;
		Il2CppCodeGenWriteBarrier((&___gradient2_12), value);
	}

	inline static int32_t get_offset_of_gradient2Max_13() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___gradient2Max_13)); }
	inline float get_gradient2Max_13() const { return ___gradient2Max_13; }
	inline float* get_address_of_gradient2Max_13() { return &___gradient2Max_13; }
	inline void set_gradient2Max_13(float value)
	{
		___gradient2Max_13 = value;
	}

	inline static int32_t get_offset_of_gradient2Min_14() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___gradient2Min_14)); }
	inline float get_gradient2Min_14() const { return ___gradient2Min_14; }
	inline float* get_address_of_gradient2Min_14() { return &___gradient2Min_14; }
	inline void set_gradient2Min_14(float value)
	{
		___gradient2Min_14 = value;
	}

	inline static int32_t get_offset_of_gradient2Modifier_15() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___gradient2Modifier_15)); }
	inline float get_gradient2Modifier_15() const { return ___gradient2Modifier_15; }
	inline float* get_address_of_gradient2Modifier_15() { return &___gradient2Modifier_15; }
	inline void set_gradient2Modifier_15(float value)
	{
		___gradient2Modifier_15 = value;
	}

	inline static int32_t get_offset_of_cylinderText_16() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___cylinderText_16)); }
	inline CylinderText_t364731485 * get_cylinderText_16() const { return ___cylinderText_16; }
	inline CylinderText_t364731485 ** get_address_of_cylinderText_16() { return &___cylinderText_16; }
	inline void set_cylinderText_16(CylinderText_t364731485 * value)
	{
		___cylinderText_16 = value;
		Il2CppCodeGenWriteBarrier((&___cylinderText_16), value);
	}

	inline static int32_t get_offset_of_cylinderTextRT_17() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___cylinderTextRT_17)); }
	inline Transform_t3600365921 * get_cylinderTextRT_17() const { return ___cylinderTextRT_17; }
	inline Transform_t3600365921 ** get_address_of_cylinderTextRT_17() { return &___cylinderTextRT_17; }
	inline void set_cylinderTextRT_17(Transform_t3600365921 * value)
	{
		___cylinderTextRT_17 = value;
		Il2CppCodeGenWriteBarrier((&___cylinderTextRT_17), value);
	}

	inline static int32_t get_offset_of_cylinderRotation_18() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___cylinderRotation_18)); }
	inline Vector3_t3722313464  get_cylinderRotation_18() const { return ___cylinderRotation_18; }
	inline Vector3_t3722313464 * get_address_of_cylinderRotation_18() { return &___cylinderRotation_18; }
	inline void set_cylinderRotation_18(Vector3_t3722313464  value)
	{
		___cylinderRotation_18 = value;
	}

	inline static int32_t get_offset_of_SAUIM_19() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___SAUIM_19)); }
	inline SoftMaskScript_t2195956899 * get_SAUIM_19() const { return ___SAUIM_19; }
	inline SoftMaskScript_t2195956899 ** get_address_of_SAUIM_19() { return &___SAUIM_19; }
	inline void set_SAUIM_19(SoftMaskScript_t2195956899 * value)
	{
		___SAUIM_19 = value;
		Il2CppCodeGenWriteBarrier((&___SAUIM_19), value);
	}

	inline static int32_t get_offset_of_SAUIMMax_20() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___SAUIMMax_20)); }
	inline float get_SAUIMMax_20() const { return ___SAUIMMax_20; }
	inline float* get_address_of_SAUIMMax_20() { return &___SAUIMMax_20; }
	inline void set_SAUIMMax_20(float value)
	{
		___SAUIMMax_20 = value;
	}

	inline static int32_t get_offset_of_SAUIMMin_21() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___SAUIMMin_21)); }
	inline float get_SAUIMMin_21() const { return ___SAUIMMin_21; }
	inline float* get_address_of_SAUIMMin_21() { return &___SAUIMMin_21; }
	inline void set_SAUIMMin_21(float value)
	{
		___SAUIMMin_21 = value;
	}

	inline static int32_t get_offset_of_SAUIMModifier_22() { return static_cast<int32_t>(offsetof(AnimateEffects_t2267068298, ___SAUIMModifier_22)); }
	inline float get_SAUIMModifier_22() const { return ___SAUIMModifier_22; }
	inline float* get_address_of_SAUIMModifier_22() { return &___SAUIMModifier_22; }
	inline void set_SAUIMModifier_22(float value)
	{
		___SAUIMModifier_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEEFFECTS_T2267068298_H
#ifndef COMBOBOXCHANGED_T1525211877_H
#define COMBOBOXCHANGED_T1525211877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.ComboBoxChanged
struct  ComboBoxChanged_t1525211877  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBOBOXCHANGED_T1525211877_H
#ifndef COOLDOWNEFFECT_IMAGE_T4108738633_H
#define COOLDOWNEFFECT_IMAGE_T4108738633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.CooldownEffect_Image
struct  CooldownEffect_Image_t4108738633  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.CooldownButton UnityEngine.UI.Extensions.Examples.CooldownEffect_Image::cooldown
	CooldownButton_t2372397950 * ___cooldown_4;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.CooldownEffect_Image::displayText
	Text_t1901882714 * ___displayText_5;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.Examples.CooldownEffect_Image::target
	Image_t2670269651 * ___target_6;
	// System.String UnityEngine.UI.Extensions.Examples.CooldownEffect_Image::originalText
	String_t* ___originalText_7;

public:
	inline static int32_t get_offset_of_cooldown_4() { return static_cast<int32_t>(offsetof(CooldownEffect_Image_t4108738633, ___cooldown_4)); }
	inline CooldownButton_t2372397950 * get_cooldown_4() const { return ___cooldown_4; }
	inline CooldownButton_t2372397950 ** get_address_of_cooldown_4() { return &___cooldown_4; }
	inline void set_cooldown_4(CooldownButton_t2372397950 * value)
	{
		___cooldown_4 = value;
		Il2CppCodeGenWriteBarrier((&___cooldown_4), value);
	}

	inline static int32_t get_offset_of_displayText_5() { return static_cast<int32_t>(offsetof(CooldownEffect_Image_t4108738633, ___displayText_5)); }
	inline Text_t1901882714 * get_displayText_5() const { return ___displayText_5; }
	inline Text_t1901882714 ** get_address_of_displayText_5() { return &___displayText_5; }
	inline void set_displayText_5(Text_t1901882714 * value)
	{
		___displayText_5 = value;
		Il2CppCodeGenWriteBarrier((&___displayText_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(CooldownEffect_Image_t4108738633, ___target_6)); }
	inline Image_t2670269651 * get_target_6() const { return ___target_6; }
	inline Image_t2670269651 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Image_t2670269651 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_originalText_7() { return static_cast<int32_t>(offsetof(CooldownEffect_Image_t4108738633, ___originalText_7)); }
	inline String_t* get_originalText_7() const { return ___originalText_7; }
	inline String_t** get_address_of_originalText_7() { return &___originalText_7; }
	inline void set_originalText_7(String_t* value)
	{
		___originalText_7 = value;
		Il2CppCodeGenWriteBarrier((&___originalText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOLDOWNEFFECT_IMAGE_T4108738633_H
#ifndef COOLDOWNEFFECT_SAUIM_T896650247_H
#define COOLDOWNEFFECT_SAUIM_T896650247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.CooldownEffect_SAUIM
struct  CooldownEffect_SAUIM_t896650247  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.CooldownButton UnityEngine.UI.Extensions.Examples.CooldownEffect_SAUIM::cooldown
	CooldownButton_t2372397950 * ___cooldown_4;
	// UnityEngine.UI.Extensions.SoftMaskScript UnityEngine.UI.Extensions.Examples.CooldownEffect_SAUIM::sauim
	SoftMaskScript_t2195956899 * ___sauim_5;

public:
	inline static int32_t get_offset_of_cooldown_4() { return static_cast<int32_t>(offsetof(CooldownEffect_SAUIM_t896650247, ___cooldown_4)); }
	inline CooldownButton_t2372397950 * get_cooldown_4() const { return ___cooldown_4; }
	inline CooldownButton_t2372397950 ** get_address_of_cooldown_4() { return &___cooldown_4; }
	inline void set_cooldown_4(CooldownButton_t2372397950 * value)
	{
		___cooldown_4 = value;
		Il2CppCodeGenWriteBarrier((&___cooldown_4), value);
	}

	inline static int32_t get_offset_of_sauim_5() { return static_cast<int32_t>(offsetof(CooldownEffect_SAUIM_t896650247, ___sauim_5)); }
	inline SoftMaskScript_t2195956899 * get_sauim_5() const { return ___sauim_5; }
	inline SoftMaskScript_t2195956899 ** get_address_of_sauim_5() { return &___sauim_5; }
	inline void set_sauim_5(SoftMaskScript_t2195956899 * value)
	{
		___sauim_5 = value;
		Il2CppCodeGenWriteBarrier((&___sauim_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOLDOWNEFFECT_SAUIM_T896650247_H
#ifndef EXAMPLE01SCENE_T4105520515_H
#define EXAMPLE01SCENE_T4105520515_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example01Scene
struct  Example01Scene_t4105520515  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.Examples.Example01ScrollView UnityEngine.UI.Extensions.Examples.Example01Scene::scrollView
	Example01ScrollView_t2860006669 * ___scrollView_4;

public:
	inline static int32_t get_offset_of_scrollView_4() { return static_cast<int32_t>(offsetof(Example01Scene_t4105520515, ___scrollView_4)); }
	inline Example01ScrollView_t2860006669 * get_scrollView_4() const { return ___scrollView_4; }
	inline Example01ScrollView_t2860006669 ** get_address_of_scrollView_4() { return &___scrollView_4; }
	inline void set_scrollView_4(Example01ScrollView_t2860006669 * value)
	{
		___scrollView_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollView_4), value);
	}
};

struct Example01Scene_t4105520515_StaticFields
{
public:
	// System.Func`2<System.Int32,UnityEngine.UI.Extensions.Examples.Example01CellDto> UnityEngine.UI.Extensions.Examples.Example01Scene::<>f__am$cache0
	Func_2_t603503607 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Example01Scene_t4105520515_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t603503607 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t603503607 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t603503607 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE01SCENE_T4105520515_H
#ifndef EXAMPLE02SCENE_T3589293443_H
#define EXAMPLE02SCENE_T3589293443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example02Scene
struct  Example02Scene_t3589293443  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.Examples.Example02ScrollView UnityEngine.UI.Extensions.Examples.Example02Scene::scrollView
	Example02ScrollView_t3654499597 * ___scrollView_4;

public:
	inline static int32_t get_offset_of_scrollView_4() { return static_cast<int32_t>(offsetof(Example02Scene_t3589293443, ___scrollView_4)); }
	inline Example02ScrollView_t3654499597 * get_scrollView_4() const { return ___scrollView_4; }
	inline Example02ScrollView_t3654499597 ** get_address_of_scrollView_4() { return &___scrollView_4; }
	inline void set_scrollView_4(Example02ScrollView_t3654499597 * value)
	{
		___scrollView_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollView_4), value);
	}
};

struct Example02Scene_t3589293443_StaticFields
{
public:
	// System.Func`2<System.Int32,UnityEngine.UI.Extensions.Examples.Example02CellDto> UnityEngine.UI.Extensions.Examples.Example02Scene::<>f__am$cache0
	Func_2_t747879415 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Example02Scene_t3589293443_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t747879415 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t747879415 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t747879415 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE02SCENE_T3589293443_H
#ifndef EXAMPLE03SCENE_T3837740419_H
#define EXAMPLE03SCENE_T3837740419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example03Scene
struct  Example03Scene_t3837740419  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.Examples.Example03ScrollView UnityEngine.UI.Extensions.Examples.Example03Scene::scrollView
	Example03ScrollView_t2613198093 * ___scrollView_4;

public:
	inline static int32_t get_offset_of_scrollView_4() { return static_cast<int32_t>(offsetof(Example03Scene_t3837740419, ___scrollView_4)); }
	inline Example03ScrollView_t2613198093 * get_scrollView_4() const { return ___scrollView_4; }
	inline Example03ScrollView_t2613198093 ** get_address_of_scrollView_4() { return &___scrollView_4; }
	inline void set_scrollView_4(Example03ScrollView_t2613198093 * value)
	{
		___scrollView_4 = value;
		Il2CppCodeGenWriteBarrier((&___scrollView_4), value);
	}
};

struct Example03Scene_t3837740419_StaticFields
{
public:
	// System.Func`2<System.Int32,UnityEngine.UI.Extensions.Examples.Example03CellDto> UnityEngine.UI.Extensions.Examples.Example03Scene::<>f__am$cache0
	Func_2_t4116102135 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Example03Scene_t3837740419_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t4116102135 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t4116102135 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t4116102135 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE03SCENE_T3837740419_H
#ifndef LINERENDERERORBIT_T3961648819_H
#define LINERENDERERORBIT_T3961648819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.LineRendererOrbit
struct  LineRendererOrbit_t3961648819  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.UILineRenderer UnityEngine.UI.Extensions.Examples.LineRendererOrbit::lr
	UILineRenderer_t3861579578 * ___lr_4;
	// UnityEngine.UI.Extensions.Circle UnityEngine.UI.Extensions.Examples.LineRendererOrbit::circle
	Circle_t4191205477 * ___circle_5;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.LineRendererOrbit::OrbitGO
	GameObject_t1113636619 * ___OrbitGO_6;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.LineRendererOrbit::orbitGOrt
	RectTransform_t3704657025 * ___orbitGOrt_7;
	// System.Single UnityEngine.UI.Extensions.Examples.LineRendererOrbit::orbitTime
	float ___orbitTime_8;
	// System.Single UnityEngine.UI.Extensions.Examples.LineRendererOrbit::_xAxis
	float ____xAxis_9;
	// System.Single UnityEngine.UI.Extensions.Examples.LineRendererOrbit::_yAxis
	float ____yAxis_10;
	// System.Int32 UnityEngine.UI.Extensions.Examples.LineRendererOrbit::_steps
	int32_t ____steps_11;

public:
	inline static int32_t get_offset_of_lr_4() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ___lr_4)); }
	inline UILineRenderer_t3861579578 * get_lr_4() const { return ___lr_4; }
	inline UILineRenderer_t3861579578 ** get_address_of_lr_4() { return &___lr_4; }
	inline void set_lr_4(UILineRenderer_t3861579578 * value)
	{
		___lr_4 = value;
		Il2CppCodeGenWriteBarrier((&___lr_4), value);
	}

	inline static int32_t get_offset_of_circle_5() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ___circle_5)); }
	inline Circle_t4191205477 * get_circle_5() const { return ___circle_5; }
	inline Circle_t4191205477 ** get_address_of_circle_5() { return &___circle_5; }
	inline void set_circle_5(Circle_t4191205477 * value)
	{
		___circle_5 = value;
		Il2CppCodeGenWriteBarrier((&___circle_5), value);
	}

	inline static int32_t get_offset_of_OrbitGO_6() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ___OrbitGO_6)); }
	inline GameObject_t1113636619 * get_OrbitGO_6() const { return ___OrbitGO_6; }
	inline GameObject_t1113636619 ** get_address_of_OrbitGO_6() { return &___OrbitGO_6; }
	inline void set_OrbitGO_6(GameObject_t1113636619 * value)
	{
		___OrbitGO_6 = value;
		Il2CppCodeGenWriteBarrier((&___OrbitGO_6), value);
	}

	inline static int32_t get_offset_of_orbitGOrt_7() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ___orbitGOrt_7)); }
	inline RectTransform_t3704657025 * get_orbitGOrt_7() const { return ___orbitGOrt_7; }
	inline RectTransform_t3704657025 ** get_address_of_orbitGOrt_7() { return &___orbitGOrt_7; }
	inline void set_orbitGOrt_7(RectTransform_t3704657025 * value)
	{
		___orbitGOrt_7 = value;
		Il2CppCodeGenWriteBarrier((&___orbitGOrt_7), value);
	}

	inline static int32_t get_offset_of_orbitTime_8() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ___orbitTime_8)); }
	inline float get_orbitTime_8() const { return ___orbitTime_8; }
	inline float* get_address_of_orbitTime_8() { return &___orbitTime_8; }
	inline void set_orbitTime_8(float value)
	{
		___orbitTime_8 = value;
	}

	inline static int32_t get_offset_of__xAxis_9() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ____xAxis_9)); }
	inline float get__xAxis_9() const { return ____xAxis_9; }
	inline float* get_address_of__xAxis_9() { return &____xAxis_9; }
	inline void set__xAxis_9(float value)
	{
		____xAxis_9 = value;
	}

	inline static int32_t get_offset_of__yAxis_10() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ____yAxis_10)); }
	inline float get__yAxis_10() const { return ____yAxis_10; }
	inline float* get_address_of__yAxis_10() { return &____yAxis_10; }
	inline void set__yAxis_10(float value)
	{
		____yAxis_10 = value;
	}

	inline static int32_t get_offset_of__steps_11() { return static_cast<int32_t>(offsetof(LineRendererOrbit_t3961648819, ____steps_11)); }
	inline int32_t get__steps_11() const { return ____steps_11; }
	inline int32_t* get_address_of__steps_11() { return &____steps_11; }
	inline void set__steps_11(int32_t value)
	{
		____steps_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINERENDERERORBIT_T3961648819_H
#ifndef PAGINATIONSCRIPT_T3257083080_H
#define PAGINATIONSCRIPT_T3257083080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.PaginationScript
struct  PaginationScript_t3257083080  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.HorizontalScrollSnap UnityEngine.UI.Extensions.Examples.PaginationScript::hss
	HorizontalScrollSnap_t1980761718 * ___hss_4;
	// System.Int32 UnityEngine.UI.Extensions.Examples.PaginationScript::Page
	int32_t ___Page_5;

public:
	inline static int32_t get_offset_of_hss_4() { return static_cast<int32_t>(offsetof(PaginationScript_t3257083080, ___hss_4)); }
	inline HorizontalScrollSnap_t1980761718 * get_hss_4() const { return ___hss_4; }
	inline HorizontalScrollSnap_t1980761718 ** get_address_of_hss_4() { return &___hss_4; }
	inline void set_hss_4(HorizontalScrollSnap_t1980761718 * value)
	{
		___hss_4 = value;
		Il2CppCodeGenWriteBarrier((&___hss_4), value);
	}

	inline static int32_t get_offset_of_Page_5() { return static_cast<int32_t>(offsetof(PaginationScript_t3257083080, ___Page_5)); }
	inline int32_t get_Page_5() const { return ___Page_5; }
	inline int32_t* get_address_of_Page_5() { return &___Page_5; }
	inline void set_Page_5(int32_t value)
	{
		___Page_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAGINATIONSCRIPT_T3257083080_H
#ifndef SCROLLINGCALENDAR_T1593431244_H
#define SCROLLINGCALENDAR_T1593431244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.ScrollingCalendar
struct  ScrollingCalendar_t1593431244  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsScrollingPanel
	RectTransform_t3704657025 * ___monthsScrollingPanel_4;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsScrollingPanel
	RectTransform_t3704657025 * ___yearsScrollingPanel_5;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysScrollingPanel
	RectTransform_t3704657025 * ___daysScrollingPanel_6;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsButtonPrefab
	GameObject_t1113636619 * ___yearsButtonPrefab_7;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsButtonPrefab
	GameObject_t1113636619 * ___monthsButtonPrefab_8;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysButtonPrefab
	GameObject_t1113636619 * ___daysButtonPrefab_9;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsButtons
	GameObjectU5BU5D_t3328599146* ___monthsButtons_10;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsButtons
	GameObjectU5BU5D_t3328599146* ___yearsButtons_11;
	// UnityEngine.GameObject[] UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysButtons
	GameObjectU5BU5D_t3328599146* ___daysButtons_12;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthCenter
	RectTransform_t3704657025 * ___monthCenter_13;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsCenter
	RectTransform_t3704657025 * ___yearsCenter_14;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysCenter
	RectTransform_t3704657025 * ___daysCenter_15;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsVerticalScroller
	UIVerticalScroller_t3292120078 * ___yearsVerticalScroller_16;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsVerticalScroller
	UIVerticalScroller_t3292120078 * ___monthsVerticalScroller_17;
	// UnityEngine.UI.Extensions.UIVerticalScroller UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysVerticalScroller
	UIVerticalScroller_t3292120078 * ___daysVerticalScroller_18;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.ScrollingCalendar::inputFieldDays
	InputField_t3762917431 * ___inputFieldDays_19;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.ScrollingCalendar::inputFieldMonths
	InputField_t3762917431 * ___inputFieldMonths_20;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.ScrollingCalendar::inputFieldYears
	InputField_t3762917431 * ___inputFieldYears_21;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.ScrollingCalendar::dateText
	Text_t1901882714 * ___dateText_22;
	// System.Int32 UnityEngine.UI.Extensions.Examples.ScrollingCalendar::daysSet
	int32_t ___daysSet_23;
	// System.Int32 UnityEngine.UI.Extensions.Examples.ScrollingCalendar::monthsSet
	int32_t ___monthsSet_24;
	// System.Int32 UnityEngine.UI.Extensions.Examples.ScrollingCalendar::yearsSet
	int32_t ___yearsSet_25;

public:
	inline static int32_t get_offset_of_monthsScrollingPanel_4() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___monthsScrollingPanel_4)); }
	inline RectTransform_t3704657025 * get_monthsScrollingPanel_4() const { return ___monthsScrollingPanel_4; }
	inline RectTransform_t3704657025 ** get_address_of_monthsScrollingPanel_4() { return &___monthsScrollingPanel_4; }
	inline void set_monthsScrollingPanel_4(RectTransform_t3704657025 * value)
	{
		___monthsScrollingPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___monthsScrollingPanel_4), value);
	}

	inline static int32_t get_offset_of_yearsScrollingPanel_5() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___yearsScrollingPanel_5)); }
	inline RectTransform_t3704657025 * get_yearsScrollingPanel_5() const { return ___yearsScrollingPanel_5; }
	inline RectTransform_t3704657025 ** get_address_of_yearsScrollingPanel_5() { return &___yearsScrollingPanel_5; }
	inline void set_yearsScrollingPanel_5(RectTransform_t3704657025 * value)
	{
		___yearsScrollingPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___yearsScrollingPanel_5), value);
	}

	inline static int32_t get_offset_of_daysScrollingPanel_6() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___daysScrollingPanel_6)); }
	inline RectTransform_t3704657025 * get_daysScrollingPanel_6() const { return ___daysScrollingPanel_6; }
	inline RectTransform_t3704657025 ** get_address_of_daysScrollingPanel_6() { return &___daysScrollingPanel_6; }
	inline void set_daysScrollingPanel_6(RectTransform_t3704657025 * value)
	{
		___daysScrollingPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___daysScrollingPanel_6), value);
	}

	inline static int32_t get_offset_of_yearsButtonPrefab_7() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___yearsButtonPrefab_7)); }
	inline GameObject_t1113636619 * get_yearsButtonPrefab_7() const { return ___yearsButtonPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_yearsButtonPrefab_7() { return &___yearsButtonPrefab_7; }
	inline void set_yearsButtonPrefab_7(GameObject_t1113636619 * value)
	{
		___yearsButtonPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___yearsButtonPrefab_7), value);
	}

	inline static int32_t get_offset_of_monthsButtonPrefab_8() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___monthsButtonPrefab_8)); }
	inline GameObject_t1113636619 * get_monthsButtonPrefab_8() const { return ___monthsButtonPrefab_8; }
	inline GameObject_t1113636619 ** get_address_of_monthsButtonPrefab_8() { return &___monthsButtonPrefab_8; }
	inline void set_monthsButtonPrefab_8(GameObject_t1113636619 * value)
	{
		___monthsButtonPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___monthsButtonPrefab_8), value);
	}

	inline static int32_t get_offset_of_daysButtonPrefab_9() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___daysButtonPrefab_9)); }
	inline GameObject_t1113636619 * get_daysButtonPrefab_9() const { return ___daysButtonPrefab_9; }
	inline GameObject_t1113636619 ** get_address_of_daysButtonPrefab_9() { return &___daysButtonPrefab_9; }
	inline void set_daysButtonPrefab_9(GameObject_t1113636619 * value)
	{
		___daysButtonPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___daysButtonPrefab_9), value);
	}

	inline static int32_t get_offset_of_monthsButtons_10() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___monthsButtons_10)); }
	inline GameObjectU5BU5D_t3328599146* get_monthsButtons_10() const { return ___monthsButtons_10; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_monthsButtons_10() { return &___monthsButtons_10; }
	inline void set_monthsButtons_10(GameObjectU5BU5D_t3328599146* value)
	{
		___monthsButtons_10 = value;
		Il2CppCodeGenWriteBarrier((&___monthsButtons_10), value);
	}

	inline static int32_t get_offset_of_yearsButtons_11() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___yearsButtons_11)); }
	inline GameObjectU5BU5D_t3328599146* get_yearsButtons_11() const { return ___yearsButtons_11; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_yearsButtons_11() { return &___yearsButtons_11; }
	inline void set_yearsButtons_11(GameObjectU5BU5D_t3328599146* value)
	{
		___yearsButtons_11 = value;
		Il2CppCodeGenWriteBarrier((&___yearsButtons_11), value);
	}

	inline static int32_t get_offset_of_daysButtons_12() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___daysButtons_12)); }
	inline GameObjectU5BU5D_t3328599146* get_daysButtons_12() const { return ___daysButtons_12; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_daysButtons_12() { return &___daysButtons_12; }
	inline void set_daysButtons_12(GameObjectU5BU5D_t3328599146* value)
	{
		___daysButtons_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysButtons_12), value);
	}

	inline static int32_t get_offset_of_monthCenter_13() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___monthCenter_13)); }
	inline RectTransform_t3704657025 * get_monthCenter_13() const { return ___monthCenter_13; }
	inline RectTransform_t3704657025 ** get_address_of_monthCenter_13() { return &___monthCenter_13; }
	inline void set_monthCenter_13(RectTransform_t3704657025 * value)
	{
		___monthCenter_13 = value;
		Il2CppCodeGenWriteBarrier((&___monthCenter_13), value);
	}

	inline static int32_t get_offset_of_yearsCenter_14() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___yearsCenter_14)); }
	inline RectTransform_t3704657025 * get_yearsCenter_14() const { return ___yearsCenter_14; }
	inline RectTransform_t3704657025 ** get_address_of_yearsCenter_14() { return &___yearsCenter_14; }
	inline void set_yearsCenter_14(RectTransform_t3704657025 * value)
	{
		___yearsCenter_14 = value;
		Il2CppCodeGenWriteBarrier((&___yearsCenter_14), value);
	}

	inline static int32_t get_offset_of_daysCenter_15() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___daysCenter_15)); }
	inline RectTransform_t3704657025 * get_daysCenter_15() const { return ___daysCenter_15; }
	inline RectTransform_t3704657025 ** get_address_of_daysCenter_15() { return &___daysCenter_15; }
	inline void set_daysCenter_15(RectTransform_t3704657025 * value)
	{
		___daysCenter_15 = value;
		Il2CppCodeGenWriteBarrier((&___daysCenter_15), value);
	}

	inline static int32_t get_offset_of_yearsVerticalScroller_16() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___yearsVerticalScroller_16)); }
	inline UIVerticalScroller_t3292120078 * get_yearsVerticalScroller_16() const { return ___yearsVerticalScroller_16; }
	inline UIVerticalScroller_t3292120078 ** get_address_of_yearsVerticalScroller_16() { return &___yearsVerticalScroller_16; }
	inline void set_yearsVerticalScroller_16(UIVerticalScroller_t3292120078 * value)
	{
		___yearsVerticalScroller_16 = value;
		Il2CppCodeGenWriteBarrier((&___yearsVerticalScroller_16), value);
	}

	inline static int32_t get_offset_of_monthsVerticalScroller_17() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___monthsVerticalScroller_17)); }
	inline UIVerticalScroller_t3292120078 * get_monthsVerticalScroller_17() const { return ___monthsVerticalScroller_17; }
	inline UIVerticalScroller_t3292120078 ** get_address_of_monthsVerticalScroller_17() { return &___monthsVerticalScroller_17; }
	inline void set_monthsVerticalScroller_17(UIVerticalScroller_t3292120078 * value)
	{
		___monthsVerticalScroller_17 = value;
		Il2CppCodeGenWriteBarrier((&___monthsVerticalScroller_17), value);
	}

	inline static int32_t get_offset_of_daysVerticalScroller_18() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___daysVerticalScroller_18)); }
	inline UIVerticalScroller_t3292120078 * get_daysVerticalScroller_18() const { return ___daysVerticalScroller_18; }
	inline UIVerticalScroller_t3292120078 ** get_address_of_daysVerticalScroller_18() { return &___daysVerticalScroller_18; }
	inline void set_daysVerticalScroller_18(UIVerticalScroller_t3292120078 * value)
	{
		___daysVerticalScroller_18 = value;
		Il2CppCodeGenWriteBarrier((&___daysVerticalScroller_18), value);
	}

	inline static int32_t get_offset_of_inputFieldDays_19() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___inputFieldDays_19)); }
	inline InputField_t3762917431 * get_inputFieldDays_19() const { return ___inputFieldDays_19; }
	inline InputField_t3762917431 ** get_address_of_inputFieldDays_19() { return &___inputFieldDays_19; }
	inline void set_inputFieldDays_19(InputField_t3762917431 * value)
	{
		___inputFieldDays_19 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldDays_19), value);
	}

	inline static int32_t get_offset_of_inputFieldMonths_20() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___inputFieldMonths_20)); }
	inline InputField_t3762917431 * get_inputFieldMonths_20() const { return ___inputFieldMonths_20; }
	inline InputField_t3762917431 ** get_address_of_inputFieldMonths_20() { return &___inputFieldMonths_20; }
	inline void set_inputFieldMonths_20(InputField_t3762917431 * value)
	{
		___inputFieldMonths_20 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldMonths_20), value);
	}

	inline static int32_t get_offset_of_inputFieldYears_21() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___inputFieldYears_21)); }
	inline InputField_t3762917431 * get_inputFieldYears_21() const { return ___inputFieldYears_21; }
	inline InputField_t3762917431 ** get_address_of_inputFieldYears_21() { return &___inputFieldYears_21; }
	inline void set_inputFieldYears_21(InputField_t3762917431 * value)
	{
		___inputFieldYears_21 = value;
		Il2CppCodeGenWriteBarrier((&___inputFieldYears_21), value);
	}

	inline static int32_t get_offset_of_dateText_22() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___dateText_22)); }
	inline Text_t1901882714 * get_dateText_22() const { return ___dateText_22; }
	inline Text_t1901882714 ** get_address_of_dateText_22() { return &___dateText_22; }
	inline void set_dateText_22(Text_t1901882714 * value)
	{
		___dateText_22 = value;
		Il2CppCodeGenWriteBarrier((&___dateText_22), value);
	}

	inline static int32_t get_offset_of_daysSet_23() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___daysSet_23)); }
	inline int32_t get_daysSet_23() const { return ___daysSet_23; }
	inline int32_t* get_address_of_daysSet_23() { return &___daysSet_23; }
	inline void set_daysSet_23(int32_t value)
	{
		___daysSet_23 = value;
	}

	inline static int32_t get_offset_of_monthsSet_24() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___monthsSet_24)); }
	inline int32_t get_monthsSet_24() const { return ___monthsSet_24; }
	inline int32_t* get_address_of_monthsSet_24() { return &___monthsSet_24; }
	inline void set_monthsSet_24(int32_t value)
	{
		___monthsSet_24 = value;
	}

	inline static int32_t get_offset_of_yearsSet_25() { return static_cast<int32_t>(offsetof(ScrollingCalendar_t1593431244, ___yearsSet_25)); }
	inline int32_t get_yearsSet_25() const { return ___yearsSet_25; }
	inline int32_t* get_address_of_yearsSet_25() { return &___yearsSet_25; }
	inline void set_yearsSet_25(int32_t value)
	{
		___yearsSet_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINGCALENDAR_T1593431244_H
#ifndef TESTADDINGPOINTS_T4285918360_H
#define TESTADDINGPOINTS_T4285918360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.TestAddingPoints
struct  TestAddingPoints_t4285918360  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.UILineRenderer UnityEngine.UI.Extensions.Examples.TestAddingPoints::LineRenderer
	UILineRenderer_t3861579578 * ___LineRenderer_4;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.TestAddingPoints::XValue
	Text_t1901882714 * ___XValue_5;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.TestAddingPoints::YValue
	Text_t1901882714 * ___YValue_6;

public:
	inline static int32_t get_offset_of_LineRenderer_4() { return static_cast<int32_t>(offsetof(TestAddingPoints_t4285918360, ___LineRenderer_4)); }
	inline UILineRenderer_t3861579578 * get_LineRenderer_4() const { return ___LineRenderer_4; }
	inline UILineRenderer_t3861579578 ** get_address_of_LineRenderer_4() { return &___LineRenderer_4; }
	inline void set_LineRenderer_4(UILineRenderer_t3861579578 * value)
	{
		___LineRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___LineRenderer_4), value);
	}

	inline static int32_t get_offset_of_XValue_5() { return static_cast<int32_t>(offsetof(TestAddingPoints_t4285918360, ___XValue_5)); }
	inline Text_t1901882714 * get_XValue_5() const { return ___XValue_5; }
	inline Text_t1901882714 ** get_address_of_XValue_5() { return &___XValue_5; }
	inline void set_XValue_5(Text_t1901882714 * value)
	{
		___XValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___XValue_5), value);
	}

	inline static int32_t get_offset_of_YValue_6() { return static_cast<int32_t>(offsetof(TestAddingPoints_t4285918360, ___YValue_6)); }
	inline Text_t1901882714 * get_YValue_6() const { return ___YValue_6; }
	inline Text_t1901882714 ** get_address_of_YValue_6() { return &___YValue_6; }
	inline void set_YValue_6(Text_t1901882714 * value)
	{
		___YValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___YValue_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTADDINGPOINTS_T4285918360_H
#ifndef UPDATERADIALVALUE_T3318911919_H
#define UPDATERADIALVALUE_T3318911919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.UpdateRadialValue
struct  UpdateRadialValue_t3318911919  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.UpdateRadialValue::input
	InputField_t3762917431 * ___input_4;
	// UnityEngine.UI.Extensions.RadialSlider UnityEngine.UI.Extensions.Examples.UpdateRadialValue::slider
	RadialSlider_t2127270712 * ___slider_5;

public:
	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(UpdateRadialValue_t3318911919, ___input_4)); }
	inline InputField_t3762917431 * get_input_4() const { return ___input_4; }
	inline InputField_t3762917431 ** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(InputField_t3762917431 * value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier((&___input_4), value);
	}

	inline static int32_t get_offset_of_slider_5() { return static_cast<int32_t>(offsetof(UpdateRadialValue_t3318911919, ___slider_5)); }
	inline RadialSlider_t2127270712 * get_slider_5() const { return ___slider_5; }
	inline RadialSlider_t2127270712 ** get_address_of_slider_5() { return &___slider_5; }
	inline void set_slider_5(RadialSlider_t2127270712 * value)
	{
		___slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___slider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATERADIALVALUE_T3318911919_H
#ifndef UPDATESCROLLSNAP_T208304835_H
#define UPDATESCROLLSNAP_T208304835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.UpdateScrollSnap
struct  UpdateScrollSnap_t208304835  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.HorizontalScrollSnap UnityEngine.UI.Extensions.Examples.UpdateScrollSnap::HSS
	HorizontalScrollSnap_t1980761718 * ___HSS_4;
	// UnityEngine.UI.Extensions.VerticalScrollSnap UnityEngine.UI.Extensions.Examples.UpdateScrollSnap::VSS
	VerticalScrollSnap_t3018533569 * ___VSS_5;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.UpdateScrollSnap::HorizontalPagePrefab
	GameObject_t1113636619 * ___HorizontalPagePrefab_6;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.Examples.UpdateScrollSnap::VerticalPagePrefab
	GameObject_t1113636619 * ___VerticalPagePrefab_7;
	// UnityEngine.UI.InputField UnityEngine.UI.Extensions.Examples.UpdateScrollSnap::JumpPage
	InputField_t3762917431 * ___JumpPage_8;

public:
	inline static int32_t get_offset_of_HSS_4() { return static_cast<int32_t>(offsetof(UpdateScrollSnap_t208304835, ___HSS_4)); }
	inline HorizontalScrollSnap_t1980761718 * get_HSS_4() const { return ___HSS_4; }
	inline HorizontalScrollSnap_t1980761718 ** get_address_of_HSS_4() { return &___HSS_4; }
	inline void set_HSS_4(HorizontalScrollSnap_t1980761718 * value)
	{
		___HSS_4 = value;
		Il2CppCodeGenWriteBarrier((&___HSS_4), value);
	}

	inline static int32_t get_offset_of_VSS_5() { return static_cast<int32_t>(offsetof(UpdateScrollSnap_t208304835, ___VSS_5)); }
	inline VerticalScrollSnap_t3018533569 * get_VSS_5() const { return ___VSS_5; }
	inline VerticalScrollSnap_t3018533569 ** get_address_of_VSS_5() { return &___VSS_5; }
	inline void set_VSS_5(VerticalScrollSnap_t3018533569 * value)
	{
		___VSS_5 = value;
		Il2CppCodeGenWriteBarrier((&___VSS_5), value);
	}

	inline static int32_t get_offset_of_HorizontalPagePrefab_6() { return static_cast<int32_t>(offsetof(UpdateScrollSnap_t208304835, ___HorizontalPagePrefab_6)); }
	inline GameObject_t1113636619 * get_HorizontalPagePrefab_6() const { return ___HorizontalPagePrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_HorizontalPagePrefab_6() { return &___HorizontalPagePrefab_6; }
	inline void set_HorizontalPagePrefab_6(GameObject_t1113636619 * value)
	{
		___HorizontalPagePrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___HorizontalPagePrefab_6), value);
	}

	inline static int32_t get_offset_of_VerticalPagePrefab_7() { return static_cast<int32_t>(offsetof(UpdateScrollSnap_t208304835, ___VerticalPagePrefab_7)); }
	inline GameObject_t1113636619 * get_VerticalPagePrefab_7() const { return ___VerticalPagePrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_VerticalPagePrefab_7() { return &___VerticalPagePrefab_7; }
	inline void set_VerticalPagePrefab_7(GameObject_t1113636619 * value)
	{
		___VerticalPagePrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___VerticalPagePrefab_7), value);
	}

	inline static int32_t get_offset_of_JumpPage_8() { return static_cast<int32_t>(offsetof(UpdateScrollSnap_t208304835, ___JumpPage_8)); }
	inline InputField_t3762917431 * get_JumpPage_8() const { return ___JumpPage_8; }
	inline InputField_t3762917431 ** get_address_of_JumpPage_8() { return &___JumpPage_8; }
	inline void set_JumpPage_8(InputField_t3762917431 * value)
	{
		___JumpPage_8 = value;
		Il2CppCodeGenWriteBarrier((&___JumpPage_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATESCROLLSNAP_T208304835_H
#ifndef TESTHREF_T2744501834_H
#define TESTHREF_T2744501834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.testHref
struct  testHref_t2744501834  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Extensions.TextPic UnityEngine.UI.Extensions.Examples.testHref::textPic
	TextPic_t3289895869 * ___textPic_4;

public:
	inline static int32_t get_offset_of_textPic_4() { return static_cast<int32_t>(offsetof(testHref_t2744501834, ___textPic_4)); }
	inline TextPic_t3289895869 * get_textPic_4() const { return ___textPic_4; }
	inline TextPic_t3289895869 ** get_address_of_textPic_4() { return &___textPic_4; }
	inline void set_textPic_4(TextPic_t3289895869 * value)
	{
		___textPic_4 = value;
		Il2CppCodeGenWriteBarrier((&___textPic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTHREF_T2744501834_H
#ifndef FANCYSCROLLVIEWCELL_2_T538255395_H
#define FANCYSCROLLVIEWCELL_2_T538255395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollViewCell`2<UnityEngine.UI.Extensions.Examples.Example01CellDto,UnityEngine.UI.Extensions.FancyScrollViewNullContext>
struct  FancyScrollViewCell_2_t538255395  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.UI.Extensions.FancyScrollViewCell`2::<DataIndex>k__BackingField
	int32_t ___U3CDataIndexU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CDataIndexU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FancyScrollViewCell_2_t538255395, ___U3CDataIndexU3Ek__BackingField_4)); }
	inline int32_t get_U3CDataIndexU3Ek__BackingField_4() const { return ___U3CDataIndexU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CDataIndexU3Ek__BackingField_4() { return &___U3CDataIndexU3Ek__BackingField_4; }
	inline void set_U3CDataIndexU3Ek__BackingField_4(int32_t value)
	{
		___U3CDataIndexU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEWCELL_2_T538255395_H
#ifndef FANCYSCROLLVIEWCELL_2_T4151960851_H
#define FANCYSCROLLVIEWCELL_2_T4151960851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollViewCell`2<UnityEngine.UI.Extensions.Examples.Example02CellDto,UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext>
struct  FancyScrollViewCell_2_t4151960851  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.UI.Extensions.FancyScrollViewCell`2::<DataIndex>k__BackingField
	int32_t ___U3CDataIndexU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CDataIndexU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FancyScrollViewCell_2_t4151960851, ___U3CDataIndexU3Ek__BackingField_4)); }
	inline int32_t get_U3CDataIndexU3Ek__BackingField_4() const { return ___U3CDataIndexU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CDataIndexU3Ek__BackingField_4() { return &___U3CDataIndexU3Ek__BackingField_4; }
	inline void set_U3CDataIndexU3Ek__BackingField_4(int32_t value)
	{
		___U3CDataIndexU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEWCELL_2_T4151960851_H
#ifndef FANCYSCROLLVIEWCELL_2_T3638465384_H
#define FANCYSCROLLVIEWCELL_2_T3638465384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollViewCell`2<UnityEngine.UI.Extensions.Examples.Example03CellDto,UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext>
struct  FancyScrollViewCell_2_t3638465384  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.UI.Extensions.FancyScrollViewCell`2::<DataIndex>k__BackingField
	int32_t ___U3CDataIndexU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CDataIndexU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FancyScrollViewCell_2_t3638465384, ___U3CDataIndexU3Ek__BackingField_4)); }
	inline int32_t get_U3CDataIndexU3Ek__BackingField_4() const { return ___U3CDataIndexU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CDataIndexU3Ek__BackingField_4() { return &___U3CDataIndexU3Ek__BackingField_4; }
	inline void set_U3CDataIndexU3Ek__BackingField_4(int32_t value)
	{
		___U3CDataIndexU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEWCELL_2_T3638465384_H
#ifndef FANCYSCROLLVIEW_2_T1122335903_H
#define FANCYSCROLLVIEW_2_T1122335903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollView`2<UnityEngine.UI.Extensions.Examples.Example01CellDto,UnityEngine.UI.Extensions.FancyScrollViewNullContext>
struct  FancyScrollView_2_t1122335903  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::cellInterval
	float ___cellInterval_4;
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::cellOffset
	float ___cellOffset_5;
	// System.Boolean UnityEngine.UI.Extensions.FancyScrollView`2::loop
	bool ___loop_6;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.FancyScrollView`2::cellBase
	GameObject_t1113636619 * ___cellBase_7;
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::currentPosition
	float ___currentPosition_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.FancyScrollViewCell`2<TData,TContext>> UnityEngine.UI.Extensions.FancyScrollView`2::cells
	List_1_t2010330137 * ___cells_9;
	// TContext UnityEngine.UI.Extensions.FancyScrollView`2::context
	FancyScrollViewNullContext_t3783020080 * ___context_10;
	// System.Collections.Generic.List`1<TData> UnityEngine.UI.Extensions.FancyScrollView`2::cellData
	List_1_t872279796 * ___cellData_11;

public:
	inline static int32_t get_offset_of_cellInterval_4() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___cellInterval_4)); }
	inline float get_cellInterval_4() const { return ___cellInterval_4; }
	inline float* get_address_of_cellInterval_4() { return &___cellInterval_4; }
	inline void set_cellInterval_4(float value)
	{
		___cellInterval_4 = value;
	}

	inline static int32_t get_offset_of_cellOffset_5() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___cellOffset_5)); }
	inline float get_cellOffset_5() const { return ___cellOffset_5; }
	inline float* get_address_of_cellOffset_5() { return &___cellOffset_5; }
	inline void set_cellOffset_5(float value)
	{
		___cellOffset_5 = value;
	}

	inline static int32_t get_offset_of_loop_6() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___loop_6)); }
	inline bool get_loop_6() const { return ___loop_6; }
	inline bool* get_address_of_loop_6() { return &___loop_6; }
	inline void set_loop_6(bool value)
	{
		___loop_6 = value;
	}

	inline static int32_t get_offset_of_cellBase_7() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___cellBase_7)); }
	inline GameObject_t1113636619 * get_cellBase_7() const { return ___cellBase_7; }
	inline GameObject_t1113636619 ** get_address_of_cellBase_7() { return &___cellBase_7; }
	inline void set_cellBase_7(GameObject_t1113636619 * value)
	{
		___cellBase_7 = value;
		Il2CppCodeGenWriteBarrier((&___cellBase_7), value);
	}

	inline static int32_t get_offset_of_currentPosition_8() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___currentPosition_8)); }
	inline float get_currentPosition_8() const { return ___currentPosition_8; }
	inline float* get_address_of_currentPosition_8() { return &___currentPosition_8; }
	inline void set_currentPosition_8(float value)
	{
		___currentPosition_8 = value;
	}

	inline static int32_t get_offset_of_cells_9() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___cells_9)); }
	inline List_1_t2010330137 * get_cells_9() const { return ___cells_9; }
	inline List_1_t2010330137 ** get_address_of_cells_9() { return &___cells_9; }
	inline void set_cells_9(List_1_t2010330137 * value)
	{
		___cells_9 = value;
		Il2CppCodeGenWriteBarrier((&___cells_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___context_10)); }
	inline FancyScrollViewNullContext_t3783020080 * get_context_10() const { return ___context_10; }
	inline FancyScrollViewNullContext_t3783020080 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(FancyScrollViewNullContext_t3783020080 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_cellData_11() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t1122335903, ___cellData_11)); }
	inline List_1_t872279796 * get_cellData_11() const { return ___cellData_11; }
	inline List_1_t872279796 ** get_address_of_cellData_11() { return &___cellData_11; }
	inline void set_cellData_11(List_1_t872279796 * value)
	{
		___cellData_11 = value;
		Il2CppCodeGenWriteBarrier((&___cellData_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEW_2_T1122335903_H
#ifndef FANCYSCROLLVIEW_2_T441074063_H
#define FANCYSCROLLVIEW_2_T441074063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollView`2<UnityEngine.UI.Extensions.Examples.Example02CellDto,UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext>
struct  FancyScrollView_2_t441074063  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::cellInterval
	float ___cellInterval_4;
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::cellOffset
	float ___cellOffset_5;
	// System.Boolean UnityEngine.UI.Extensions.FancyScrollView`2::loop
	bool ___loop_6;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.FancyScrollView`2::cellBase
	GameObject_t1113636619 * ___cellBase_7;
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::currentPosition
	float ___currentPosition_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.FancyScrollViewCell`2<TData,TContext>> UnityEngine.UI.Extensions.FancyScrollView`2::cells
	List_1_t1329068297 * ___cells_9;
	// TContext UnityEngine.UI.Extensions.FancyScrollView`2::context
	Example02ScrollViewContext_t1179128608 * ___context_10;
	// System.Collections.Generic.List`1<TData> UnityEngine.UI.Extensions.FancyScrollView`2::cellData
	List_1_t1016655604 * ___cellData_11;

public:
	inline static int32_t get_offset_of_cellInterval_4() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___cellInterval_4)); }
	inline float get_cellInterval_4() const { return ___cellInterval_4; }
	inline float* get_address_of_cellInterval_4() { return &___cellInterval_4; }
	inline void set_cellInterval_4(float value)
	{
		___cellInterval_4 = value;
	}

	inline static int32_t get_offset_of_cellOffset_5() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___cellOffset_5)); }
	inline float get_cellOffset_5() const { return ___cellOffset_5; }
	inline float* get_address_of_cellOffset_5() { return &___cellOffset_5; }
	inline void set_cellOffset_5(float value)
	{
		___cellOffset_5 = value;
	}

	inline static int32_t get_offset_of_loop_6() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___loop_6)); }
	inline bool get_loop_6() const { return ___loop_6; }
	inline bool* get_address_of_loop_6() { return &___loop_6; }
	inline void set_loop_6(bool value)
	{
		___loop_6 = value;
	}

	inline static int32_t get_offset_of_cellBase_7() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___cellBase_7)); }
	inline GameObject_t1113636619 * get_cellBase_7() const { return ___cellBase_7; }
	inline GameObject_t1113636619 ** get_address_of_cellBase_7() { return &___cellBase_7; }
	inline void set_cellBase_7(GameObject_t1113636619 * value)
	{
		___cellBase_7 = value;
		Il2CppCodeGenWriteBarrier((&___cellBase_7), value);
	}

	inline static int32_t get_offset_of_currentPosition_8() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___currentPosition_8)); }
	inline float get_currentPosition_8() const { return ___currentPosition_8; }
	inline float* get_address_of_currentPosition_8() { return &___currentPosition_8; }
	inline void set_currentPosition_8(float value)
	{
		___currentPosition_8 = value;
	}

	inline static int32_t get_offset_of_cells_9() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___cells_9)); }
	inline List_1_t1329068297 * get_cells_9() const { return ___cells_9; }
	inline List_1_t1329068297 ** get_address_of_cells_9() { return &___cells_9; }
	inline void set_cells_9(List_1_t1329068297 * value)
	{
		___cells_9 = value;
		Il2CppCodeGenWriteBarrier((&___cells_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___context_10)); }
	inline Example02ScrollViewContext_t1179128608 * get_context_10() const { return ___context_10; }
	inline Example02ScrollViewContext_t1179128608 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(Example02ScrollViewContext_t1179128608 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_cellData_11() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t441074063, ___cellData_11)); }
	inline List_1_t1016655604 * get_cellData_11() const { return ___cellData_11; }
	inline List_1_t1016655604 ** get_address_of_cellData_11() { return &___cellData_11; }
	inline void set_cellData_11(List_1_t1016655604 * value)
	{
		___cellData_11 = value;
		Il2CppCodeGenWriteBarrier((&___cellData_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEW_2_T441074063_H
#ifndef FANCYSCROLLVIEW_2_T4222545892_H
#define FANCYSCROLLVIEW_2_T4222545892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollView`2<UnityEngine.UI.Extensions.Examples.Example03CellDto,UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext>
struct  FancyScrollView_2_t4222545892  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::cellInterval
	float ___cellInterval_4;
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::cellOffset
	float ___cellOffset_5;
	// System.Boolean UnityEngine.UI.Extensions.FancyScrollView`2::loop
	bool ___loop_6;
	// UnityEngine.GameObject UnityEngine.UI.Extensions.FancyScrollView`2::cellBase
	GameObject_t1113636619 * ___cellBase_7;
	// System.Single UnityEngine.UI.Extensions.FancyScrollView`2::currentPosition
	float ___currentPosition_8;
	// System.Collections.Generic.List`1<UnityEngine.UI.Extensions.FancyScrollViewCell`2<TData,TContext>> UnityEngine.UI.Extensions.FancyScrollView`2::cells
	List_1_t815572830 * ___cells_9;
	// TContext UnityEngine.UI.Extensions.FancyScrollView`2::context
	Example03ScrollViewContext_t47956341 * ___context_10;
	// System.Collections.Generic.List`1<TData> UnityEngine.UI.Extensions.FancyScrollView`2::cellData
	List_1_t89911028 * ___cellData_11;

public:
	inline static int32_t get_offset_of_cellInterval_4() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___cellInterval_4)); }
	inline float get_cellInterval_4() const { return ___cellInterval_4; }
	inline float* get_address_of_cellInterval_4() { return &___cellInterval_4; }
	inline void set_cellInterval_4(float value)
	{
		___cellInterval_4 = value;
	}

	inline static int32_t get_offset_of_cellOffset_5() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___cellOffset_5)); }
	inline float get_cellOffset_5() const { return ___cellOffset_5; }
	inline float* get_address_of_cellOffset_5() { return &___cellOffset_5; }
	inline void set_cellOffset_5(float value)
	{
		___cellOffset_5 = value;
	}

	inline static int32_t get_offset_of_loop_6() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___loop_6)); }
	inline bool get_loop_6() const { return ___loop_6; }
	inline bool* get_address_of_loop_6() { return &___loop_6; }
	inline void set_loop_6(bool value)
	{
		___loop_6 = value;
	}

	inline static int32_t get_offset_of_cellBase_7() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___cellBase_7)); }
	inline GameObject_t1113636619 * get_cellBase_7() const { return ___cellBase_7; }
	inline GameObject_t1113636619 ** get_address_of_cellBase_7() { return &___cellBase_7; }
	inline void set_cellBase_7(GameObject_t1113636619 * value)
	{
		___cellBase_7 = value;
		Il2CppCodeGenWriteBarrier((&___cellBase_7), value);
	}

	inline static int32_t get_offset_of_currentPosition_8() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___currentPosition_8)); }
	inline float get_currentPosition_8() const { return ___currentPosition_8; }
	inline float* get_address_of_currentPosition_8() { return &___currentPosition_8; }
	inline void set_currentPosition_8(float value)
	{
		___currentPosition_8 = value;
	}

	inline static int32_t get_offset_of_cells_9() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___cells_9)); }
	inline List_1_t815572830 * get_cells_9() const { return ___cells_9; }
	inline List_1_t815572830 ** get_address_of_cells_9() { return &___cells_9; }
	inline void set_cells_9(List_1_t815572830 * value)
	{
		___cells_9 = value;
		Il2CppCodeGenWriteBarrier((&___cells_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___context_10)); }
	inline Example03ScrollViewContext_t47956341 * get_context_10() const { return ___context_10; }
	inline Example03ScrollViewContext_t47956341 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(Example03ScrollViewContext_t47956341 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_cellData_11() { return static_cast<int32_t>(offsetof(FancyScrollView_2_t4222545892, ___cellData_11)); }
	inline List_1_t89911028 * get_cellData_11() const { return ___cellData_11; }
	inline List_1_t89911028 ** get_address_of_cellData_11() { return &___cellData_11; }
	inline void set_cellData_11(List_1_t89911028 * value)
	{
		___cellData_11 = value;
		Il2CppCodeGenWriteBarrier((&___cellData_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEW_2_T4222545892_H
#ifndef MENU_T3172337931_H
#define MENU_T3172337931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu
struct  Menu_t3172337931  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.UI.Extensions.Menu::DestroyWhenClosed
	bool ___DestroyWhenClosed_4;
	// System.Boolean UnityEngine.UI.Extensions.Menu::DisableMenusUnderneath
	bool ___DisableMenusUnderneath_5;

public:
	inline static int32_t get_offset_of_DestroyWhenClosed_4() { return static_cast<int32_t>(offsetof(Menu_t3172337931, ___DestroyWhenClosed_4)); }
	inline bool get_DestroyWhenClosed_4() const { return ___DestroyWhenClosed_4; }
	inline bool* get_address_of_DestroyWhenClosed_4() { return &___DestroyWhenClosed_4; }
	inline void set_DestroyWhenClosed_4(bool value)
	{
		___DestroyWhenClosed_4 = value;
	}

	inline static int32_t get_offset_of_DisableMenusUnderneath_5() { return static_cast<int32_t>(offsetof(Menu_t3172337931, ___DisableMenusUnderneath_5)); }
	inline bool get_DisableMenusUnderneath_5() const { return ___DisableMenusUnderneath_5; }
	inline bool* get_address_of_DisableMenusUnderneath_5() { return &___DisableMenusUnderneath_5; }
	inline void set_DisableMenusUnderneath_5(bool value)
	{
		___DisableMenusUnderneath_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_T3172337931_H
#ifndef TILTWINDOW_T4093125161_H
#define TILTWINDOW_T4093125161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.TiltWindow
struct  TiltWindow_t4093125161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TiltWindow::range
	Vector2_t2156229523  ___range_4;
	// UnityEngine.Transform UnityEngine.UI.Extensions.TiltWindow::mTrans
	Transform_t3600365921 * ___mTrans_5;
	// UnityEngine.Quaternion UnityEngine.UI.Extensions.TiltWindow::mStart
	Quaternion_t2301928331  ___mStart_6;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.TiltWindow::mRot
	Vector2_t2156229523  ___mRot_7;

public:
	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___range_4)); }
	inline Vector2_t2156229523  get_range_4() const { return ___range_4; }
	inline Vector2_t2156229523 * get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(Vector2_t2156229523  value)
	{
		___range_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___mTrans_5)); }
	inline Transform_t3600365921 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t3600365921 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t3600365921 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mStart_6() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___mStart_6)); }
	inline Quaternion_t2301928331  get_mStart_6() const { return ___mStart_6; }
	inline Quaternion_t2301928331 * get_address_of_mStart_6() { return &___mStart_6; }
	inline void set_mStart_6(Quaternion_t2301928331  value)
	{
		___mStart_6 = value;
	}

	inline static int32_t get_offset_of_mRot_7() { return static_cast<int32_t>(offsetof(TiltWindow_t4093125161, ___mRot_7)); }
	inline Vector2_t2156229523  get_mRot_7() const { return ___mRot_7; }
	inline Vector2_t2156229523 * get_address_of_mRot_7() { return &___mRot_7; }
	inline void set_mRot_7(Vector2_t2156229523  value)
	{
		___mRot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTWINDOW_T4093125161_H
#ifndef VIDEOCONTROLLER_T2238106033_H
#define VIDEOCONTROLLER_T2238106033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoController
struct  VideoController_t2238106033  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Video.VideoPlayer VideoController::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_4;
	// UnityEngine.UI.Button VideoController::m_PlayButton
	Button_t4055032469 * ___m_PlayButton_5;
	// UnityEngine.RectTransform VideoController::m_ProgressBar
	RectTransform_t3704657025 * ___m_ProgressBar_6;

public:
	inline static int32_t get_offset_of_videoPlayer_4() { return static_cast<int32_t>(offsetof(VideoController_t2238106033, ___videoPlayer_4)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_4() const { return ___videoPlayer_4; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_4() { return &___videoPlayer_4; }
	inline void set_videoPlayer_4(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_4), value);
	}

	inline static int32_t get_offset_of_m_PlayButton_5() { return static_cast<int32_t>(offsetof(VideoController_t2238106033, ___m_PlayButton_5)); }
	inline Button_t4055032469 * get_m_PlayButton_5() const { return ___m_PlayButton_5; }
	inline Button_t4055032469 ** get_address_of_m_PlayButton_5() { return &___m_PlayButton_5; }
	inline void set_m_PlayButton_5(Button_t4055032469 * value)
	{
		___m_PlayButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayButton_5), value);
	}

	inline static int32_t get_offset_of_m_ProgressBar_6() { return static_cast<int32_t>(offsetof(VideoController_t2238106033, ___m_ProgressBar_6)); }
	inline RectTransform_t3704657025 * get_m_ProgressBar_6() const { return ___m_ProgressBar_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_ProgressBar_6() { return &___m_ProgressBar_6; }
	inline void set_m_ProgressBar_6(RectTransform_t3704657025 * value)
	{
		___m_ProgressBar_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProgressBar_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCONTROLLER_T2238106033_H
#ifndef ASTRONAUT_T1874369735_H
#define ASTRONAUT_T1874369735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Astronaut
struct  Astronaut_t1874369735  : public Augmentation_t2596699517
{
public:
	// DrillController Astronaut::m_Drill
	DrillController_t3872647469 * ___m_Drill_11;
	// UnityEngine.ParticleSystem Astronaut::m_DrillEffectSmoke
	ParticleSystem_t1800779281 * ___m_DrillEffectSmoke_12;
	// UnityEngine.ParticleSystem Astronaut::m_DrillEffectRocks
	ParticleSystem_t1800779281 * ___m_DrillEffectRocks_13;
	// RockPileController Astronaut::m_RockPile
	RockPileController_t2893225296 * ___m_RockPile_14;

public:
	inline static int32_t get_offset_of_m_Drill_11() { return static_cast<int32_t>(offsetof(Astronaut_t1874369735, ___m_Drill_11)); }
	inline DrillController_t3872647469 * get_m_Drill_11() const { return ___m_Drill_11; }
	inline DrillController_t3872647469 ** get_address_of_m_Drill_11() { return &___m_Drill_11; }
	inline void set_m_Drill_11(DrillController_t3872647469 * value)
	{
		___m_Drill_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Drill_11), value);
	}

	inline static int32_t get_offset_of_m_DrillEffectSmoke_12() { return static_cast<int32_t>(offsetof(Astronaut_t1874369735, ___m_DrillEffectSmoke_12)); }
	inline ParticleSystem_t1800779281 * get_m_DrillEffectSmoke_12() const { return ___m_DrillEffectSmoke_12; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_DrillEffectSmoke_12() { return &___m_DrillEffectSmoke_12; }
	inline void set_m_DrillEffectSmoke_12(ParticleSystem_t1800779281 * value)
	{
		___m_DrillEffectSmoke_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_DrillEffectSmoke_12), value);
	}

	inline static int32_t get_offset_of_m_DrillEffectRocks_13() { return static_cast<int32_t>(offsetof(Astronaut_t1874369735, ___m_DrillEffectRocks_13)); }
	inline ParticleSystem_t1800779281 * get_m_DrillEffectRocks_13() const { return ___m_DrillEffectRocks_13; }
	inline ParticleSystem_t1800779281 ** get_address_of_m_DrillEffectRocks_13() { return &___m_DrillEffectRocks_13; }
	inline void set_m_DrillEffectRocks_13(ParticleSystem_t1800779281 * value)
	{
		___m_DrillEffectRocks_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_DrillEffectRocks_13), value);
	}

	inline static int32_t get_offset_of_m_RockPile_14() { return static_cast<int32_t>(offsetof(Astronaut_t1874369735, ___m_RockPile_14)); }
	inline RockPileController_t2893225296 * get_m_RockPile_14() const { return ___m_RockPile_14; }
	inline RockPileController_t2893225296 ** get_address_of_m_RockPile_14() { return &___m_RockPile_14; }
	inline void set_m_RockPile_14(RockPileController_t2893225296 * value)
	{
		___m_RockPile_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_RockPile_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTRONAUT_T1874369735_H
#ifndef DRONE_T259598734_H
#define DRONE_T259598734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Drone
struct  Drone_t259598734  : public Augmentation_t2596699517
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRONE_T259598734_H
#ifndef HABITAT_T2099304582_H
#define HABITAT_T2099304582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Habitat
struct  Habitat_t2099304582  : public Augmentation_t2596699517
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HABITAT_T2099304582_H
#ifndef OXYGENTANK_T391139921_H
#define OXYGENTANK_T391139921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OxygenTank
struct  OxygenTank_t391139921  : public Augmentation_t2596699517
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OXYGENTANK_T391139921_H
#ifndef EXAMPLE02SCROLLVIEW_T3654499597_H
#define EXAMPLE02SCROLLVIEW_T3654499597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example02ScrollView
struct  Example02ScrollView_t3654499597  : public FancyScrollView_2_t441074063
{
public:
	// UnityEngine.UI.Extensions.ScrollPositionController UnityEngine.UI.Extensions.Examples.Example02ScrollView::scrollPositionController
	ScrollPositionController_t2221482878 * ___scrollPositionController_12;

public:
	inline static int32_t get_offset_of_scrollPositionController_12() { return static_cast<int32_t>(offsetof(Example02ScrollView_t3654499597, ___scrollPositionController_12)); }
	inline ScrollPositionController_t2221482878 * get_scrollPositionController_12() const { return ___scrollPositionController_12; }
	inline ScrollPositionController_t2221482878 ** get_address_of_scrollPositionController_12() { return &___scrollPositionController_12; }
	inline void set_scrollPositionController_12(ScrollPositionController_t2221482878 * value)
	{
		___scrollPositionController_12 = value;
		Il2CppCodeGenWriteBarrier((&___scrollPositionController_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE02SCROLLVIEW_T3654499597_H
#ifndef EXAMPLE02SCROLLVIEWCELL_T1417103111_H
#define EXAMPLE02SCROLLVIEWCELL_T1417103111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell
struct  Example02ScrollViewCell_t1417103111  : public FancyScrollViewCell_2_t4151960851
{
public:
	// UnityEngine.Animator UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell::animator
	Animator_t434523843 * ___animator_5;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell::message
	Text_t1901882714 * ___message_6;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell::image
	Image_t2670269651 * ___image_7;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell::button
	Button_t4055032469 * ___button_8;
	// System.Int32 UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell::scrollTriggerHash
	int32_t ___scrollTriggerHash_9;
	// UnityEngine.UI.Extensions.Examples.Example02ScrollViewContext UnityEngine.UI.Extensions.Examples.Example02ScrollViewCell::context
	Example02ScrollViewContext_t1179128608 * ___context_10;

public:
	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(Example02ScrollViewCell_t1417103111, ___animator_5)); }
	inline Animator_t434523843 * get_animator_5() const { return ___animator_5; }
	inline Animator_t434523843 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t434523843 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___animator_5), value);
	}

	inline static int32_t get_offset_of_message_6() { return static_cast<int32_t>(offsetof(Example02ScrollViewCell_t1417103111, ___message_6)); }
	inline Text_t1901882714 * get_message_6() const { return ___message_6; }
	inline Text_t1901882714 ** get_address_of_message_6() { return &___message_6; }
	inline void set_message_6(Text_t1901882714 * value)
	{
		___message_6 = value;
		Il2CppCodeGenWriteBarrier((&___message_6), value);
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(Example02ScrollViewCell_t1417103111, ___image_7)); }
	inline Image_t2670269651 * get_image_7() const { return ___image_7; }
	inline Image_t2670269651 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(Image_t2670269651 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}

	inline static int32_t get_offset_of_button_8() { return static_cast<int32_t>(offsetof(Example02ScrollViewCell_t1417103111, ___button_8)); }
	inline Button_t4055032469 * get_button_8() const { return ___button_8; }
	inline Button_t4055032469 ** get_address_of_button_8() { return &___button_8; }
	inline void set_button_8(Button_t4055032469 * value)
	{
		___button_8 = value;
		Il2CppCodeGenWriteBarrier((&___button_8), value);
	}

	inline static int32_t get_offset_of_scrollTriggerHash_9() { return static_cast<int32_t>(offsetof(Example02ScrollViewCell_t1417103111, ___scrollTriggerHash_9)); }
	inline int32_t get_scrollTriggerHash_9() const { return ___scrollTriggerHash_9; }
	inline int32_t* get_address_of_scrollTriggerHash_9() { return &___scrollTriggerHash_9; }
	inline void set_scrollTriggerHash_9(int32_t value)
	{
		___scrollTriggerHash_9 = value;
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(Example02ScrollViewCell_t1417103111, ___context_10)); }
	inline Example02ScrollViewContext_t1179128608 * get_context_10() const { return ___context_10; }
	inline Example02ScrollViewContext_t1179128608 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(Example02ScrollViewContext_t1179128608 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE02SCROLLVIEWCELL_T1417103111_H
#ifndef EXAMPLE03SCROLLVIEW_T2613198093_H
#define EXAMPLE03SCROLLVIEW_T2613198093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example03ScrollView
struct  Example03ScrollView_t2613198093  : public FancyScrollView_2_t4222545892
{
public:
	// UnityEngine.UI.Extensions.ScrollPositionController UnityEngine.UI.Extensions.Examples.Example03ScrollView::scrollPositionController
	ScrollPositionController_t2221482878 * ___scrollPositionController_12;

public:
	inline static int32_t get_offset_of_scrollPositionController_12() { return static_cast<int32_t>(offsetof(Example03ScrollView_t2613198093, ___scrollPositionController_12)); }
	inline ScrollPositionController_t2221482878 * get_scrollPositionController_12() const { return ___scrollPositionController_12; }
	inline ScrollPositionController_t2221482878 ** get_address_of_scrollPositionController_12() { return &___scrollPositionController_12; }
	inline void set_scrollPositionController_12(ScrollPositionController_t2221482878 * value)
	{
		___scrollPositionController_12 = value;
		Il2CppCodeGenWriteBarrier((&___scrollPositionController_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE03SCROLLVIEW_T2613198093_H
#ifndef EXAMPLE03SCROLLVIEWCELL_T3211020039_H
#define EXAMPLE03SCROLLVIEWCELL_T3211020039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell
struct  Example03ScrollViewCell_t3211020039  : public FancyScrollViewCell_2_t3638465384
{
public:
	// UnityEngine.Animator UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell::animator
	Animator_t434523843 * ___animator_5;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell::message
	Text_t1901882714 * ___message_6;
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell::image
	Image_t2670269651 * ___image_7;
	// UnityEngine.UI.Button UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell::button
	Button_t4055032469 * ___button_8;
	// System.Int32 UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell::scrollTriggerHash
	int32_t ___scrollTriggerHash_9;
	// UnityEngine.UI.Extensions.Examples.Example03ScrollViewContext UnityEngine.UI.Extensions.Examples.Example03ScrollViewCell::context
	Example03ScrollViewContext_t47956341 * ___context_10;

public:
	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(Example03ScrollViewCell_t3211020039, ___animator_5)); }
	inline Animator_t434523843 * get_animator_5() const { return ___animator_5; }
	inline Animator_t434523843 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t434523843 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___animator_5), value);
	}

	inline static int32_t get_offset_of_message_6() { return static_cast<int32_t>(offsetof(Example03ScrollViewCell_t3211020039, ___message_6)); }
	inline Text_t1901882714 * get_message_6() const { return ___message_6; }
	inline Text_t1901882714 ** get_address_of_message_6() { return &___message_6; }
	inline void set_message_6(Text_t1901882714 * value)
	{
		___message_6 = value;
		Il2CppCodeGenWriteBarrier((&___message_6), value);
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(Example03ScrollViewCell_t3211020039, ___image_7)); }
	inline Image_t2670269651 * get_image_7() const { return ___image_7; }
	inline Image_t2670269651 ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(Image_t2670269651 * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}

	inline static int32_t get_offset_of_button_8() { return static_cast<int32_t>(offsetof(Example03ScrollViewCell_t3211020039, ___button_8)); }
	inline Button_t4055032469 * get_button_8() const { return ___button_8; }
	inline Button_t4055032469 ** get_address_of_button_8() { return &___button_8; }
	inline void set_button_8(Button_t4055032469 * value)
	{
		___button_8 = value;
		Il2CppCodeGenWriteBarrier((&___button_8), value);
	}

	inline static int32_t get_offset_of_scrollTriggerHash_9() { return static_cast<int32_t>(offsetof(Example03ScrollViewCell_t3211020039, ___scrollTriggerHash_9)); }
	inline int32_t get_scrollTriggerHash_9() const { return ___scrollTriggerHash_9; }
	inline int32_t* get_address_of_scrollTriggerHash_9() { return &___scrollTriggerHash_9; }
	inline void set_scrollTriggerHash_9(int32_t value)
	{
		___scrollTriggerHash_9 = value;
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(Example03ScrollViewCell_t3211020039, ___context_10)); }
	inline Example03ScrollViewContext_t47956341 * get_context_10() const { return ___context_10; }
	inline Example03ScrollViewContext_t47956341 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(Example03ScrollViewContext_t47956341 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE03SCROLLVIEWCELL_T3211020039_H
#ifndef FANCYSCROLLVIEWCELL_1_T1100186540_H
#define FANCYSCROLLVIEWCELL_1_T1100186540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollViewCell`1<UnityEngine.UI.Extensions.Examples.Example01CellDto>
struct  FancyScrollViewCell_1_t1100186540  : public FancyScrollViewCell_2_t538255395
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEWCELL_1_T1100186540_H
#ifndef FANCYSCROLLVIEW_1_T3974408614_H
#define FANCYSCROLLVIEW_1_T3974408614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.FancyScrollView`1<UnityEngine.UI.Extensions.Examples.Example01CellDto>
struct  FancyScrollView_1_t3974408614  : public FancyScrollView_2_t1122335903
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FANCYSCROLLVIEW_1_T3974408614_H
#ifndef MENU_1_T1430563936_H
#define MENU_1_T1430563936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu`1<UnityEngine.UI.Extensions.Examples.AwesomeMenu>
struct  Menu_1_t1430563936  : public Menu_t3172337931
{
public:

public:
};

struct Menu_1_t1430563936_StaticFields
{
public:
	// T UnityEngine.UI.Extensions.Menu`1::<Instance>k__BackingField
	AwesomeMenu_t320995884 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Menu_1_t1430563936_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline AwesomeMenu_t320995884 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline AwesomeMenu_t320995884 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(AwesomeMenu_t320995884 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_1_T1430563936_H
#ifndef MENU_1_T3307275099_H
#define MENU_1_T3307275099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu`1<UnityEngine.UI.Extensions.Examples.GameMenu>
struct  Menu_1_t3307275099  : public Menu_t3172337931
{
public:

public:
};

struct Menu_1_t3307275099_StaticFields
{
public:
	// T UnityEngine.UI.Extensions.Menu`1::<Instance>k__BackingField
	GameMenu_t2197707047 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Menu_1_t3307275099_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline GameMenu_t2197707047 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline GameMenu_t2197707047 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(GameMenu_t2197707047 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_1_T3307275099_H
#ifndef MENU_1_T625619660_H
#define MENU_1_T625619660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu`1<UnityEngine.UI.Extensions.Examples.MainMenu>
struct  Menu_1_t625619660  : public Menu_t3172337931
{
public:

public:
};

struct Menu_1_t625619660_StaticFields
{
public:
	// T UnityEngine.UI.Extensions.Menu`1::<Instance>k__BackingField
	MainMenu_t3811018904 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Menu_1_t625619660_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline MainMenu_t3811018904 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline MainMenu_t3811018904 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(MainMenu_t3811018904 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_1_T625619660_H
#ifndef MENU_1_T381279942_H
#define MENU_1_T381279942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu`1<UnityEngine.UI.Extensions.Examples.OptionsMenu>
struct  Menu_1_t381279942  : public Menu_t3172337931
{
public:

public:
};

struct Menu_1_t381279942_StaticFields
{
public:
	// T UnityEngine.UI.Extensions.Menu`1::<Instance>k__BackingField
	OptionsMenu_t3566679186 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Menu_1_t381279942_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline OptionsMenu_t3566679186 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline OptionsMenu_t3566679186 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(OptionsMenu_t3566679186 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_1_T381279942_H
#ifndef MENU_1_T2814838230_H
#define MENU_1_T2814838230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Menu`1<UnityEngine.UI.Extensions.Examples.PauseMenu>
struct  Menu_1_t2814838230  : public Menu_t3172337931
{
public:

public:
};

struct Menu_1_t2814838230_StaticFields
{
public:
	// T UnityEngine.UI.Extensions.Menu`1::<Instance>k__BackingField
	PauseMenu_t1705270178 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Menu_1_t2814838230_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline PauseMenu_t1705270178 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline PauseMenu_t1705270178 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(PauseMenu_t1705270178 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENU_1_T2814838230_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef VIDEOTRACKABLEEVENTHANDLER_T944783369_H
#define VIDEOTRACKABLEEVENTHANDLER_T944783369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VideoTrackableEventHandler
struct  VideoTrackableEventHandler_t944783369  : public DefaultTrackableEventHandler_t1588957063
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTRACKABLEEVENTHANDLER_T944783369_H
#ifndef BOXSLIDER_T3694973841_H
#define BOXSLIDER_T3694973841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.BoxSlider
struct  BoxSlider_t3694973841  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.BoxSlider::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_18;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_MinValue
	float ___m_MinValue_19;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_MaxValue
	float ___m_MaxValue_20;
	// System.Boolean UnityEngine.UI.Extensions.BoxSlider::m_WholeNumbers
	bool ___m_WholeNumbers_21;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_ValueX
	float ___m_ValueX_22;
	// System.Single UnityEngine.UI.Extensions.BoxSlider::m_ValueY
	float ___m_ValueY_23;
	// UnityEngine.UI.Extensions.BoxSlider/BoxSliderEvent UnityEngine.UI.Extensions.BoxSlider::m_OnValueChanged
	BoxSliderEvent_t4223315588 * ___m_OnValueChanged_24;
	// UnityEngine.Transform UnityEngine.UI.Extensions.BoxSlider::m_HandleTransform
	Transform_t3600365921 * ___m_HandleTransform_25;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.BoxSlider::m_HandleContainerRect
	RectTransform_t3704657025 * ___m_HandleContainerRect_26;
	// UnityEngine.Vector2 UnityEngine.UI.Extensions.BoxSlider::m_Offset
	Vector2_t2156229523  ___m_Offset_27;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Extensions.BoxSlider::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_28;

public:
	inline static int32_t get_offset_of_m_HandleRect_18() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_HandleRect_18)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_18() const { return ___m_HandleRect_18; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_18() { return &___m_HandleRect_18; }
	inline void set_m_HandleRect_18(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_18), value);
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_MinValue_19)); }
	inline float get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline float* get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(float value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_MaxValue_20)); }
	inline float get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline float* get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(float value)
	{
		___m_MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_21() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_WholeNumbers_21)); }
	inline bool get_m_WholeNumbers_21() const { return ___m_WholeNumbers_21; }
	inline bool* get_address_of_m_WholeNumbers_21() { return &___m_WholeNumbers_21; }
	inline void set_m_WholeNumbers_21(bool value)
	{
		___m_WholeNumbers_21 = value;
	}

	inline static int32_t get_offset_of_m_ValueX_22() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_ValueX_22)); }
	inline float get_m_ValueX_22() const { return ___m_ValueX_22; }
	inline float* get_address_of_m_ValueX_22() { return &___m_ValueX_22; }
	inline void set_m_ValueX_22(float value)
	{
		___m_ValueX_22 = value;
	}

	inline static int32_t get_offset_of_m_ValueY_23() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_ValueY_23)); }
	inline float get_m_ValueY_23() const { return ___m_ValueY_23; }
	inline float* get_address_of_m_ValueY_23() { return &___m_ValueY_23; }
	inline void set_m_ValueY_23(float value)
	{
		___m_ValueY_23 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_24() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_OnValueChanged_24)); }
	inline BoxSliderEvent_t4223315588 * get_m_OnValueChanged_24() const { return ___m_OnValueChanged_24; }
	inline BoxSliderEvent_t4223315588 ** get_address_of_m_OnValueChanged_24() { return &___m_OnValueChanged_24; }
	inline void set_m_OnValueChanged_24(BoxSliderEvent_t4223315588 * value)
	{
		___m_OnValueChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_24), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_25() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_HandleTransform_25)); }
	inline Transform_t3600365921 * get_m_HandleTransform_25() const { return ___m_HandleTransform_25; }
	inline Transform_t3600365921 ** get_address_of_m_HandleTransform_25() { return &___m_HandleTransform_25; }
	inline void set_m_HandleTransform_25(Transform_t3600365921 * value)
	{
		___m_HandleTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_25), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_26() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_HandleContainerRect_26)); }
	inline RectTransform_t3704657025 * get_m_HandleContainerRect_26() const { return ___m_HandleContainerRect_26; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleContainerRect_26() { return &___m_HandleContainerRect_26; }
	inline void set_m_HandleContainerRect_26(RectTransform_t3704657025 * value)
	{
		___m_HandleContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_26), value);
	}

	inline static int32_t get_offset_of_m_Offset_27() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_Offset_27)); }
	inline Vector2_t2156229523  get_m_Offset_27() const { return ___m_Offset_27; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_27() { return &___m_Offset_27; }
	inline void set_m_Offset_27(Vector2_t2156229523  value)
	{
		___m_Offset_27 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_28() { return static_cast<int32_t>(offsetof(BoxSlider_t3694973841, ___m_Tracker_28)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_28() const { return ___m_Tracker_28; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_28() { return &___m_Tracker_28; }
	inline void set_m_Tracker_28(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXSLIDER_T3694973841_H
#ifndef AWESOMEMENU_T320995884_H
#define AWESOMEMENU_T320995884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.AwesomeMenu
struct  AwesomeMenu_t320995884  : public Menu_1_t1430563936
{
public:
	// UnityEngine.UI.Image UnityEngine.UI.Extensions.Examples.AwesomeMenu::Background
	Image_t2670269651 * ___Background_7;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.AwesomeMenu::Title
	Text_t1901882714 * ___Title_8;

public:
	inline static int32_t get_offset_of_Background_7() { return static_cast<int32_t>(offsetof(AwesomeMenu_t320995884, ___Background_7)); }
	inline Image_t2670269651 * get_Background_7() const { return ___Background_7; }
	inline Image_t2670269651 ** get_address_of_Background_7() { return &___Background_7; }
	inline void set_Background_7(Image_t2670269651 * value)
	{
		___Background_7 = value;
		Il2CppCodeGenWriteBarrier((&___Background_7), value);
	}

	inline static int32_t get_offset_of_Title_8() { return static_cast<int32_t>(offsetof(AwesomeMenu_t320995884, ___Title_8)); }
	inline Text_t1901882714 * get_Title_8() const { return ___Title_8; }
	inline Text_t1901882714 ** get_address_of_Title_8() { return &___Title_8; }
	inline void set_Title_8(Text_t1901882714 * value)
	{
		___Title_8 = value;
		Il2CppCodeGenWriteBarrier((&___Title_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWESOMEMENU_T320995884_H
#ifndef EXAMPLE01SCROLLVIEW_T2860006669_H
#define EXAMPLE01SCROLLVIEW_T2860006669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example01ScrollView
struct  Example01ScrollView_t2860006669  : public FancyScrollView_1_t3974408614
{
public:
	// UnityEngine.UI.Extensions.ScrollPositionController UnityEngine.UI.Extensions.Examples.Example01ScrollView::scrollPositionController
	ScrollPositionController_t2221482878 * ___scrollPositionController_12;

public:
	inline static int32_t get_offset_of_scrollPositionController_12() { return static_cast<int32_t>(offsetof(Example01ScrollView_t2860006669, ___scrollPositionController_12)); }
	inline ScrollPositionController_t2221482878 * get_scrollPositionController_12() const { return ___scrollPositionController_12; }
	inline ScrollPositionController_t2221482878 ** get_address_of_scrollPositionController_12() { return &___scrollPositionController_12; }
	inline void set_scrollPositionController_12(ScrollPositionController_t2221482878 * value)
	{
		___scrollPositionController_12 = value;
		Il2CppCodeGenWriteBarrier((&___scrollPositionController_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE01SCROLLVIEW_T2860006669_H
#ifndef EXAMPLE01SCROLLVIEWCELL_T1502270060_H
#define EXAMPLE01SCROLLVIEWCELL_T1502270060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.Example01ScrollViewCell
struct  Example01ScrollViewCell_t1502270060  : public FancyScrollViewCell_1_t1100186540
{
public:
	// UnityEngine.Animator UnityEngine.UI.Extensions.Examples.Example01ScrollViewCell::animator
	Animator_t434523843 * ___animator_5;
	// UnityEngine.UI.Text UnityEngine.UI.Extensions.Examples.Example01ScrollViewCell::message
	Text_t1901882714 * ___message_6;
	// System.Int32 UnityEngine.UI.Extensions.Examples.Example01ScrollViewCell::scrollTriggerHash
	int32_t ___scrollTriggerHash_7;

public:
	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(Example01ScrollViewCell_t1502270060, ___animator_5)); }
	inline Animator_t434523843 * get_animator_5() const { return ___animator_5; }
	inline Animator_t434523843 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t434523843 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((&___animator_5), value);
	}

	inline static int32_t get_offset_of_message_6() { return static_cast<int32_t>(offsetof(Example01ScrollViewCell_t1502270060, ___message_6)); }
	inline Text_t1901882714 * get_message_6() const { return ___message_6; }
	inline Text_t1901882714 ** get_address_of_message_6() { return &___message_6; }
	inline void set_message_6(Text_t1901882714 * value)
	{
		___message_6 = value;
		Il2CppCodeGenWriteBarrier((&___message_6), value);
	}

	inline static int32_t get_offset_of_scrollTriggerHash_7() { return static_cast<int32_t>(offsetof(Example01ScrollViewCell_t1502270060, ___scrollTriggerHash_7)); }
	inline int32_t get_scrollTriggerHash_7() const { return ___scrollTriggerHash_7; }
	inline int32_t* get_address_of_scrollTriggerHash_7() { return &___scrollTriggerHash_7; }
	inline void set_scrollTriggerHash_7(int32_t value)
	{
		___scrollTriggerHash_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE01SCROLLVIEWCELL_T1502270060_H
#ifndef SIMPLEMENU_1_T1307066844_H
#define SIMPLEMENU_1_T1307066844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SimpleMenu`1<UnityEngine.UI.Extensions.Examples.GameMenu>
struct  SimpleMenu_1_t1307066844  : public Menu_1_t3307275099
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMENU_1_T1307066844_H
#ifndef SIMPLEMENU_1_T2920378701_H
#define SIMPLEMENU_1_T2920378701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SimpleMenu`1<UnityEngine.UI.Extensions.Examples.MainMenu>
struct  SimpleMenu_1_t2920378701  : public Menu_1_t625619660
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMENU_1_T2920378701_H
#ifndef SIMPLEMENU_1_T2676038983_H
#define SIMPLEMENU_1_T2676038983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SimpleMenu`1<UnityEngine.UI.Extensions.Examples.OptionsMenu>
struct  SimpleMenu_1_t2676038983  : public Menu_1_t381279942
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMENU_1_T2676038983_H
#ifndef SIMPLEMENU_1_T814629975_H
#define SIMPLEMENU_1_T814629975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.SimpleMenu`1<UnityEngine.UI.Extensions.Examples.PauseMenu>
struct  SimpleMenu_1_t814629975  : public Menu_1_t2814838230
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMENU_1_T814629975_H
#ifndef TOGGLE_T2735377061_H
#define TOGGLE_T2735377061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle
struct  Toggle_t2735377061  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Toggle/ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_18;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_t1660335611 * ___graphic_19;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t123837990 * ___m_Group_20;
	// UnityEngine.UI.Toggle/ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t1873685584 * ___onValueChanged_21;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_22;

public:
	inline static int32_t get_offset_of_toggleTransition_18() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___toggleTransition_18)); }
	inline int32_t get_toggleTransition_18() const { return ___toggleTransition_18; }
	inline int32_t* get_address_of_toggleTransition_18() { return &___toggleTransition_18; }
	inline void set_toggleTransition_18(int32_t value)
	{
		___toggleTransition_18 = value;
	}

	inline static int32_t get_offset_of_graphic_19() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___graphic_19)); }
	inline Graphic_t1660335611 * get_graphic_19() const { return ___graphic_19; }
	inline Graphic_t1660335611 ** get_address_of_graphic_19() { return &___graphic_19; }
	inline void set_graphic_19(Graphic_t1660335611 * value)
	{
		___graphic_19 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_19), value);
	}

	inline static int32_t get_offset_of_m_Group_20() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___m_Group_20)); }
	inline ToggleGroup_t123837990 * get_m_Group_20() const { return ___m_Group_20; }
	inline ToggleGroup_t123837990 ** get_address_of_m_Group_20() { return &___m_Group_20; }
	inline void set_m_Group_20(ToggleGroup_t123837990 * value)
	{
		___m_Group_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_Group_20), value);
	}

	inline static int32_t get_offset_of_onValueChanged_21() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___onValueChanged_21)); }
	inline ToggleEvent_t1873685584 * get_onValueChanged_21() const { return ___onValueChanged_21; }
	inline ToggleEvent_t1873685584 ** get_address_of_onValueChanged_21() { return &___onValueChanged_21; }
	inline void set_onValueChanged_21(ToggleEvent_t1873685584 * value)
	{
		___onValueChanged_21 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_21), value);
	}

	inline static int32_t get_offset_of_m_IsOn_22() { return static_cast<int32_t>(offsetof(Toggle_t2735377061, ___m_IsOn_22)); }
	inline bool get_m_IsOn_22() const { return ___m_IsOn_22; }
	inline bool* get_address_of_m_IsOn_22() { return &___m_IsOn_22; }
	inline void set_m_IsOn_22(bool value)
	{
		___m_IsOn_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLE_T2735377061_H
#ifndef ACCORDIONELEMENT_T774870540_H
#define ACCORDIONELEMENT_T774870540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.AccordionElement
struct  AccordionElement_t774870540  : public Toggle_t2735377061
{
public:
	// System.Single UnityEngine.UI.Extensions.AccordionElement::m_MinHeight
	float ___m_MinHeight_23;
	// UnityEngine.UI.Extensions.Accordion UnityEngine.UI.Extensions.AccordionElement::m_Accordion
	Accordion_t2301662264 * ___m_Accordion_24;
	// UnityEngine.RectTransform UnityEngine.UI.Extensions.AccordionElement::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_25;
	// UnityEngine.UI.LayoutElement UnityEngine.UI.Extensions.AccordionElement::m_LayoutElement
	LayoutElement_t1785403678 * ___m_LayoutElement_26;
	// UnityEngine.UI.Extensions.Tweens.TweenRunner`1<UnityEngine.UI.Extensions.Tweens.FloatTween> UnityEngine.UI.Extensions.AccordionElement::m_FloatTweenRunner
	TweenRunner_1_t2527600155 * ___m_FloatTweenRunner_27;

public:
	inline static int32_t get_offset_of_m_MinHeight_23() { return static_cast<int32_t>(offsetof(AccordionElement_t774870540, ___m_MinHeight_23)); }
	inline float get_m_MinHeight_23() const { return ___m_MinHeight_23; }
	inline float* get_address_of_m_MinHeight_23() { return &___m_MinHeight_23; }
	inline void set_m_MinHeight_23(float value)
	{
		___m_MinHeight_23 = value;
	}

	inline static int32_t get_offset_of_m_Accordion_24() { return static_cast<int32_t>(offsetof(AccordionElement_t774870540, ___m_Accordion_24)); }
	inline Accordion_t2301662264 * get_m_Accordion_24() const { return ___m_Accordion_24; }
	inline Accordion_t2301662264 ** get_address_of_m_Accordion_24() { return &___m_Accordion_24; }
	inline void set_m_Accordion_24(Accordion_t2301662264 * value)
	{
		___m_Accordion_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Accordion_24), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_25() { return static_cast<int32_t>(offsetof(AccordionElement_t774870540, ___m_RectTransform_25)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_25() const { return ___m_RectTransform_25; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_25() { return &___m_RectTransform_25; }
	inline void set_m_RectTransform_25(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_25), value);
	}

	inline static int32_t get_offset_of_m_LayoutElement_26() { return static_cast<int32_t>(offsetof(AccordionElement_t774870540, ___m_LayoutElement_26)); }
	inline LayoutElement_t1785403678 * get_m_LayoutElement_26() const { return ___m_LayoutElement_26; }
	inline LayoutElement_t1785403678 ** get_address_of_m_LayoutElement_26() { return &___m_LayoutElement_26; }
	inline void set_m_LayoutElement_26(LayoutElement_t1785403678 * value)
	{
		___m_LayoutElement_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_26), value);
	}

	inline static int32_t get_offset_of_m_FloatTweenRunner_27() { return static_cast<int32_t>(offsetof(AccordionElement_t774870540, ___m_FloatTweenRunner_27)); }
	inline TweenRunner_1_t2527600155 * get_m_FloatTweenRunner_27() const { return ___m_FloatTweenRunner_27; }
	inline TweenRunner_1_t2527600155 ** get_address_of_m_FloatTweenRunner_27() { return &___m_FloatTweenRunner_27; }
	inline void set_m_FloatTweenRunner_27(TweenRunner_1_t2527600155 * value)
	{
		___m_FloatTweenRunner_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_FloatTweenRunner_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCORDIONELEMENT_T774870540_H
#ifndef GAMEMENU_T2197707047_H
#define GAMEMENU_T2197707047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.GameMenu
struct  GameMenu_t2197707047  : public SimpleMenu_1_t1307066844
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMENU_T2197707047_H
#ifndef MAINMENU_T3811018904_H
#define MAINMENU_T3811018904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.MainMenu
struct  MainMenu_t3811018904  : public SimpleMenu_1_t2920378701
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T3811018904_H
#ifndef OPTIONSMENU_T3566679186_H
#define OPTIONSMENU_T3566679186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.OptionsMenu
struct  OptionsMenu_t3566679186  : public SimpleMenu_1_t2676038983
{
public:
	// UnityEngine.UI.Slider UnityEngine.UI.Extensions.Examples.OptionsMenu::Slider
	Slider_t3903728902 * ___Slider_7;

public:
	inline static int32_t get_offset_of_Slider_7() { return static_cast<int32_t>(offsetof(OptionsMenu_t3566679186, ___Slider_7)); }
	inline Slider_t3903728902 * get_Slider_7() const { return ___Slider_7; }
	inline Slider_t3903728902 ** get_address_of_Slider_7() { return &___Slider_7; }
	inline void set_Slider_7(Slider_t3903728902 * value)
	{
		___Slider_7 = value;
		Il2CppCodeGenWriteBarrier((&___Slider_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONSMENU_T3566679186_H
#ifndef PAUSEMENU_T1705270178_H
#define PAUSEMENU_T1705270178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Extensions.Examples.PauseMenu
struct  PauseMenu_t1705270178  : public SimpleMenu_1_t814629975
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_T1705270178_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4500 = { sizeof (SceneOrientation_t160940122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4500[1] = 
{
	SceneOrientation_t160940122::get_offset_of_sceneOrientation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4501 = { sizeof (Orientation_t3671038586)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4501[4] = 
{
	Orientation_t3671038586::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4502 = { sizeof (StatusMessage_t696038889), -1, sizeof(StatusMessage_t696038889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4502[5] = 
{
	StatusMessage_t696038889::get_offset_of_canvasGroup_4(),
	StatusMessage_t696038889::get_offset_of_message_5(),
	StatusMessage_t696038889::get_offset_of_initialized_6(),
	StatusMessage_t696038889::get_offset_of_showing_7(),
	StatusMessage_t696038889_StaticFields::get_offset_of_statusMessage_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4503 = { sizeof (TapHandler_t334234343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4503[5] = 
{
	0,
	TapHandler_t334234343::get_offset_of_mTimeSinceLastTap_5(),
	TapHandler_t334234343::get_offset_of_m_MenuOptions_6(),
	TapHandler_t334234343::get_offset_of_m_CameraSettings_7(),
	TapHandler_t334234343::get_offset_of_mTapCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4504 = { sizeof (TrackableSettings_t2862243993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4504[4] = 
{
	TrackableSettings_t2862243993::get_offset_of_deviceTrackerEnabled_4(),
	TrackableSettings_t2862243993::get_offset_of_positionalDeviceTracker_5(),
	TrackableSettings_t2862243993::get_offset_of_relocalizationStatusDelayTimer_6(),
	TrackableSettings_t2862243993::get_offset_of_resetDeviceTrackerTimer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4505 = { sizeof (Astronaut_t1874369735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4505[4] = 
{
	Astronaut_t1874369735::get_offset_of_m_Drill_11(),
	Astronaut_t1874369735::get_offset_of_m_DrillEffectSmoke_12(),
	Astronaut_t1874369735::get_offset_of_m_DrillEffectRocks_13(),
	Astronaut_t1874369735::get_offset_of_m_RockPile_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4506 = { sizeof (Augmentation_t2596699517), -1, sizeof(Augmentation_t2596699517_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4506[7] = 
{
	Augmentation_t2596699517::get_offset_of_m_EvtOnEnter_4(),
	Augmentation_t2596699517::get_offset_of_m_EvtOnExit_5(),
	Augmentation_t2596699517::get_offset_of_animator_6(),
	Augmentation_t2596699517::get_offset_of_active_7(),
	Augmentation_t2596699517::get_offset_of_waitCoroutine_8(),
	Augmentation_t2596699517_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	Augmentation_t2596699517_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4507 = { sizeof (U3CWaitForThenU3Ec__Iterator0_t751251344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4507[5] = 
{
	U3CWaitForThenU3Ec__Iterator0_t751251344::get_offset_of_waitSeconds_0(),
	U3CWaitForThenU3Ec__Iterator0_t751251344::get_offset_of_action_1(),
	U3CWaitForThenU3Ec__Iterator0_t751251344::get_offset_of_U24current_2(),
	U3CWaitForThenU3Ec__Iterator0_t751251344::get_offset_of_U24disposing_3(),
	U3CWaitForThenU3Ec__Iterator0_t751251344::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4508 = { sizeof (Drone_t259598734), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4509 = { sizeof (Fissure_t4281376855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4509[2] = 
{
	Fissure_t4281376855::get_offset_of_particleGradientNormal_4(),
	Fissure_t4281376855::get_offset_of_particleGradientPressed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4510 = { sizeof (Habitat_t2099304582), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4511 = { sizeof (OxygenTank_t391139921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4512 = { sizeof (RockPileController_t2893225296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4512[2] = 
{
	RockPileController_t2893225296::get_offset_of_m_GrabbableRock_4(),
	RockPileController_t2893225296::get_offset_of_fadeController_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4513 = { sizeof (DrillController_t3872647469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4513[2] = 
{
	DrillController_t3872647469::get_offset_of_m_IsDrilling_4(),
	DrillController_t3872647469::get_offset_of_drillLerpPercentage_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4514 = { sizeof (FadeObject_t1880495183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4514[5] = 
{
	FadeObject_t1880495183::get_offset_of_m_IsVisible_4(),
	FadeObject_t1880495183::get_offset_of_m_FadeDuration_5(),
	FadeObject_t1880495183::get_offset_of_m_RenderersToFade_6(),
	FadeObject_t1880495183::get_offset_of_fadeRatio_7(),
	FadeObject_t1880495183::get_offset_of_isInitialOpacitySet_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4515 = { sizeof (AstronautStateMachineBehaviour_t3422698390), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4516 = { sizeof (AugmentationStateMachineBehaviour_t3849818102), -1, sizeof(AugmentationStateMachineBehaviour_t3849818102_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4516[4] = 
{
	AugmentationStateMachineBehaviour_t3849818102::get_offset_of_m_OnEnterMethodName_4(),
	AugmentationStateMachineBehaviour_t3849818102::get_offset_of_m_OnUpdateMethodName_5(),
	AugmentationStateMachineBehaviour_t3849818102::get_offset_of_m_OnExitMethodName_6(),
	AugmentationStateMachineBehaviour_t3849818102_StaticFields::get_offset_of_cachedDelegates_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4517 = { sizeof (CameraFocusController_t1033776956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4517[1] = 
{
	CameraFocusController_t1033776956::get_offset_of_mVuforiaStarted_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4518 = { sizeof (GroundPlaneUI_t2709539096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4518[9] = 
{
	GroundPlaneUI_t2709539096::get_offset_of_productPanel_4(),
	GroundPlaneUI_t2709539096::get_offset_of_planeManager_5(),
	GroundPlaneUI_t2709539096::get_offset_of_productPlacement_6(),
	GroundPlaneUI_t2709539096::get_offset_of_graphicRayCaster_7(),
	GroundPlaneUI_t2709539096::get_offset_of_pointerEventData_8(),
	GroundPlaneUI_t2709539096::get_offset_of_eventSystem_9(),
	GroundPlaneUI_t2709539096::get_offset_of_showMessage_10(),
	GroundPlaneUI_t2709539096::get_offset_of_notPlaced_11(),
	GroundPlaneUI_t2709539096::get_offset_of_inVuforia_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4519 = { sizeof (U3COffVuforiaU3Ec__Iterator0_t3838014700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4519[4] = 
{
	U3COffVuforiaU3Ec__Iterator0_t3838014700::get_offset_of_U24this_0(),
	U3COffVuforiaU3Ec__Iterator0_t3838014700::get_offset_of_U24current_1(),
	U3COffVuforiaU3Ec__Iterator0_t3838014700::get_offset_of_U24disposing_2(),
	U3COffVuforiaU3Ec__Iterator0_t3838014700::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4520 = { sizeof (U3COnVuforiaU3Ec__Iterator1_t2073417735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4520[4] = 
{
	U3COnVuforiaU3Ec__Iterator1_t2073417735::get_offset_of_U24this_0(),
	U3COnVuforiaU3Ec__Iterator1_t2073417735::get_offset_of_U24current_1(),
	U3COnVuforiaU3Ec__Iterator1_t2073417735::get_offset_of_U24disposing_2(),
	U3COnVuforiaU3Ec__Iterator1_t2073417735::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4521 = { sizeof (PlaneManager_t2021199913), -1, sizeof(PlaneManager_t2021199913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4521[22] = 
{
	PlaneManager_t2021199913_StaticFields::get_offset_of_CurrentPlaneMode_4(),
	PlaneManager_t2021199913_StaticFields::get_offset_of_U3CGroundPlaneHitReceivedU3Ek__BackingField_5(),
	PlaneManager_t2021199913_StaticFields::get_offset_of_U3CAnchorExistsU3Ek__BackingField_6(),
	PlaneManager_t2021199913::get_offset_of_anchorPlacement_7(),
	PlaneManager_t2021199913::get_offset_of_productPlacement_8(),
	PlaneManager_t2021199913::get_offset_of_placementAugmentation_9(),
	0,
	0,
	PlaneManager_t2021199913::get_offset_of_stateManager_12(),
	PlaneManager_t2021199913::get_offset_of_smartTerrain_13(),
	PlaneManager_t2021199913::get_offset_of_positionalDeviceTracker_14(),
	PlaneManager_t2021199913::get_offset_of_contentPositioningBehaviour_15(),
	PlaneManager_t2021199913::get_offset_of_touchHandler_16(),
	PlaneManager_t2021199913::get_offset_of_groundPlaneUI_17(),
	PlaneManager_t2021199913::get_offset_of_planeFinder_18(),
	PlaneManager_t2021199913::get_offset_of_placementAnchor_19(),
	PlaneManager_t2021199913::get_offset_of_automaticHitTestFrameCount_20(),
	PlaneManager_t2021199913_StaticFields::get_offset_of_StatusCached_21(),
	PlaneManager_t2021199913_StaticFields::get_offset_of_StatusInfoCached_22(),
	PlaneManager_t2021199913::get_offset_of_timer_23(),
	PlaneManager_t2021199913::get_offset_of_timerFinished_24(),
	PlaneManager_t2021199913::get_offset_of_messageBoxPrefab_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4522 = { sizeof (PlaneMode_t282559100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4522[4] = 
{
	PlaneMode_t282559100::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4523 = { sizeof (ProductPlacement_t2927687625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4523[24] = 
{
	ProductPlacement_t2927687625::get_offset_of_U3CIsPlacedU3Ek__BackingField_4(),
	ProductPlacement_t2927687625::get_offset_of_augmentationObject_5(),
	ProductPlacement_t2927687625::get_offset_of_pivot_6(),
	ProductPlacement_t2927687625::get_offset_of_holder_7(),
	ProductPlacement_t2927687625::get_offset_of_visual_8(),
	ProductPlacement_t2927687625::get_offset_of_boxCollider_9(),
	ProductPlacement_t2927687625::get_offset_of_objectRenderer_10(),
	ProductPlacement_t2927687625::get_offset_of_objectShadow_11(),
	ProductPlacement_t2927687625::get_offset_of_animator_12(),
	ProductPlacement_t2927687625::get_offset_of_productSize_13(),
	ProductPlacement_t2927687625::get_offset_of_selecting_14(),
	ProductPlacement_t2927687625::get_offset_of_groundPlaneUI_15(),
	ProductPlacement_t2927687625::get_offset_of_mainCamera_16(),
	ProductPlacement_t2927687625::get_offset_of_cameraToPlaneRay_17(),
	ProductPlacement_t2927687625::get_offset_of_cameraToPlaneHit_18(),
	ProductPlacement_t2927687625::get_offset_of_cameraToObjectRay_19(),
	ProductPlacement_t2927687625::get_offset_of_cameraToObjectHit_20(),
	ProductPlacement_t2927687625::get_offset_of_productScale_21(),
	ProductPlacement_t2927687625::get_offset_of_LocalOffset_22(),
	ProductPlacement_t2927687625::get_offset_of_targetPosition_23(),
	ProductPlacement_t2927687625::get_offset_of_augmentationScale_24(),
	ProductPlacement_t2927687625::get_offset_of_floorName_25(),
	ProductPlacement_t2927687625::get_offset_of_hold_26(),
	ProductPlacement_t2927687625::get_offset_of_tap_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4524 = { sizeof (U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4524[5] = 
{
	U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553::get_offset_of_time_0(),
	U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553::get_offset_of_U24this_1(),
	U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553::get_offset_of_U24current_2(),
	U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553::get_offset_of_U24disposing_3(),
	U3CWaitForSecondsOnU3Ec__Iterator0_t4215825553::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4525 = { sizeof (U3CWaitForSecondsOffU3Ec__Iterator1_t911118217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4525[5] = 
{
	U3CWaitForSecondsOffU3Ec__Iterator1_t911118217::get_offset_of_time_0(),
	U3CWaitForSecondsOffU3Ec__Iterator1_t911118217::get_offset_of_U24this_1(),
	U3CWaitForSecondsOffU3Ec__Iterator1_t911118217::get_offset_of_U24current_2(),
	U3CWaitForSecondsOffU3Ec__Iterator1_t911118217::get_offset_of_U24disposing_3(),
	U3CWaitForSecondsOffU3Ec__Iterator1_t911118217::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4526 = { sizeof (TouchHandler_t3441426771), -1, sizeof(TouchHandler_t3441426771_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4526[12] = 
{
	TouchHandler_t3441426771::get_offset_of_augmentationObject_4(),
	TouchHandler_t3441426771::get_offset_of_enableRotation_5(),
	TouchHandler_t3441426771::get_offset_of_enablePinchScaling_6(),
	0,
	0,
	TouchHandler_t3441426771::get_offset_of_touches_9(),
	TouchHandler_t3441426771_StaticFields::get_offset_of_lastTouchCount_10(),
	TouchHandler_t3441426771::get_offset_of_isFirstFrameWithTwoTouches_11(),
	TouchHandler_t3441426771::get_offset_of_cachedTouchAngle_12(),
	TouchHandler_t3441426771::get_offset_of_cachedTouchDistance_13(),
	TouchHandler_t3441426771::get_offset_of_cachedAugmentationScale_14(),
	TouchHandler_t3441426771::get_offset_of_cachedAugmentationRotation_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4527 = { sizeof (UtilityHelper_t1766679415), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4528 = { sizeof (ModelSwap_t1632145241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4528[4] = 
{
	ModelSwap_t1632145241::get_offset_of_m_DefaultModel_4(),
	ModelSwap_t1632145241::get_offset_of_m_ExtendedModel_5(),
	ModelSwap_t1632145241::get_offset_of_m_ActiveModel_6(),
	ModelSwap_t1632145241::get_offset_of_m_TrackableSettings_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4529 = { sizeof (VideoController_t2238106033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4529[3] = 
{
	VideoController_t2238106033::get_offset_of_videoPlayer_4(),
	VideoController_t2238106033::get_offset_of_m_PlayButton_5(),
	VideoController_t2238106033::get_offset_of_m_ProgressBar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (VideoTrackableEventHandler_t944783369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (SamplesMainMenu_t83803002), -1, sizeof(SamplesMainMenu_t83803002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4531[11] = 
{
	SamplesMainMenu_t83803002_StaticFields::get_offset_of_menuItem_4(),
	0,
	0,
	SamplesMainMenu_t83803002_StaticFields::get_offset_of_isAboutScreenVisible_7(),
	SamplesMainMenu_t83803002::get_offset_of_aboutCanvas_8(),
	SamplesMainMenu_t83803002::get_offset_of_aboutTitle_9(),
	SamplesMainMenu_t83803002::get_offset_of_aboutDescription_10(),
	SamplesMainMenu_t83803002::get_offset_of_aboutScreenInfo_11(),
	SamplesMainMenu_t83803002::get_offset_of_safeAreaManager_12(),
	SamplesMainMenu_t83803002::get_offset_of_lightGrey_13(),
	SamplesMainMenu_t83803002_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (MenuItem_t4061609034)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4532[12] = 
{
	MenuItem_t4061609034::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (SamplesNavigationHandler_t3426315339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4533[1] = 
{
	SamplesNavigationHandler_t3426315339::get_offset_of_currentSceneName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (ComboBoxChanged_t1525211877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (CooldownEffect_Image_t4108738633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4535[4] = 
{
	CooldownEffect_Image_t4108738633::get_offset_of_cooldown_4(),
	CooldownEffect_Image_t4108738633::get_offset_of_displayText_5(),
	CooldownEffect_Image_t4108738633::get_offset_of_target_6(),
	CooldownEffect_Image_t4108738633::get_offset_of_originalText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { sizeof (CooldownEffect_SAUIM_t896650247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4536[2] = 
{
	CooldownEffect_SAUIM_t896650247::get_offset_of_cooldown_4(),
	CooldownEffect_SAUIM_t896650247::get_offset_of_sauim_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (Example01CellDto_t3695172350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4537[1] = 
{
	Example01CellDto_t3695172350::get_offset_of_Message_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (Example01Scene_t4105520515), -1, sizeof(Example01Scene_t4105520515_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4538[2] = 
{
	Example01Scene_t4105520515::get_offset_of_scrollView_4(),
	Example01Scene_t4105520515_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (Example01ScrollView_t2860006669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4539[1] = 
{
	Example01ScrollView_t2860006669::get_offset_of_scrollPositionController_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { sizeof (Example01ScrollViewCell_t1502270060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4540[3] = 
{
	Example01ScrollViewCell_t1502270060::get_offset_of_animator_5(),
	Example01ScrollViewCell_t1502270060::get_offset_of_message_6(),
	Example01ScrollViewCell_t1502270060::get_offset_of_scrollTriggerHash_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (Example02CellDto_t3839548158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4541[1] = 
{
	Example02CellDto_t3839548158::get_offset_of_Message_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { sizeof (Example02Scene_t3589293443), -1, sizeof(Example02Scene_t3589293443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4542[2] = 
{
	Example02Scene_t3589293443::get_offset_of_scrollView_4(),
	Example02Scene_t3589293443_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (Example02ScrollView_t3654499597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4543[1] = 
{
	Example02ScrollView_t3654499597::get_offset_of_scrollPositionController_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (Example02ScrollViewCell_t1417103111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4544[6] = 
{
	Example02ScrollViewCell_t1417103111::get_offset_of_animator_5(),
	Example02ScrollViewCell_t1417103111::get_offset_of_message_6(),
	Example02ScrollViewCell_t1417103111::get_offset_of_image_7(),
	Example02ScrollViewCell_t1417103111::get_offset_of_button_8(),
	Example02ScrollViewCell_t1417103111::get_offset_of_scrollTriggerHash_9(),
	Example02ScrollViewCell_t1417103111::get_offset_of_context_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (Example02ScrollViewContext_t1179128608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4545[2] = 
{
	Example02ScrollViewContext_t1179128608::get_offset_of_OnPressedCell_0(),
	Example02ScrollViewContext_t1179128608::get_offset_of_SelectedIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (Example03CellDto_t2912803582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4546[1] = 
{
	Example03CellDto_t2912803582::get_offset_of_Message_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (Example03Scene_t3837740419), -1, sizeof(Example03Scene_t3837740419_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4547[2] = 
{
	Example03Scene_t3837740419::get_offset_of_scrollView_4(),
	Example03Scene_t3837740419_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (Example03ScrollView_t2613198093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4548[1] = 
{
	Example03ScrollView_t2613198093::get_offset_of_scrollPositionController_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (Example03ScrollViewCell_t3211020039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4549[6] = 
{
	Example03ScrollViewCell_t3211020039::get_offset_of_animator_5(),
	Example03ScrollViewCell_t3211020039::get_offset_of_message_6(),
	Example03ScrollViewCell_t3211020039::get_offset_of_image_7(),
	Example03ScrollViewCell_t3211020039::get_offset_of_button_8(),
	Example03ScrollViewCell_t3211020039::get_offset_of_scrollTriggerHash_9(),
	Example03ScrollViewCell_t3211020039::get_offset_of_context_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (Example03ScrollViewContext_t47956341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4550[2] = 
{
	Example03ScrollViewContext_t47956341::get_offset_of_OnPressedCell_0(),
	Example03ScrollViewContext_t47956341::get_offset_of_SelectedIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (PaginationScript_t3257083080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4551[2] = 
{
	PaginationScript_t3257083080::get_offset_of_hss_4(),
	PaginationScript_t3257083080::get_offset_of_Page_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (UpdateScrollSnap_t208304835), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4552[5] = 
{
	UpdateScrollSnap_t208304835::get_offset_of_HSS_4(),
	UpdateScrollSnap_t208304835::get_offset_of_VSS_5(),
	UpdateScrollSnap_t208304835::get_offset_of_HorizontalPagePrefab_6(),
	UpdateScrollSnap_t208304835::get_offset_of_VerticalPagePrefab_7(),
	UpdateScrollSnap_t208304835::get_offset_of_JumpPage_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (AwesomeMenu_t320995884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4553[2] = 
{
	AwesomeMenu_t320995884::get_offset_of_Background_7(),
	AwesomeMenu_t320995884::get_offset_of_Title_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (GameMenu_t2197707047), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (MainMenu_t3811018904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (OptionsMenu_t3566679186), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4556[1] = 
{
	OptionsMenu_t3566679186::get_offset_of_Slider_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (PauseMenu_t1705270178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (UpdateRadialValue_t3318911919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4558[2] = 
{
	UpdateRadialValue_t3318911919::get_offset_of_input_4(),
	UpdateRadialValue_t3318911919::get_offset_of_slider_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (AnimateEffects_t2267068298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4559[19] = 
{
	AnimateEffects_t2267068298::get_offset_of_letterSpacing_4(),
	AnimateEffects_t2267068298::get_offset_of_letterSpacingMax_5(),
	AnimateEffects_t2267068298::get_offset_of_letterSpacingMin_6(),
	AnimateEffects_t2267068298::get_offset_of_letterSpacingModifier_7(),
	AnimateEffects_t2267068298::get_offset_of_curvedText_8(),
	AnimateEffects_t2267068298::get_offset_of_curvedTextMax_9(),
	AnimateEffects_t2267068298::get_offset_of_curvedTextMin_10(),
	AnimateEffects_t2267068298::get_offset_of_curvedTextModifier_11(),
	AnimateEffects_t2267068298::get_offset_of_gradient2_12(),
	AnimateEffects_t2267068298::get_offset_of_gradient2Max_13(),
	AnimateEffects_t2267068298::get_offset_of_gradient2Min_14(),
	AnimateEffects_t2267068298::get_offset_of_gradient2Modifier_15(),
	AnimateEffects_t2267068298::get_offset_of_cylinderText_16(),
	AnimateEffects_t2267068298::get_offset_of_cylinderTextRT_17(),
	AnimateEffects_t2267068298::get_offset_of_cylinderRotation_18(),
	AnimateEffects_t2267068298::get_offset_of_SAUIM_19(),
	AnimateEffects_t2267068298::get_offset_of_SAUIMMax_20(),
	AnimateEffects_t2267068298::get_offset_of_SAUIMMin_21(),
	AnimateEffects_t2267068298::get_offset_of_SAUIMModifier_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (testHref_t2744501834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4560[1] = 
{
	testHref_t2744501834::get_offset_of_textPic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (LineRendererOrbit_t3961648819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4561[8] = 
{
	LineRendererOrbit_t3961648819::get_offset_of_lr_4(),
	LineRendererOrbit_t3961648819::get_offset_of_circle_5(),
	LineRendererOrbit_t3961648819::get_offset_of_OrbitGO_6(),
	LineRendererOrbit_t3961648819::get_offset_of_orbitGOrt_7(),
	LineRendererOrbit_t3961648819::get_offset_of_orbitTime_8(),
	LineRendererOrbit_t3961648819::get_offset_of__xAxis_9(),
	LineRendererOrbit_t3961648819::get_offset_of__yAxis_10(),
	LineRendererOrbit_t3961648819::get_offset_of__steps_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (TestAddingPoints_t4285918360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4562[3] = 
{
	TestAddingPoints_t4285918360::get_offset_of_LineRenderer_4(),
	TestAddingPoints_t4285918360::get_offset_of_XValue_5(),
	TestAddingPoints_t4285918360::get_offset_of_YValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (ScrollingCalendar_t1593431244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4563[22] = 
{
	ScrollingCalendar_t1593431244::get_offset_of_monthsScrollingPanel_4(),
	ScrollingCalendar_t1593431244::get_offset_of_yearsScrollingPanel_5(),
	ScrollingCalendar_t1593431244::get_offset_of_daysScrollingPanel_6(),
	ScrollingCalendar_t1593431244::get_offset_of_yearsButtonPrefab_7(),
	ScrollingCalendar_t1593431244::get_offset_of_monthsButtonPrefab_8(),
	ScrollingCalendar_t1593431244::get_offset_of_daysButtonPrefab_9(),
	ScrollingCalendar_t1593431244::get_offset_of_monthsButtons_10(),
	ScrollingCalendar_t1593431244::get_offset_of_yearsButtons_11(),
	ScrollingCalendar_t1593431244::get_offset_of_daysButtons_12(),
	ScrollingCalendar_t1593431244::get_offset_of_monthCenter_13(),
	ScrollingCalendar_t1593431244::get_offset_of_yearsCenter_14(),
	ScrollingCalendar_t1593431244::get_offset_of_daysCenter_15(),
	ScrollingCalendar_t1593431244::get_offset_of_yearsVerticalScroller_16(),
	ScrollingCalendar_t1593431244::get_offset_of_monthsVerticalScroller_17(),
	ScrollingCalendar_t1593431244::get_offset_of_daysVerticalScroller_18(),
	ScrollingCalendar_t1593431244::get_offset_of_inputFieldDays_19(),
	ScrollingCalendar_t1593431244::get_offset_of_inputFieldMonths_20(),
	ScrollingCalendar_t1593431244::get_offset_of_inputFieldYears_21(),
	ScrollingCalendar_t1593431244::get_offset_of_dateText_22(),
	ScrollingCalendar_t1593431244::get_offset_of_daysSet_23(),
	ScrollingCalendar_t1593431244::get_offset_of_monthsSet_24(),
	ScrollingCalendar_t1593431244::get_offset_of_yearsSet_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (Accordion_t2301662264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4564[2] = 
{
	Accordion_t2301662264::get_offset_of_m_Transition_4(),
	Accordion_t2301662264::get_offset_of_m_TransitionDuration_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (Transition_t1740872941)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4565[3] = 
{
	Transition_t1740872941::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (AccordionElement_t774870540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4566[5] = 
{
	AccordionElement_t774870540::get_offset_of_m_MinHeight_23(),
	AccordionElement_t774870540::get_offset_of_m_Accordion_24(),
	AccordionElement_t774870540::get_offset_of_m_RectTransform_25(),
	AccordionElement_t774870540::get_offset_of_m_LayoutElement_26(),
	AccordionElement_t774870540::get_offset_of_m_FloatTweenRunner_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (FloatTween_t916090420)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4567[6] = 
{
	FloatTween_t916090420::get_offset_of_m_StartFloat_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t916090420::get_offset_of_m_TargetFloat_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t916090420::get_offset_of_m_Duration_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t916090420::get_offset_of_m_IgnoreTimeScale_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t916090420::get_offset_of_m_Target_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t916090420::get_offset_of_m_Finish_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (FloatTweenCallback_t1418708507), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (FloatFinishCallback_t3825696533), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4571[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4572[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (BoxSlider_t3694973841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4573[11] = 
{
	BoxSlider_t3694973841::get_offset_of_m_HandleRect_18(),
	BoxSlider_t3694973841::get_offset_of_m_MinValue_19(),
	BoxSlider_t3694973841::get_offset_of_m_MaxValue_20(),
	BoxSlider_t3694973841::get_offset_of_m_WholeNumbers_21(),
	BoxSlider_t3694973841::get_offset_of_m_ValueX_22(),
	BoxSlider_t3694973841::get_offset_of_m_ValueY_23(),
	BoxSlider_t3694973841::get_offset_of_m_OnValueChanged_24(),
	BoxSlider_t3694973841::get_offset_of_m_HandleTransform_25(),
	BoxSlider_t3694973841::get_offset_of_m_HandleContainerRect_26(),
	BoxSlider_t3694973841::get_offset_of_m_Offset_27(),
	BoxSlider_t3694973841::get_offset_of_m_Tracker_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (Direction_t1135146939)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4574[5] = 
{
	Direction_t1135146939::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (BoxSliderEvent_t4223315588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (Axis_t1696420063)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4576[3] = 
{
	Axis_t1696420063::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (ColorImage_t1780977718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4577[2] = 
{
	ColorImage_t1780977718::get_offset_of_picker_4(),
	ColorImage_t1780977718::get_offset_of_image_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (ColorLabel_t1657108856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4578[7] = 
{
	ColorLabel_t1657108856::get_offset_of_picker_4(),
	ColorLabel_t1657108856::get_offset_of_type_5(),
	ColorLabel_t1657108856::get_offset_of_prefix_6(),
	ColorLabel_t1657108856::get_offset_of_minValue_7(),
	ColorLabel_t1657108856::get_offset_of_maxValue_8(),
	ColorLabel_t1657108856::get_offset_of_precision_9(),
	ColorLabel_t1657108856::get_offset_of_label_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (ColorPickerControl_t2793111723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4579[9] = 
{
	ColorPickerControl_t2793111723::get_offset_of__hue_4(),
	ColorPickerControl_t2793111723::get_offset_of__saturation_5(),
	ColorPickerControl_t2793111723::get_offset_of__brightness_6(),
	ColorPickerControl_t2793111723::get_offset_of__red_7(),
	ColorPickerControl_t2793111723::get_offset_of__green_8(),
	ColorPickerControl_t2793111723::get_offset_of__blue_9(),
	ColorPickerControl_t2793111723::get_offset_of__alpha_10(),
	ColorPickerControl_t2793111723::get_offset_of_onValueChanged_11(),
	ColorPickerControl_t2793111723::get_offset_of_onHSVChanged_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (ColorPickerPresets_t2164031678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4580[3] = 
{
	ColorPickerPresets_t2164031678::get_offset_of_picker_4(),
	ColorPickerPresets_t2164031678::get_offset_of_presets_5(),
	ColorPickerPresets_t2164031678::get_offset_of_createPresetImage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (ColorPickerTester_t1752022104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4581[2] = 
{
	ColorPickerTester_t1752022104::get_offset_of_pickerRenderer_4(),
	ColorPickerTester_t1752022104::get_offset_of_picker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (ColorSlider_t188049029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4582[4] = 
{
	ColorSlider_t188049029::get_offset_of_ColorPicker_4(),
	ColorSlider_t188049029::get_offset_of_type_5(),
	ColorSlider_t188049029::get_offset_of_slider_6(),
	ColorSlider_t188049029::get_offset_of_listen_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (ColorSliderImage_t1534316151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4583[4] = 
{
	ColorSliderImage_t1534316151::get_offset_of_picker_4(),
	ColorSliderImage_t1534316151::get_offset_of_type_5(),
	ColorSliderImage_t1534316151::get_offset_of_direction_6(),
	ColorSliderImage_t1534316151::get_offset_of_image_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (ColorValues_t950091080)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4584[8] = 
{
	ColorValues_t950091080::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (ColorChangedEvent_t3019780707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (HSVChangedEvent_t911780251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (HexColorField_t106756477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4587[4] = 
{
	HexColorField_t106756477::get_offset_of_ColorPicker_4(),
	HexColorField_t106756477::get_offset_of_displayAlpha_5(),
	HexColorField_t106756477::get_offset_of_hexInputField_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (HSVUtil_t2354927206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (HsvColor_t3316572990)+ sizeof (RuntimeObject), sizeof(HsvColor_t3316572990 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4589[3] = 
{
	HsvColor_t3316572990::get_offset_of_H_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t3316572990::get_offset_of_S_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HsvColor_t3316572990::get_offset_of_V_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (SVBoxSlider_t1594361808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4590[5] = 
{
	SVBoxSlider_t1594361808::get_offset_of_picker_4(),
	SVBoxSlider_t1594361808::get_offset_of_slider_5(),
	SVBoxSlider_t1594361808::get_offset_of_image_6(),
	SVBoxSlider_t1594361808::get_offset_of_lastH_7(),
	SVBoxSlider_t1594361808::get_offset_of_listen_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (TiltWindow_t4093125161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4591[4] = 
{
	TiltWindow_t4093125161::get_offset_of_range_4(),
	TiltWindow_t4093125161::get_offset_of_mTrans_5(),
	TiltWindow_t4093125161::get_offset_of_mStart_6(),
	TiltWindow_t4093125161::get_offset_of_mRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (AutoCompleteSearchType_t2628651075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4592[3] = 
{
	AutoCompleteSearchType_t2628651075::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (AutoCompleteComboBox_t2765567798), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4593[33] = 
{
	AutoCompleteComboBox_t2765567798::get_offset_of_disabledTextColor_4(),
	AutoCompleteComboBox_t2765567798::get_offset_of_U3CSelectedItemU3Ek__BackingField_5(),
	AutoCompleteComboBox_t2765567798::get_offset_of_AvailableOptions_6(),
	AutoCompleteComboBox_t2765567798::get_offset_of__isPanelActive_7(),
	AutoCompleteComboBox_t2765567798::get_offset_of__hasDrawnOnce_8(),
	AutoCompleteComboBox_t2765567798::get_offset_of__mainInput_9(),
	AutoCompleteComboBox_t2765567798::get_offset_of__inputRT_10(),
	AutoCompleteComboBox_t2765567798::get_offset_of__rectTransform_11(),
	AutoCompleteComboBox_t2765567798::get_offset_of__overlayRT_12(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollPanelRT_13(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollBarRT_14(),
	AutoCompleteComboBox_t2765567798::get_offset_of__slidingAreaRT_15(),
	AutoCompleteComboBox_t2765567798::get_offset_of__itemsPanelRT_16(),
	AutoCompleteComboBox_t2765567798::get_offset_of__canvas_17(),
	AutoCompleteComboBox_t2765567798::get_offset_of__canvasRT_18(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollRect_19(),
	AutoCompleteComboBox_t2765567798::get_offset_of__panelItems_20(),
	AutoCompleteComboBox_t2765567798::get_offset_of__prunedPanelItems_21(),
	AutoCompleteComboBox_t2765567798::get_offset_of_panelObjects_22(),
	AutoCompleteComboBox_t2765567798::get_offset_of_itemTemplate_23(),
	AutoCompleteComboBox_t2765567798::get_offset_of_U3CTextU3Ek__BackingField_24(),
	AutoCompleteComboBox_t2765567798::get_offset_of__scrollBarWidth_25(),
	AutoCompleteComboBox_t2765567798::get_offset_of__itemsToDisplay_26(),
	AutoCompleteComboBox_t2765567798::get_offset_of_SelectFirstItemOnStart_27(),
	AutoCompleteComboBox_t2765567798::get_offset_of__ChangeInputTextColorBasedOnMatchingItems_28(),
	AutoCompleteComboBox_t2765567798::get_offset_of_ValidSelectionTextColor_29(),
	AutoCompleteComboBox_t2765567798::get_offset_of_MatchingItemsRemainingTextColor_30(),
	AutoCompleteComboBox_t2765567798::get_offset_of_NoItemsRemainingTextColor_31(),
	AutoCompleteComboBox_t2765567798::get_offset_of_autocompleteSearchType_32(),
	AutoCompleteComboBox_t2765567798::get_offset_of__selectionIsValid_33(),
	AutoCompleteComboBox_t2765567798::get_offset_of_OnSelectionTextChanged_34(),
	AutoCompleteComboBox_t2765567798::get_offset_of_OnSelectionValidityChanged_35(),
	AutoCompleteComboBox_t2765567798::get_offset_of_OnSelectionChanged_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (SelectionChangedEvent_t1822043360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (SelectionTextChangedEvent_t4051177638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (SelectionValidityChangedEvent_t954817928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (U3CRebuildPanelU3Ec__AnonStorey0_t1110430399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4597[2] = 
{
	U3CRebuildPanelU3Ec__AnonStorey0_t1110430399::get_offset_of_textOfItem_0(),
	U3CRebuildPanelU3Ec__AnonStorey0_t1110430399::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4598[1] = 
{
	U3CPruneItemsLinqU3Ec__AnonStorey1_t1526519783::get_offset_of_currText_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (ComboBox_t4216213764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4599[23] = 
{
	ComboBox_t4216213764::get_offset_of_disabledTextColor_4(),
	ComboBox_t4216213764::get_offset_of_U3CSelectedItemU3Ek__BackingField_5(),
	ComboBox_t4216213764::get_offset_of_AvailableOptions_6(),
	ComboBox_t4216213764::get_offset_of__scrollBarWidth_7(),
	ComboBox_t4216213764::get_offset_of__itemsToDisplay_8(),
	ComboBox_t4216213764::get_offset_of_OnSelectionChanged_9(),
	ComboBox_t4216213764::get_offset_of__isPanelActive_10(),
	ComboBox_t4216213764::get_offset_of__hasDrawnOnce_11(),
	ComboBox_t4216213764::get_offset_of__mainInput_12(),
	ComboBox_t4216213764::get_offset_of__inputRT_13(),
	ComboBox_t4216213764::get_offset_of__rectTransform_14(),
	ComboBox_t4216213764::get_offset_of__overlayRT_15(),
	ComboBox_t4216213764::get_offset_of__scrollPanelRT_16(),
	ComboBox_t4216213764::get_offset_of__scrollBarRT_17(),
	ComboBox_t4216213764::get_offset_of__slidingAreaRT_18(),
	ComboBox_t4216213764::get_offset_of__itemsPanelRT_19(),
	ComboBox_t4216213764::get_offset_of__canvas_20(),
	ComboBox_t4216213764::get_offset_of__canvasRT_21(),
	ComboBox_t4216213764::get_offset_of__scrollRect_22(),
	ComboBox_t4216213764::get_offset_of__panelItems_23(),
	ComboBox_t4216213764::get_offset_of_panelObjects_24(),
	ComboBox_t4216213764::get_offset_of_itemTemplate_25(),
	ComboBox_t4216213764::get_offset_of_U3CTextU3Ek__BackingField_26(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
